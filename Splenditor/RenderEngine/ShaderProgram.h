//
// Created by jack on 20/11/15.
//

#ifndef SPLENDITORMASTER_SHADERPROGRAM_H
#define SPLENDITORMASTER_SHADERPROGRAM_H

#include <string>
#include "GL/glew.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#define max_lights 1

using namespace std;
using namespace glm;

class ShaderProgram {
public:
    ShaderProgram(const char * vertex_file_path, const char * fragment_file_path);
    virtual ~ShaderProgram() { cleanUp(); }
    void start();
    void stop();
    void cleanUp();
    void bindAttribute(int attribute, const char* variableName);
protected:
    virtual void getAllUniformLocations() = 0;
    virtual void bindAttributes() = 0;
    void loadFloat(int location, float value);
    void load3DVector(int location, vec3 vector);
    void load4DVector(int location, vec4 vector);
    void load2DVector(int location, vec2 vector);
    void loadBoolean(int location, bool value);
    void loadMatrix(int location, mat4 matrix);
    void loadInt(int location, int value);
    GLuint getProgramID() const { return programID; }
    GLint getUniformLocation(string uniformName);
private:
    int CompileShader(const char* ShaderPath, const GLuint ShaderID);
    GLuint programID;
    GLuint vertexShaderID;
    GLuint fragmentShaderID;

};


#endif //SPLENDITORMASTER_SHADERPROGRAM_H
