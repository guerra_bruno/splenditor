//
// Created by jack on 20/11/15.
//

#ifndef SPLENDITORMASTER_DISPLAYMANAGER_H
#define SPLENDITORMASTER_DISPLAYMANAGER_H


#include <GL/glew.h>
#include <glfw3.h>
#include <stdio.h>


class DisplayManager {

public:
    DisplayManager(int width, int height);
    ~DisplayManager();
    GLFWwindow *getWindow() const { return window; }
    void updateDisplay();
    int getWidth() { return width; }
    int getHeight() { return height; }

private:
    GLFWwindow* window;
    int width;
    int height;

};


#endif //SPLENDITORMASTER_DISPLAYMANAGER_H
