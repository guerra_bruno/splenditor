    //
// Created by jack on 23/11/15.
//

#ifndef SPLENDITORMASTER_MODELTEXTURE_H
#define SPLENDITORMASTER_MODELTEXTURE_H


#include "GL/glew.h"
#include <iostream>

using namespace std;


// class that control a textured model
class ModelTexture {
public:
    ModelTexture(GLuint id);
    ~ModelTexture();
    GLuint getTextureID();
    void setReflectivity(float reflectivity);
    void setShineDamper(float shineDamper);
    void setHasTransparency(bool transparency);
    void setUseFakeLighting(bool useFakeLighting);
    void setRowNumber(int rowNumber);
    bool isHasTransparency();
    bool isUseFakeLighting() const;
    float getReflectivity() const;
    float getShineDamper() const;
    int getRowNumber() const;
    GLuint getNormalMap() const { return normalMap; }
    void setNormalMap(GLuint normalMap) { ModelTexture::normalMap = normalMap; }
private:
    GLuint textureID;
    GLuint normalMap;
    int rowNumber;
    float shineDamper;
    float reflectivity;;
    bool hasTransparency;
    bool useFakeLighting;

};


#endif //SPLENDITORMASTER_MODELTEXTURE_H
