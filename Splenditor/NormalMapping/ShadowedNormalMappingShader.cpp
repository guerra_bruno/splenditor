//
// Created by jack on 12/03/16.
//

#include "ShadowedNormalMappingShader.h"

ShadowedNormalMappingShader::ShadowedNormalMappingShader(const char *vertex_file_path, const char *fragment_file_path)  : NormalMappingShader(vertex_file_path, fragment_file_path){
    getAllUniformLocations();
}

ShadowedNormalMappingShader::~ShadowedNormalMappingShader() {

}

void ShadowedNormalMappingShader::connectTextureUnits() {
    this->NormalMappingShader::connectTextureUnits();
    loadInt(location_shadowMap, 5);
}

void ShadowedNormalMappingShader::loadMapSize(float mapSize) {
    loadFloat(location_mapSize, mapSize);
}

void ShadowedNormalMappingShader::loadToShadowMapSpace(mat4 matrix) {
    this->loadMatrix(location_toShadowMapSpace, matrix);
}

void ShadowedNormalMappingShader::getAllUniformLocations() {
    location_toShadowMapSpace = getUniformLocation("toShadowMapSpace");
    location_shadowMap = getUniformLocation("shadowMap");
    location_mapSize = getUniformLocation("mapSize");
    location_pcfCount = getUniformLocation("pcfCount");
    location_shadowDistance = getUniformLocation("shadowDistance");

}

void ShadowedNormalMappingShader::loadPCF(int pcf) {
    this->loadInt(location_pcfCount, pcf);
}

void ShadowedNormalMappingShader::loadShadowDistance(float shadowDistance) {
    this->loadFloat(location_shadowDistance, shadowDistance);
}
