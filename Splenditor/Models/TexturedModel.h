//
// Created by jack on 23/11/15.
//

#ifndef SPLENDITORMASTER_TEXTUREDMODEL_H
#define SPLENDITORMASTER_TEXTUREDMODEL_H

#include "../Models/RawModel.h"
#include "ModelTexture.h"
#include <iostream>

using namespace std;


// textureModel is a raw model with a textureID
class TexturedModel {
public:
    TexturedModel(RawModel *model, ModelTexture *texture);
    ~TexturedModel();
    RawModel* GetRawModel() const;
    ModelTexture* GetTexture() const;
private:
    RawModel *rawModel;
    ModelTexture *texture;
};



#endif //SPLENDITORMASTER_TEXTUREDMODEL_H
