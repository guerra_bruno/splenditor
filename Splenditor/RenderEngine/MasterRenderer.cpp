//
// Created by jack on 27/11/15.
//

#include "MasterRenderer.h"


MasterRenderer::MasterRenderer(DisplayManager* display, World *world) {
    PCF = 2;
    FOV = 50.0f, NEAR_PLANE = 0.1f, FAR_PLANE = 3000.0f;
    //shadowResolution = 4096;
    shadowResolution = 2048;
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    this->width = display->getWidth();
    this->height = display->getHeight();
    createProjectionMatrix();
    this->renderer = new EntityRenderer(projectionMatrix);
    this->terrainRenderer = new TerrainRenderer(projectionMatrix);
    this->skyboxRenderer = new SkyboxRenderer(world->loader, projectionMatrix);
    this->normalMapRenderer = new NormalMappingRenderer(projectionMatrix);
    this->shadowMapRenderer = new ShadowMapMasterRenderer(world->camera, shadowResolution, NEAR_PLANE, FOV, display);
    colour = rbg_OpenGl(world->colour);
}

MasterRenderer::MasterRenderer() {

}


void MasterRenderer::renderScene(World *world, vec4 clipPlane, double delta, bool day, bool refractive, bool reflective) {

    if(refractive){
        for (auto &entity : world->refractiveWaterEntities) processEntity(entity);
        processEntity(world->player);
    }
    else if(reflective){
        for (auto &entity : world->reflectiveWaterEntities) processEntity(entity);
        processEntity(world->player);
    }
    else {
        for (auto &entity : world->entities) processEntity(entity);
        for (auto &entity : world->normalMapEntities) processNormalMapEntity(entity);
    }


    // player
    processNormalMapEntity(world->player);

    // terrain
    processTerrain(world->terrain);

    // render
    render(world, clipPlane, delta, day, refractive || reflective);

}




void MasterRenderer::render(World *world, vec4 clipPlane, double delta, bool day, bool water) {

    if(water) {
        prepare();
        renderer->render(entities, clipPlane, colour, world);
        terrainRenderer->render(terrains, clipPlane, colour, world);

    }
    else {

        prepare();

        // shadow
        glActiveTexture(GL_TEXTURE5);
        glBindTexture(GL_TEXTURE_2D, getShadowMapTexture());
        mat4 shadowMapSpaceMatrix = shadowMapRenderer->getToShadowMapSpaceMatrix();

        renderer->render(entities, clipPlane, colour, world, shadowResolution, shadowMapSpaceMatrix, PCF, shadowMapRenderer->getShadowDistance());
        normalMapRenderer->render(normalMapEntities, clipPlane, world, shadowResolution, shadowMapSpaceMatrix, PCF, shadowMapRenderer->getShadowDistance());
        terrainRenderer->render(terrains, clipPlane, colour, world, shadowResolution, shadowMapSpaceMatrix, PCF, shadowMapRenderer->getShadowDistance());

        normalMapEntities.clear();
    }

    skyboxRenderer->render(world->camera, delta, day, colour.r, colour.g, colour.b);

    entities.clear();
    terrains.clear();

}


void MasterRenderer::processEntity(Entity *entity) {
    TexturedModel *entityModel = entity->GetModel();
    if (entities.find(entityModel) == entities.end()) {
        std::list<Entity*> newBatch;
        newBatch.push_back(entity);
        entities[entityModel] = newBatch;
    } else {
        entities[entityModel].push_back(entity);
    }
}

void MasterRenderer::processNormalMapEntity(Entity *entity) {
    TexturedModel *entityModel = entity->GetModel();
    if (normalMapEntities.find(entityModel) == normalMapEntities.end()) {
        std::list<Entity*> newBatch;
        newBatch.push_back(entity);
        normalMapEntities[entityModel] = newBatch;
    } else {
        normalMapEntities[entityModel].push_back(entity);
    }
}

void MasterRenderer::prepare() {
    glEnable(GL_DEPTH_TEST);
    glClearColor(colour.r, colour.g, colour.b, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void MasterRenderer::createProjectionMatrix() {
    projectionMatrix = perspective(FOV, (float) width / (float) height, NEAR_PLANE, FAR_PLANE);
}

void MasterRenderer::processTerrain(Terrain *terrain) {
    terrains.push_back(terrain);
}

MasterRenderer::~MasterRenderer() {
    delete renderer;
    delete terrainRenderer;
    delete skyboxRenderer;
    delete normalMapRenderer;
    delete shadowMapRenderer;
    entities.clear();
    normalMapEntities.clear();
}

mat4 MasterRenderer::getProjectionMatrix() {
    return projectionMatrix;
}

vec3 MasterRenderer::rbg_OpenGl(vec3 colour) {
    return vec3(colour.r * (1.0/255.0), colour.g * (1.0/255.0), colour.b * (1.0/255.0));
}

GLuint MasterRenderer::getShadowMapTexture() {
    return shadowMapRenderer->getShadowMap();
}

void MasterRenderer::renderShadowMap(std::list<Entity *> entityList, Light *sun) {
    for(auto& entity : entityList){
        processEntity(entity);
    }
    shadowMapRenderer->render(entities, sun);
    entities.clear();
}
