//
// Created by jack on 18/02/16.
//

#include <iostream>
#include <Splenditor/ToolBox/World.h>
#include "Particle.h"
#include <glm/gtx/norm.hpp>

bool Particle::update(World *world) {
    velocity.y += GRAVITY * gravityEffect * world->delta;
    reusable_change = velocity;
    reusable_change = (reusable_change * world->delta);
    position = (position + reusable_change);
    distance = length2(world->camera->GetPosition() - position);
    updateTextureCoordInfo();
    elapsedTime += world->delta;
    return elapsedTime < lifeLength;
}

Particle::Particle(ParticleTexture* texture, vec3 position, vec3 velocity, float gravityEffect, float lifeLength, float rotation, float scale) {
    this->distance = 0.0;
    this->texture = texture;
    this->elapsedTime = 0.0;
    this->position = position;
    this->velocity = velocity;
    this->gravityEffect = gravityEffect;
    this->lifeLength = lifeLength;
    this->rotation = rotation;
    this->scale = scale;
}

Particle::~Particle() {
}

void Particle::updateTextureCoordInfo() {
    float lifeFator = elapsedTime / lifeLength;
    int stageCount = texture->getNumberOfRows() * texture->getNumberOfRows();
    float atlasProgression = lifeFator * stageCount;
    int index2, index1 = (int) floor(atlasProgression);
    (index1 < stageCount - 1) ? index2 = index1 + 1 : index2 = index1;
    this->blendFactor = mod(atlasProgression, 1.0f);
    texOffset1 = setTextureOffset(index1);
    texOffset2 = setTextureOffset(index2);
}

vec2 Particle::setTextureOffset(int index) {
    vec2 offset;
    int column = index % (int) texture->getNumberOfRows();
    int row = index / texture->getNumberOfRows();
    offset.x = (float) column / texture->getNumberOfRows();
    offset.y = (float) row / texture->getNumberOfRows();
    return offset;
}

