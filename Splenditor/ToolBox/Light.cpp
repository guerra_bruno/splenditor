//
// Created by jack on 26/11/15.
//

#include "Light.h"

Light::Light(vec3 position, vec3 colour) {
    this->position = position;
    this->colour = colour;
    this->attenuation = vec3(1.0, 0.0, 0.0);
}

Light::Light(vec3 position, vec3 colour, vec3 attenuation) {
    this->position = position;
    this->colour = colour;
    this->attenuation = attenuation;
}

void Light::SetColour(vec3 colour) {
    this->colour = colour;
}

vec3 Light::GetColour() const {
    return colour;
}

void Light::SetPosition(vec3 position) {
    this->position = position;
}

vec3 Light::GetPosition() const {
    return position;
}

void Light::SetAttenuation(vec3 attenuation) {
    this->attenuation = attenuation;
}

vec3 Light::GetAttenuation() const {
    return attenuation;
}

Light::~Light() {
}
