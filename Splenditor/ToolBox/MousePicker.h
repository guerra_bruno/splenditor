//
// Created by jack on 10/12/15.
//

#ifndef SPLENDITORMASTER_MOUSEPICKER_H
#define SPLENDITORMASTER_MOUSEPICKER_H

#include "common.h"
#include "Camera.h"


class MousePicker {
public:
    MousePicker(Camera *cam, Mouse *mouse, Terrain *terrain, mat4 projection);
    ~MousePicker();
    vec2 getNormalizedDeviceCoords(float mouse_x, float mouse_y);
    vec3 getCurrentRay();
    vec3 getCurrentTerrainPoint();
    vec3 calculateMouseRay();
    vec3 toWorldCoords(vec4 eyeCoords);
    vec3 getPointOnRay(vec3 ray, float distance);
    vec3 binarySearch(int count, float start, float finish, vec3 ray);
    vec4 toEyeCoords(vec4 clipCoords);
    bool intersectionInRange(float start, float finish, vec3 ray);
    bool isUnderGround(vec3 testPoint);
    void update();
private:
    vec3 currentRay;
    vec3 currentTerrainPoint;
    mat4 projectionMatrix;
    mat4 viewMatrix;
    Camera *camera;
    Mouse *mouse;
    Terrain *terrain;
    int RECURSION_COUNT, RAY_RANGE;
};


#endif //SPLENDITORMASTER_MOUSEPICKER_H
