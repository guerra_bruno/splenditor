//
// Created by jack on 12/01/16.
//

#include "WaterRenderer.h"

WaterRenderer::WaterRenderer(Loader *loader, mat4 projectionMatrix, WaterFrameBuffers *fbos) {
    this->shader = new WaterShader("Shaders/waterVertexShader.glsl", "Shaders/waterFragmentShader.glsl");
    this->fbos = fbos;
    moveFactor = 0.0;
    shader->start();
    shader->connectTextureUnits();
    shader->loadProjectionMatrix(projectionMatrix);
    shader->stop();
    setUpVAO(loader);
}

void WaterRenderer::prepareRender(World* world, Camera *camera) {
    shader->start();
    shader->loadViewMatrix(camera);
    moveFactor += world->delta * WAVE_SPEED;
    moveFactor = mod(moveFactor, 1.0f);
    shader->loadMoveFactor(moveFactor);
    shader->loadLights(world->lights);
    shader->loadCameraPosition(camera->GetPosition());
    glBindVertexArray(quad->getVaoID());
    glEnableVertexAttribArray(0);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, fbos->getReflectionTexture());

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, fbos->getRefractionTexture());

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, world->water_dudv);

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, world->water_normal);

    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_2D, fbos->getRefractionDepthTexture());

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

}

void WaterRenderer::setUpVAO(Loader *loader) {
    float v[12] = {-1.0, -1.0, -1.0, 1.0, 1.0, -1.0, 1.0, -1.0, -1.0, 1.0, 1.0, 1.0};
    vector<float> vertices(v, v + sizeof v / sizeof v[0]);
    quad = loader->loadToVAO(vertices, 2);
}

void WaterRenderer::render(World* world, Camera *camera) {
    prepareRender(world, camera);
    for(auto& water : world->waters){
        mat4 modelMatrix = createTransformation(water->getPosition(), 0.0, 0.0, 0.0, water->GetTILE_SIZE());
        shader->loadModelMatrix(modelMatrix);
        glDrawArrays(GL_TRIANGLES, 0, quad->getVertexCount());
    }
    unbind();
}

void WaterRenderer::unbind() {
    glDisable(GL_BLEND);
    glDisable(GL_BLEND);
    glDisableVertexAttribArray(0);
    glBindVertexArray(0);
    shader->stop();
}

WaterRenderer::~WaterRenderer() {
    delete shader;
    delete quad;
    delete fbos;
}
