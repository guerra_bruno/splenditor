//
// Created by jack on 20/11/15.
//

#ifndef SPLENDITORMASTER_ENTITYSHADER_H
#define SPLENDITORMASTER_ENTITYSHADER_H



#include <string>
#include <Splenditor/ToolBox/Camera.h>
#include <Splenditor/ToolBox/Light.h>
#include <Splenditor/RenderEngine/ShaderProgram.h>

using namespace std;



class EntityShader : public ShaderProgram {
public:
    EntityShader(const char *vertex_file_path, const char *fragment_file_path);
    ~EntityShader();
    void loadTransformationMatrix(mat4 matrix);
    void loadProjectionMatrix(mat4 matrix);
    void loadViewMatrix(Camera *camera);
    void loadLights(vector<Light*> lights);
    void loadFakeLighting(bool useFake);
    void loadShineVariables(float damper, float reflectivity);
    void loadSkyColour(float r, float g, float b);
    void loadClipPlane(vec4 plane);
    void loadRowNumber(int rowNumber);
    void loadOffset(float x, float y);

protected:
    void getAllUniformLocations();
    void bindAttributes();

private:
    GLint location_transformationMatrix;
    GLint location_projectionMatrix;
    GLint location_viewMatrix;
    GLint location_lightColour[max_lights];
    GLint location_lightPosition[max_lights];
    GLint location_attenuation[max_lights];
    GLint location_shineDamper;
    GLint location_reflectivity;
    GLint location_useFakeLighting;
    GLint location_skyColour;
    GLint location_rowNumber;
    GLint location_offset;
    GLint location_plane;
};


#endif //SPLENDITORMASTER_ENTITYSHADER_H
