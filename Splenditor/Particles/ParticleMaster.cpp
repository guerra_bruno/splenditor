//
// Created by jack on 18/02/16.
//


#include <Splenditor/ToolBox/common.h>
#include "ParticleMaster.h"


ParticleMaster::ParticleMaster(Loader *loader, mat4 projectionMatrix) {
    renderer = new ParticleRenderer(loader, projectionMatrix);
}

ParticleMaster::~ParticleMaster() {
    for (auto& tex : particles) {
        delete tex.first;
        for(auto& particle : tex.second){
            delete particle;
        }
        tex.second.clear();
    }
    particles.clear();
    delete renderer;
}


void ParticleMaster::update(World* world) {

    for (auto it_map = particles.begin(); it_map != particles.end() /* not hoisted */; /* no increment */) {
        // list
        auto it_list = it_map->second.begin();
        while (it_list != it_map->second.end()){
            if (!(*it_list)->update(world))
                it_map->second.erase(it_list++);
            else
                ++it_list;
        }
        if (it_map->second.empty())
            particles.erase(it_map++);
        else if (!it_map->first->getAdditive())
                it_map++;
        else
            it_map++->second.sort(compareParticle);

    }

}

void ParticleMaster::renderParticles(Camera *camera) {
    renderer->render(particles, camera);
}

void ParticleMaster::addParticle(Particle *particle) {
    std::list<Particle*> list = particles[particle->getTexture()];
    if(list.empty()){
        particles[particle->getTexture()] = list;
    }
    particles[particle->getTexture()].push_back(particle);
}

bool compareParticle(Particle *const &p1, Particle *const &p2) {
    return p1->getDistance() < p2->getDistance();
}
