//
// Created by jack on 23/11/15.
//

#include <stdio.h>
#include "ModelTexture.h"



ModelTexture::ModelTexture(GLuint id) {
    this->shineDamper = 1.0;
    this->rowNumber = 1;
    this->reflectivity = 0.0;
    this->textureID = id;
    this->useFakeLighting = false;
    this->hasTransparency = false;
}


GLuint ModelTexture::getTextureID() {
    return textureID;
}

void ModelTexture::setReflectivity(float reflectivity) {
    this->reflectivity = reflectivity;
}

bool ModelTexture::isHasTransparency() {
    return hasTransparency;
}

void ModelTexture::setHasTransparency(bool transparency) {
    this->hasTransparency = transparency;
}


float ModelTexture::getReflectivity() const {
    return reflectivity;
}

void ModelTexture::setShineDamper(float shineDamper) {
    this->shineDamper = shineDamper;
}

float ModelTexture::getShineDamper() const {
    return shineDamper;
}

void ModelTexture::setUseFakeLighting(bool useFakeLighting) {
    this->useFakeLighting = useFakeLighting;
}

bool ModelTexture::isUseFakeLighting() const {
    return useFakeLighting;
}

ModelTexture::~ModelTexture() {
}

int ModelTexture::getRowNumber() const {
    return rowNumber;
}

void ModelTexture::setRowNumber(int rowNumber) {
    this->rowNumber = rowNumber;
}
