//
// Created by jack on 17/02/16.
//

#ifndef SPLENDITORMASTER_GUITEXT_H
#define SPLENDITORMASTER_GUITEXT_H


#include <Splenditor/FontRendering/TextMaster.h>
#include "Text.h"

class GUIText {
public:
    GUIText(string text, float fontSize, vec2 position, float maxLineLength, FontType* font, TextMaster *master, bool centered = false);
    GUIText(Text* text, TextMaster *master);
    ~GUIText();
    Text *getText() const { return text; }
    TextMaster *getMaster() const { return master; }
    void remove();

private:
    Text* text;
    TextMaster *master;
};


#endif //SPLENDITORMASTER_GUITEXT_H
