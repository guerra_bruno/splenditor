//
// Created by jack on 1/12/15.
//



#include "Player.h"


void Player::move(bool *keys, double delta, Terrain *terrain) {
    checkInputs(keys);
    increaseRotation(0.0, (float) (currentTurnSpeed * delta), 0.0);
    double distance = currentSpeed * delta;
    double dx = distance * sin(radians(GetRy()));
    double dz = distance * cos(radians(GetRy()));
    increasePosition((float) dx, 0.0, (float) dz);
    upwardsSpeed += GRAVITY * delta;
    increasePosition(0.0, (float) (upwardsSpeed * delta), 0.0);
    float terrainHeight = terrain->getHeightOfTerrain(this->GetPosition().x, this->GetPosition().z);
    if(GetPosition().y < terrainHeight){
        upwardsSpeed = 0.0;
        isInAir = false;
        SetPosition(vec3(GetPosition().x, terrainHeight, GetPosition().z));
    }
}



void Player::checkInputs(bool *keys) {

    if(keys[GLFW_KEY_W])
        currentSpeed = RUN_SPEED;
    else if(keys[GLFW_KEY_S])
        currentSpeed = -RUN_SPEED;
    else
        currentSpeed = 0.0;


    if(keys[GLFW_KEY_D])
        currentTurnSpeed = -TURN_SPEED;
    else if(keys[GLFW_KEY_A])
        currentTurnSpeed = TURN_SPEED;
    else
        currentTurnSpeed = 0.0;


    if(keys[GLFW_KEY_SPACE])
        jump();

}

void Player::jump() {
    if(!isInAir) {
        upwardsSpeed = JUMP_POWER;
        isInAir = true;
    }
}

Player::~Player() {

}

float Player::getSpeed() {
    return this->RUN_SPEED;
}

float Player::getCurrentTurnSpeed() {
    return this->currentTurnSpeed;
}
