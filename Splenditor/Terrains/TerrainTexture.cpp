//
// Created by jack on 30/11/15.
//

#include "TerrainTexture.h"

TerrainTexture::TerrainTexture(GLuint textureID) {
    this->textureID = textureID;
}

GLuint TerrainTexture::GetTextureID() const {
    return textureID;
}

TerrainTexture::~TerrainTexture() {

}
