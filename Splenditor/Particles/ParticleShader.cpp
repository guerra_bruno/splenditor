//
// Created by jack on 18/02/16.
//

#include "ParticleShader.h"

ParticleShader::ParticleShader(const char *vertex_file_path, const char *fragment_file_path) : ShaderProgram(vertex_file_path, fragment_file_path){
    bindAttributes();
    glLinkProgram(getProgramID());
    getAllUniformLocations();
}


void ParticleShader::loadProjectionMatrix(mat4 projectionMatrix) {
    loadMatrix(location_projectionMatrix, projectionMatrix);
}

void ParticleShader::bindAttributes() {
    bindAttribute(0, "position");
    bindAttribute(1, "modelViewMatrix");
    bindAttribute(5, "textOffsets");
    bindAttribute(6, "blendFactor");
}

void ParticleShader::getAllUniformLocations() {
    location_numberOfRows = getUniformLocation("numberOfRows");
    location_projectionMatrix = getUniformLocation("projectionMatrix");
}

void ParticleShader::loadNumberOfRows(float numRows) {
    loadFloat(location_numberOfRows, numRows);
}

