//
// Created by jack on 16/02/16.
//

#ifndef SPLENDITORMASTER_TEXTMESHDATA_H
#define SPLENDITORMASTER_TEXTMESHDATA_H

#include <vector>

using namespace std;


class TextMeshData {
public:
    TextMeshData(vector<float> vertexPositions, vector<float> textureCoords);
    ~TextMeshData();
    vector<float> getVertexPositions() { return vertexPositions; }
    vector<float> getTextureCoords() { return textureCoords; }
    int getVertexCount();
private:
    vector<float> vertexPositions, textureCoords;
};


#endif //SPLENDITORMASTER_TEXTMESHDATA_H
