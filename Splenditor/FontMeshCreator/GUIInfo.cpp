//
// Created by jack on 16/02/16.
//

#include "GUIInfo.h"

GUIInfo::GUIInfo(string text, float fontSize, vec2 position, float maxLineLength, bool centered) {
    this->textString = text;
    this->fontSize = fontSize;

    this->position = position;
    this->lineMaxSize = maxLineLength;
    this->centerText = centered;
}

void GUIInfo::remove() {
}

void GUIInfo::setMeshInfo(GLuint vao, vector<GLuint> vbo, int verticesCount) {
    this->textMeshVao = vao;
    this->textMeshVbos = vbo;
    this->vertexCount = verticesCount;
}

GUIInfo::~GUIInfo() {
}
