//
// Created by jack on 31/01/16.
//

#ifndef SPLENDITORMASTER_METAFILE_H
#define SPLENDITORMASTER_METAFILE_H

#include <Splenditor/ToolBox/common.h>
#include <string>
#include <vector>
#include <fstream>
#include <map>
#include <Splenditor/RenderEngine/DisplayManager.h>
#include <Splenditor/FontMeshCreator/Character.h>


class MetaFile {
public:
    MetaFile(string path, DisplayManager *display);
    ~MetaFile();
    double getSpaceWidth();
    Character* getCharacter(int ascii);
    static constexpr int SPACE_ASCII = 32;
    static constexpr double LINE_HEIGHT = 0.03;
private:
    static constexpr int PAD_TOP = 0;
    static constexpr int PAD_LEFT = 1;
    static constexpr int PAD_BOTTOM = 2;
    static constexpr int PAD_RIGHT = 3;
    static constexpr int DESIRED_PADDING = 8;
    static constexpr char SPLITTER = ' ';
    static constexpr char NUMBER_SEPARATOR = ',';
    double aspectRatio;
    double verticalPerPixelSize;
    double horizontalPerPixelSize;
    double spaceWidth;
    int paddingWidth;
    int paddingHeight;
    vector<int> padding;
    map<int, Character*> metaData;
    map<string, string> values;
    ifstream file;
    void loadPaddingData();
    void loadLineSizes();
    void loadCharacterData(int imageWidth);
    bool processNextLine();
    int getValueOfVariable(string var);
    vector<int> getValuesOfVariable(string var);
    Character* loadCharacter(int imageSize);
    vector<string> str_split(string s, const char delimiter);

};


#endif //SPLENDITORMASTER_METAFILE_H
