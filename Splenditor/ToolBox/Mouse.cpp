//
// Created by jack on 10/12/15.
//

#include <iostream>
#include "Mouse.h"

Mouse::Mouse(DisplayManager *display) {
    this->display = display;
    for(int i = 0; i < GLFW_MOUSE_BUTTON_LAST; i++) mouse_buttons[i] = false;
    updatePosition();
    old_position = position;
}

Mouse::~Mouse() {
}

bool Mouse::isButtomActive(int buttomCode) {
    return mouse_buttons[buttomCode];
}

void Mouse::setPosition(vec2 position) {
    this->position = position;
}

void Mouse::updatePosition() {
    double x,y;
    glfwGetCursorPos(display->getWindow(), &x, &y);
    position = vec2(x,y);
}

vec2 Mouse::getPosition() {
    updatePosition();
    return position;
}

vec2 Mouse::getOffset() {
    updatePosition();
    vec2 offset = position - old_position;
    old_position = position;
    return offset;
}

void Mouse::setButtom(int code, bool value) {
    mouse_buttons[code] = value;
}

void Mouse::updateAllPosition() {
    updatePosition();
    old_position = position;
}

DisplayManager *Mouse::getDisplay() {
    return display;
}

void Mouse::setWheelOffset(float wheelOffset) {
    this->wheelOffset = wheelOffset;
}

float Mouse::getWheelOffset() {
    return wheelOffset;
}
