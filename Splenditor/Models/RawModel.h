//
// Created by jack on 20/11/15.
//

#ifndef SPLENDITORMASTER_RAWMODEL_H
#define SPLENDITORMASTER_RAWMODEL_H

#include <vector>
#include <GL/glew.h>
#include <glfw3.h>

class RawModel {
public:
    RawModel(int vaoID, int vertexCount);
    ~RawModel();
    GLuint getVaoID() const { return vaoID; }
    int getVertexCount() const { return vertexCount; }

private:
    GLuint vaoID;
    int vertexCount;
};


#endif //SPLENDITORMASTER_RAWMODEL_H
