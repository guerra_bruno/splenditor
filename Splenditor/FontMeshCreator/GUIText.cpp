//
// Created by jack on 17/02/16.
//

#include "GUIText.h"

GUIText::GUIText(Text *text, TextMaster *master) {
    this->text = text;
    this->master = master;
    master->loadText(this->text);
}

GUIText::~GUIText() {
    delete text;
}

void GUIText::remove() {
    this->master->removeText(this->text);
}

GUIText::GUIText(string text, float fontSize, vec2 position, float maxLineLength, FontType* font, TextMaster *master, bool centered){
    this->text = new Text(new GUIInfo(text, fontSize, position, maxLineLength, centered), font);
    this->master = master;
    master->loadText(this->text);
}
