//
// Created by jack on 20/02/16.
//

#ifndef SPLENDITORMASTER_SHADOWSHADER_H
#define SPLENDITORMASTER_SHADOWSHADER_H


#include <Splenditor/RenderEngine/ShaderProgram.h>

class ShadowShader : public ShaderProgram {
public:
    ShadowShader(const char *vertex_file_path, const char *fragment_file_path);
    ~ShadowShader() {}
    void loadMvpMatrix(mat4 mvpMatrix);
protected:
    void bindAttributes();
    void getAllUniformLocations();

private:
    GLint location_mvpMatrix;
};

#endif //SPLENDITORMASTER_SHADOWSHADER_H
