//
// Created by jack on 5/12/15.
//

#ifndef SPLENDITORMASTER_GUIRENDERER_H
#define SPLENDITORMASTER_GUIRENDERER_H


#include <Splenditor/RenderEngine/Loader.h>
#include "GuiTexture.h"
#include "GuiShader.h"

class GuiRenderer {
public:
    GuiRenderer(Loader *loader);
    ~GuiRenderer();
    void render(list<GuiTexture*> guis);

private:
    RawModel *quad;
    GuiShader *shader;
};


#endif //SPLENDITORMASTER_GUIRENDERER_H
