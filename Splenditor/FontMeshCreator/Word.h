//
// Created by jack on 16/02/16.
//

#ifndef SPLENDITORMASTER_WORD_H
#define SPLENDITORMASTER_WORD_H


#include <Splenditor/FontMeshCreator/Character.h>
#include <list>

using namespace std;


class Word {
public:
    Word(double fontSize);
    ~Word();
    double getWidth() const { return width; }
    void addCharacter(Character *character);
    list<Character*> getCharacters() { return characters; }
private:
    list<Character*> characters;
    double width, fontSize;

};


#endif //SPLENDITORMASTER_WORD_H
