//
// Created by jack on 11/03/16.
//

#include "ShadowedTerrainShader.h"

ShadowedTerrainShader::ShadowedTerrainShader(const char *vertex_file_path, const char *fragment_file_path) : TerrainShader(vertex_file_path, fragment_file_path){
    getAllUniformLocations();
}


void ShadowedTerrainShader::connectTextureUnits() {
    this->TerrainShader::connectTextureUnits();
    loadInt(location_shadowMap, 5);
}

void ShadowedTerrainShader::loadMapSize(float mapSize) {
    loadFloat(location_mapSize, mapSize);
}

void ShadowedTerrainShader::loadToShadowMapSpace(mat4 matrix) {
    this->loadMatrix(location_toShadowMapSpace, matrix);
}

void ShadowedTerrainShader::getAllUniformLocations() {
    location_toShadowMapSpace = getUniformLocation("toShadowMapSpace");
    location_shadowMap = getUniformLocation("shadowMap");
    location_mapSize = getUniformLocation("mapSize");
    location_pcfCount = getUniformLocation("pcfCount");
    location_shadowDistance = getUniformLocation("shadowDistance");
}

void ShadowedTerrainShader::loadPCF(int pcf) {
    this->loadInt(location_pcfCount, pcf);
}

void ShadowedTerrainShader::loadShadowDistance(float shadowDistance) {
    this->loadFloat(location_shadowDistance, shadowDistance);
}
