//
// Created by jack on 20/11/15.
//

#include <sstream>
#include "EntityShader.h"


EntityShader::EntityShader(const char *vertex_file_path, const char *fragment_file_path) : ShaderProgram(vertex_file_path, fragment_file_path){
    bindAttributes();
    glLinkProgram(getProgramID());
    getAllUniformLocations();
}

void EntityShader::bindAttributes() {
    this->bindAttribute(0, "position");
    this->bindAttribute(1, "textureCoords");
    this->bindAttribute(2, "normal");
}

void EntityShader::getAllUniformLocations() {
    location_transformationMatrix = getUniformLocation("transformationMatrix");
    location_projectionMatrix = getUniformLocation("projectionMatrix");
    location_viewMatrix = getUniformLocation("viewMatrix");
    location_shineDamper = getUniformLocation("shineDamper");
    location_reflectivity = getUniformLocation("reflectivity");
    location_useFakeLighting = getUniformLocation("useFakeLighting");
    location_skyColour = getUniformLocation("skyColour");
    location_rowNumber = getUniformLocation("rowNumber");
    location_offset = getUniformLocation("offset");
    location_plane = getUniformLocation("plane");

    for(int i=0; i<max_lights; i++){
        ostringstream c,p,a; c << "lightColour[" << i << "]"; p << "lightPosition[" << i << "]"; a << "attenuation[" << i << "]";
        location_lightColour[i] = getUniformLocation(c.str().c_str());
        location_lightPosition[i] = getUniformLocation(p.str().c_str());
        location_attenuation[i] = getUniformLocation(a.str().c_str());
    }


};

void EntityShader::loadTransformationMatrix(mat4 matrix) {
    loadMatrix(location_transformationMatrix, matrix);
}

void EntityShader::loadProjectionMatrix(mat4 matrix) {
    loadMatrix(location_projectionMatrix, matrix);
}

void EntityShader::loadViewMatrix(Camera *camera) {
    loadMatrix(location_viewMatrix, camera->getViewMatrix());
}


void EntityShader::loadLights(vector<Light*> lights) {
    for(int i=0; i<max_lights; i++) {
        if(i < lights.size()){
            load3DVector(location_lightPosition[i], lights[i]->GetPosition());
            load3DVector(location_lightColour[i], lights[i]->GetColour());
            load3DVector(location_attenuation[i], lights[i]->GetAttenuation());
        }
        else{
            load3DVector(location_lightPosition[i], vec3(0.0, 0.0, 0.0));
            load3DVector(location_lightColour[i], vec3(0.0, 0.0, 0.0));
            load3DVector(location_attenuation[i], vec3(1.0, 0.0, 0.0));
        }
    }
}

void EntityShader::loadShineVariables(float damper, float reflectivity) {
    loadFloat(location_shineDamper, damper);
    loadFloat(location_reflectivity, reflectivity);
}

void EntityShader::loadFakeLighting(bool useFake) {
    loadBoolean(location_useFakeLighting, useFake);
}

void EntityShader::loadSkyColour(float r, float g, float b) {
    load3DVector(location_skyColour, vec3(r, g, b));
}

EntityShader::~EntityShader() {

}

void EntityShader::loadRowNumber(int rowNumber) {
    loadFloat(location_rowNumber, (float) rowNumber);
}

void EntityShader::loadOffset(float x, float y) {
    load2DVector(location_offset, vec2(x, y));
}

void EntityShader::loadClipPlane(vec4 plane) {
    load4DVector(location_plane, plane);
}
