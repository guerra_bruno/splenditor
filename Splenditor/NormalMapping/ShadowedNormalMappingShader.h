//
// Created by jack on 12/03/16.
//

#ifndef SPLENDITORMASTER_SHADOWEDNORMALMAPPINGSHADER_H
#define SPLENDITORMASTER_SHADOWEDNORMALMAPPINGSHADER_H


#include "NormalMappingShader.h"

class ShadowedNormalMappingShader : public NormalMappingShader {
public:
    ShadowedNormalMappingShader(const char *vertex_file_path, const char *fragment_file_path);
    ~ShadowedNormalMappingShader();
    void connectTextureUnits();
    void loadShadowDistance(float shadowDistance);
    void loadPCF(int pcf);
    void loadMapSize(float mapSize);
    void loadToShadowMapSpace(mat4 matrix);

protected:
    void getAllUniformLocations();

private:
    GLint location_toShadowMapSpace;
    GLint location_shadowMap;
    GLint location_mapSize;
    GLint location_pcfCount;
    GLint location_shadowDistance;
};



#endif //SPLENDITORMASTER_SHADOWEDNORMALMAPPINGSHADER_H
