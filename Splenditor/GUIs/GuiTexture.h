//
// Created by jack on 5/12/15.
//

#ifndef SPLENDITORMASTER_GUITEXTURE_H
#define SPLENDITORMASTER_GUITEXTURE_H

#include <Splenditor/RenderEngine/DisplayManager.h>
#include <Splenditor/ToolBox/common.h>

class GuiTexture {
public:
    GuiTexture(GLuint texture, vec2 position, vec2 scale) : texture(texture), position(position), scale(scale) { }
    ~GuiTexture() { }
    GLuint getTexture() const { return texture; }
    vec2 getPosition() const { return position; }
    vec2 getScale() const { return scale; }
    void setTexture(GLuint texture) { this->texture = texture; }
    void setPosition(vec2 position) { this->position = position; }
    void setScale(vec2 scale) { this->scale = scale; }
private:
    GLuint texture;
    vec2 position;
    vec2 scale;
};


#endif //SPLENDITORMASTER_GUITEXTURE_H
