//
// Created by jack on 12/01/16.
//

#ifndef SPLENDITORMASTER_WATERTILE_H
#define SPLENDITORMASTER_WATERTILE_H


#include <Splenditor/ToolBox/common.h>

class WaterTile {
public:
    WaterTile(vec3 position);
    ~WaterTile();
    float GetTILE_SIZE() const;
    float getHeight() const;
    vec3 getPosition();
private:
    static constexpr float TILE_SIZE = 4 * 60.0;
    vec3 position;
};


#endif //SPLENDITORMASTER_WATERTILE_H
