//
// Created by jack on 15/02/16.
//

#ifndef SPLENDITORMASTER_CHARACTER_H
#define SPLENDITORMASTER_CHARACTER_H

#include <cstdio>
using namespace std;

class Character {

public:

    Character(int id, double xTextureCoord, double yTextureCoord, double xTexSize, double yTexSize,
              double xOffset, double yOffset, double sizeX, double sizeY, double xAdvance) :
            id(id), xTextureCoord(xTextureCoord), yTextureCoord(yTextureCoord), xOffset(xOffset), yOffset(yOffset),
            sizeX(sizeX), sizeY(sizeY), xMaxTextureCoord(xTexSize + xTextureCoord), yMaxTextureCoord(yTexSize + yTextureCoord),
            xAdvance(xAdvance){}

    Character(){
        id = 0;
        xTextureCoord = 0;
        yTextureCoord= 0;
        xMaxTextureCoord= 0;
        yMaxTextureCoord= 0;
        xOffset= 0;
        yOffset= 0;
        sizeX= 0;
        sizeY= 0;
        xAdvance= 0;
    }

    ~Character(){
    }

    int getId() {
        return id;
    }
    double getXTextureCoord() {
        return xTextureCoord;
    }

    double getYTextureCoord() {
        return yTextureCoord;
    }

    double getXMaxTextureCoord() {
        return xMaxTextureCoord;
    }

    double getYMaxTextureCoord() {
        return yMaxTextureCoord;
    }

    double getXOffset() {
        return xOffset;
    }

    double getYOffset() {
        return yOffset;
    }

    double getSizeX() {
        return sizeX;
    }

    double getSizeY() {
        return sizeY;
    }

    double getXAdvance() {
        return xAdvance;
    }



private:

    int id;
    double xTextureCoord;
    double yTextureCoord;
    double xMaxTextureCoord;
    double yMaxTextureCoord;
    double xOffset;
    double yOffset;
    double sizeX;
    double sizeY;
    double xAdvance;
};


#endif //SPLENDITORMASTER_CHARACTER_H
