//
// Created by jack on 26/11/15.
//

#include "Camera.h"
#define toRadian(x) ((x)*M_PI/180.0)

Camera::Camera(Player *player, Mouse *mouse, Terrain* terrain) {
    position = prediction_front = prediction_back = vec3(0.0,3.0,0.0);
    this->player = player;
    this->mouse = mouse;
    this->pitch = terrain->getHeightOfTerrain(this->position.x, this->position.z) + 10;
}

float Camera::GetYaw()  {
    return yaw;
}

float Camera::GetPitch() {
    return pitch;
}

vec3 Camera::GetPosition() {
    return position;
}

void Camera::calculateZoom() {
    double zoomLevel = mouse->getWheelOffset() * 5;
    distanceFromPlayer -= zoomLevel;
    if(distanceFromPlayer < 40)  distanceFromPlayer = 40;  else if(distanceFromPlayer > 150)  distanceFromPlayer = 150;
}


void Camera::calculateAnglePitch(double delta) {
    mouse->updatePosition();
    vec2 offset = mouse->getOffset();
    pitch -=  10 * delta * offset.y;
    angleAroundPlayer -= 10 * delta * offset.x;

    if(pitch < 3)  pitch = 3;  else if(pitch > 90)  pitch = 90;
}

void Camera::move(Terrain* terrain, double delta) {


    camera_collision(-20, prediction_front, terrain);
    camera_collision(20, prediction_back, terrain);
    camera_collision(0, position, terrain);

    if(mouse->isButtomActive(GLFW_MOUSE_BUTTON_LEFT))
        calculateAnglePitch(delta);

    position = calculateCameraPosition(calculateHorizontalDistance(0), calculateVerticalDistance(0));
    yaw = 180 - (player->GetRy() + angleAroundPlayer);


}



float Camera::calculateHorizontalDistance(int prediction) {
    float dist = (float) ((distanceFromPlayer + prediction) * cos(toRadian(pitch)));
    //return (dist < 0) ? 0 : dist;
    return dist;
}

float Camera::calculateVerticalDistance(int prediction) {
    float dist = (float) ((distanceFromPlayer + prediction) * sin(toRadian(pitch)));
    //return (dist < 0) ? 0 : dist;
    return  dist;
}

vec3 Camera::calculateCameraPosition(float horizDistance, float verticDistance) {
    float theta = player->GetRy() + angleAroundPlayer;
    float offsetx = (float) (horizDistance * sin(toRadian(theta)));
    float offsetz = (float) (horizDistance * cos(toRadian(theta)));

    return vec3(player->GetPosition().x - offsetx, player->GetPosition().y + verticDistance, player->GetPosition().z - offsetz);
}

Camera::~Camera() {
}


void Camera::camera_collision(int prediction, vec3 pos, Terrain* terrain) {
    pos = calculateCameraPosition(calculateHorizontalDistance(prediction), calculateVerticalDistance(prediction));
    float collision = terrain->getHeightOfTerrain(pos.x, pos.z);

    if(collision + 2 > pos.y) {
        float new_pitch_offset = pitch + (collision - pos.y ) + 2;
        if (pitch < abs(new_pitch_offset)) pitch += abs(new_pitch_offset)/(player->getSpeed());
    }
}

void Camera::invertPitch() {
    this->pitch = -this->pitch;
}

mat4 Camera::getViewMatrix() {
    return this->viewMatrix;
}

void Camera::calculateViewMatrix() {
    viewMatrix = createViewMatrix(pitch, yaw, position);
}

void Camera::setViewMatrix(mat4 view) {
    this->viewMatrix = view;
}
