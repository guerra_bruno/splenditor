//
// Created by jack on 27/11/15.
//

#ifndef SPLENDITORMASTER_TERRAIN_H
#define SPLENDITORMASTER_TERRAIN_H


#include <Splenditor/Models/RawModel.h>
#include <Splenditor/RenderEngine/Loader.h>
#include <Splenditor/ToolBox/common.h>
#include "TerrainTexturePack.h"
#include "TerrainTexture.h"

class Terrain {
public:
    Terrain(glm::vec2 grid, Loader *loader, TerrainTexturePack *texturePack, TerrainTexture* blendMap, string heightMap);
    ~Terrain();
    RawModel* getModel() const;
    TerrainTexturePack* geTexturePack() const;
    TerrainTexture* getBlendMap() const;
    float getHeightOfTerrain(float worldX, float worldZ);
    float getHeight(int x, int z, int size, float** image);
    float getZ() const;
    float getX() const;
private:
    glm::vec3 calculateNormal(int x, int z, int size, float** image);
    RawModel* generateTerrain(Loader *loader, string fileName);
    RawModel *model;
    TerrainTexturePack *texturePack;
    TerrainTexture *blendMap;
    const static int MAX_HEIGHT = 40, MAX_PIXEL_COLOR = 256, SIZE = 800;
    float x,z;
    float** heightMap;
    int size, vertex_count;
};


#endif //SPLENDITORMASTER_TERRAIN_H
