//
// Created by jack on 8/12/15.
//

#include <Splenditor/ToolBox/common.h>
#include "SkyboxShader.h"



SkyboxShader::SkyboxShader(const char *vertex_file_path, const char *fragment_file_path) : ShaderProgram(vertex_file_path, fragment_file_path){
    bindAttributes();
    glLinkProgram(getProgramID());
    getAllUniformLocations();
    rotation = 0.0;
}

void SkyboxShader::loadProjectionMatrix(mat4 matrix) {
    loadMatrix(location_projectionMatrix, matrix);
}

void SkyboxShader::loadViewMatrix(Camera *camera, double delta) {
    mat4 viewMatrix = camera->getViewMatrix();
    viewMatrix[3][0] = viewMatrix[3][1] = viewMatrix[3][2] = 0;
    rotation += ROTATE_SPEED * delta;
    viewMatrix *= rotate(mat4(1.0), (float) radians(rotation), vec3(0.0, 1.0, 0.0));
    loadMatrix(location_viewMatrix, viewMatrix);
}

void SkyboxShader::getAllUniformLocations() {
    location_projectionMatrix = getUniformLocation("projectionMatrix");
    location_viewMatrix = getUniformLocation("viewMatrix");
    location_fogColour = getUniformLocation("fogColour");
    location_blendFactor = getUniformLocation("blendFactor");
    location_cubeMap1 = getUniformLocation("cubeMap1");
    location_cubeMap2 = getUniformLocation("cubeMap2");
}

void SkyboxShader::bindAttributes() {
    bindAttribute(0, "position");
}

SkyboxShader::~SkyboxShader() {

}

void SkyboxShader::loadFogColour(float r, float g, float b) {
    load3DVector(location_fogColour, vec3(r,g,b));
}

void SkyboxShader::loadBlendFactor(float blend) {
    loadFloat(location_blendFactor, blend);
}

void SkyboxShader::connectTextureUnits() {
    loadInt(location_cubeMap1, 0);
    loadInt(location_cubeMap2, 1);
}
