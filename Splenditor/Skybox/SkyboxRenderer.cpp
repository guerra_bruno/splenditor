//
// Created by jack on 8/12/15.
//

#include "SkyboxRenderer.h"

SkyboxRenderer::SkyboxRenderer(Loader *loader, mat4 projectionMatrix) {


    vertices = {
            -SIZE, SIZE, -SIZE,
            -SIZE, -SIZE, -SIZE,
            SIZE, -SIZE, -SIZE,
            SIZE, -SIZE, -SIZE,
            SIZE, SIZE, -SIZE,
            -SIZE, SIZE, -SIZE,

            -SIZE, -SIZE, SIZE,
            -SIZE, -SIZE, -SIZE,
            -SIZE, SIZE, -SIZE,
            -SIZE, SIZE, -SIZE,
            -SIZE, SIZE, SIZE,
            -SIZE, -SIZE, SIZE,

            SIZE, -SIZE, -SIZE,
            SIZE, -SIZE, SIZE,
            SIZE, SIZE, SIZE,
            SIZE, SIZE, SIZE,
            SIZE, SIZE, -SIZE,
            SIZE, -SIZE, -SIZE,

            -SIZE, -SIZE, SIZE,
            -SIZE, SIZE, SIZE,
            SIZE, SIZE, SIZE,
            SIZE, SIZE, SIZE,
            SIZE, -SIZE, SIZE,
            -SIZE, -SIZE, SIZE,

            -SIZE, SIZE, -SIZE,
            SIZE, SIZE, -SIZE,
            SIZE, SIZE, SIZE,
            SIZE, SIZE, SIZE,
            -SIZE, SIZE, SIZE,
            -SIZE, SIZE, -SIZE,

            -SIZE, -SIZE, -SIZE,
            -SIZE, -SIZE, SIZE,
            SIZE, -SIZE, -SIZE,
            SIZE, -SIZE, -SIZE,
            -SIZE, -SIZE, SIZE,
            SIZE, -SIZE, SIZE
    };

    textures_day.resize(6);
    textures_day[0] = "res/lake_house/files/skybox/day/pz.jpg";
    textures_day[1] = "res/lake_house/files/skybox/day/nz.jpg";
    textures_day[2] = "res/lake_house/files/skybox/day/py.jpg";
    textures_day[3] = "res/lake_house/files/skybox/day/ny.jpg";
    textures_day[4] = "res/lake_house/files/skybox/day/px.jpg";
    textures_day[5] = "res/lake_house/files/skybox/day/nx.jpg";

    textures_night.resize(6);
    textures_night[0] = "res/lake_house/files/skybox/night/pz.tga";
    textures_night[1] = "res/lake_house/files/skybox/night/nz.tga";
    textures_night[2] = "res/lake_house/files/skybox/night/py.tga";
    textures_night[3] = "res/lake_house/files/skybox/night/ny.tga";
    textures_night[4] = "res/lake_house/files/skybox/night/px.tga";
    textures_night[5] = "res/lake_house/files/skybox/night/nx.tga";

    blendFactor = 0.0;
    cube = loader->loadToVAO(vertices, 3);
    dayTextureID = loader->loadCubeMap(textures_day);
    nightTextureID = loader->loadCubeMap(textures_night);
    shader = new SkyboxShader("Shaders/skyboxVertexShader.glsl", "Shaders/skyboxFragmentShader.glsl");
    shader->start();
    shader->loadProjectionMatrix(projectionMatrix);
    shader->connectTextureUnits();
    shader->stop();
}

SkyboxRenderer::~SkyboxRenderer() {
    delete cube;
    delete shader;
    vertices.clear();
    textures_day.clear();
    textures_night.clear();
}

void SkyboxRenderer::render(Camera *camera, double delta, bool day, float r, float g, float b) {

    if(!day && blendFactor < 1.0){
        blendFactor += SUNSET_TRANSITION * delta;
        if (blendFactor > 1.0) blendFactor = 1.0;
    }
    else if(day && blendFactor > 0.0){
        blendFactor -= SUNSET_TRANSITION * delta;
        if (blendFactor < 0.0) blendFactor = 0.0;
    }


    shader->start();
    shader->loadViewMatrix(camera, delta);
    shader->loadFogColour(r, g, b);
    glBindVertexArray(cube->getVaoID());
    bindTextures(blendFactor);
    glEnableVertexAttribArray(0);
    glDrawArrays(GL_TRIANGLES, 0, cube->getVertexCount());
    glDisableVertexAttribArray(0);
    glBindVertexArray(0);
    shader->stop();
}

void SkyboxRenderer::bindTextures(float blendFactor) {
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, dayTextureID);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_CUBE_MAP, nightTextureID);

    shader->loadBlendFactor(blendFactor);
}
