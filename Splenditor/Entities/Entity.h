//
// Created by jack on 26/11/15.
//

#ifndef SPLENDITORMASTER_ENTITY_H
#define SPLENDITORMASTER_ENTITY_H

#include <string>

#include "../Models/TexturedModel.h"
#include "../ToolBox/common.h"

using namespace std;

class Entity {
public:
    Entity(TexturedModel *model, vec3 position, float rx, float ry, float rz, float scale, string name);
    Entity(TexturedModel *model, int index, vec3 position, float rx, float ry, float rz, float scale, string name);
    Entity(Entity *entity);
    Entity(Entity *entity, vec3 position);
    ~Entity();
    float GetScale() const;
    float GetRz() const;
    float GetRy() const;
    float GetRx() const;
    float getTextureXOffset();
    float getTextureYOffset();
    void SetScale(float scale);
    void SetRz(float rz);
    void SetRy(float ry);
    void SetRx(float rx);
    void SetPosition(vec3 position);
    void SetPositionY(float y);
    void SetModel(TexturedModel* model);
    void increasePosition(float dx, float dy, float dz);
    void increaseRotation(float dx, float dy, float dz);
    void PrintWorldObject() const;
    vec3 GetPosition() const;
    TexturedModel* GetModel() const;
    string GetName() const;

    int textureIndex;
private:
    TexturedModel *model;
    vec3 position;
    float rx, ry, rz;
    float scale;
    string name;
};


#endif //SPLENDITORMASTER_ENTITY_H
