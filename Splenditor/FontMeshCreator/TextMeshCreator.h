//
// Created by jack on 16/02/16.
//

#ifndef SPLENDITORMASTER_TEXTMESHCREATOR_H
#define SPLENDITORMASTER_TEXTMESHCREATOR_H


#include <Splenditor/FontMeshCreator/MetaFile.h>
#include <Splenditor/FontMeshCreator/TextMeshData.h>
#include "GUIInfo.h"
#include <Splenditor/FontMeshCreator/Line.h>
#include <Splenditor/FontMeshCreator/Word.h>
#include <list>
#include <vector>

using namespace std;

class TextMeshCreator {
public:
    TextMeshCreator();
    TextMeshCreator(string metaFile, DisplayManager* display);
    ~TextMeshCreator();
    TextMeshData* createTextMesh(GUIInfo * text);

private:
    MetaFile* metaData;
    list<Line> createStructure(GUIInfo * text);
    void completeStructure(list<Line>& lines, Line& currentLine, Word& currentWord, GUIInfo * text);
    TextMeshData* createQuadVertices(GUIInfo* text, list<Line>& lines);
    void addVerticesForCharacter(double curserX, double curserY, Character& character, double fontSize, list<float>& vertices);
    void addVertices(list<float>& vertices, double x, double y, double maxX, double maxY);
    void addTexCoords(list<float>& texCoords, double x, double y, double maxX, double maxY);

};


#endif //SPLENDITORMASTER_TEXTMESHCREATOR_H
