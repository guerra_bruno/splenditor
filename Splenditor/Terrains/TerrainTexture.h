//
// Created by jack on 30/11/15.
//

#ifndef SPLENDITORMASTER_TERRAINTEXTURE_H
#define SPLENDITORMASTER_TERRAINTEXTURE_H


#include <GL/glew.h>

class TerrainTexture {
public:
    TerrainTexture(GLuint textureID);
    ~TerrainTexture();
    GLuint GetTextureID() const;
private:
    GLuint textureID;
};


#endif //SPLENDITORMASTER_TERRAINTEXTURE_H
