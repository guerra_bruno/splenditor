//
// Created by jack on 27/11/15.
//

#ifndef SPLENDITORMASTER_MASTERRENDERER_H
#define SPLENDITORMASTER_MASTERRENDERER_H


#include <map>
#include <iostream>
#include <Splenditor/Skybox/SkyboxRenderer.h>
#include <Splenditor/ToolBox/World.h>
#include <Splenditor/NormalMapping/NormalMappingRenderer.h>
#include <Splenditor/Shadows/ShadowMapMasterRenderer.h>
#include <Splenditor/Entities/EntityRenderer.h>
#include <Splenditor/Terrains/TerrainRenderer.h>
#include <Splenditor/ToolBox/Light.h>

using namespace std;

class MasterRenderer {
public:
    MasterRenderer();
    MasterRenderer(DisplayManager* display, World *world) ;
    ~MasterRenderer();
    void renderScene(World* world, vec4 clipPlane, double delta, bool day, bool refractive, bool reflective);
    void render(World *world, vec4 clipPlane, double delta, bool day, bool water);
    void renderShadowMap(std::list<Entity*> entityList, Light* sun);
    void processEntity(Entity *entity);
    void processNormalMapEntity(Entity *entity);
    void processTerrain(Terrain *terrain);
    void prepare();
    void createProjectionMatrix();
    mat4 getProjectionMatrix();
    vec3 rbg_OpenGl(vec3 colour);
    GLuint getShadowMapTexture();
    float FOV, NEAR_PLANE, FAR_PLANE;
    int PCF;
    float shadowResolution;
private:
    EntityRenderer *renderer;
    TerrainRenderer *terrainRenderer;
    NormalMappingRenderer *normalMapRenderer;
    ShadowMapMasterRenderer* shadowMapRenderer;
    SkyboxRenderer *skyboxRenderer;
    map<TexturedModel*, std::list<Entity*> > entities;
    map<TexturedModel*, std::list<Entity*> > normalMapEntities;
    vector<Terrain*> terrains;
    mat4 projectionMatrix;
    int width, height;
    vec3 colour;

};


#endif //SPLENDITORMASTER_MASTERRENDERER_H
