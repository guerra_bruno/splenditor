//
// Created by jack on 16/02/16.
//

#include <Splenditor/FontMeshCreator/Line.h>

Line::Line(double spaceWidth, double fontSize, double maxLength) {
    this->currentLineLength = 0;
    this->spaceSize = spaceWidth * fontSize;
    this->maxLength = maxLength;
}

bool Line::attemptToAddWord(Word word) {
    double additionalLength = word.getWidth();
    if(!words.empty()) additionalLength +=  spaceSize; else  additionalLength +=  0;
    if (currentLineLength + additionalLength <= maxLength) {
        words.push_back(word);
        currentLineLength += additionalLength;
        return true;
    } else {
        return false;
    }
}
