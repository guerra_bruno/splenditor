//
// Created by jack on 12/01/16.
//

#include "WaterTile.h"


WaterTile::WaterTile(vec3 position) {
    this->position = position;
}

float WaterTile::GetTILE_SIZE() const {
    return TILE_SIZE;
}

vec3 WaterTile::getPosition() {
    return position;
}

WaterTile::~WaterTile() {

}

float WaterTile::getHeight() const {
    return position.y;
}
