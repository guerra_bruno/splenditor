//
// Created by jack on 5/12/15.
//

#include <GL/glew.h>
#include "GuiRenderer.h"

GuiRenderer::GuiRenderer(Loader *loader) {
    float v[8] = {-1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -1.0f};
    vector<float> positions(v, v + sizeof(v) / sizeof(v[0]) );
    this->quad = loader->loadToVAO(positions, 2);
    this->shader = new GuiShader("Shaders/guiVertexShader.glsl", "Shaders/guiFragmentShader.glsl");
}

GuiRenderer::~GuiRenderer() {
    delete quad;
    delete shader;
}

void GuiRenderer::render(std::list<GuiTexture*> guis) {
    shader->start();
    glBindVertexArray(quad->getVaoID());
    glEnableVertexAttribArray(0);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_DEPTH_TEST);
    for(auto& gui : guis){
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, gui->getTexture());
        shader->loadTransformationMatrix(createTransformation(gui->getPosition(), gui->getScale()));
        glDrawArrays(GL_TRIANGLE_STRIP, 0, quad->getVertexCount());
    }
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);
    glDisableVertexAttribArray(0);
    glBindVertexArray(0);
    shader->stop();
}
