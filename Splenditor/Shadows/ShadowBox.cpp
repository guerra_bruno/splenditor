//
// Created by jack on 22/02/16.
//

#include "ShadowBox.h"

ShadowBox::ShadowBox(Camera *camera, float NEAR_PLANE, float FOV, mat4 *lightViewMatrix, DisplayManager *display) {
    this->display = display;
    this->NEAR_PLANE = NEAR_PLANE;
    this->FOV = FOV;
    this->OFFSET = 10;
    this->UP = vec4(0.0, 1.0, 0.0, 0.0);
    this->FORWARD = vec4(-1.0, 0.0, 0.0, 0.0);
    this->SHADOW_DISTANCE = 250;
    this->lightViewMatrix = lightViewMatrix;
    this->cam = camera;
    calculateWidthsAndHeights();
}

void ShadowBox::update() {
    mat4 rotation = calculateCameraRotationMatrix();
    vec3 forwardVector = vec3(FORWARD * rotation);

    vec3 toFar = vec3(forwardVector);
    toFar *= SHADOW_DISTANCE;
    vec3 toNear = vec3(forwardVector);
    toNear *= NEAR_PLANE;
    vec3 centerNear = toNear + cam->GetPosition();
    vec3 centerFar = toFar + cam->GetPosition();

    vector<vec4> points = calculateFrustumVertices(rotation, forwardVector, centerNear, centerFar);

    bool first = true;
    for(auto& point : points){
        if (first) {
            minX = point.x;
            maxX = point.x;
            minY = point.y;
            maxY = point.y;
            minZ = point.z;
            maxZ = point.z;
            first = false;
            continue;
        }
        if (point.x > maxX) {
            maxX = point.x;
        } else if (point.x < minX) {
            minX = point.x;
        }
        if (point.y > maxY) {
            maxY = point.y;
        } else if (point.y < minY) {
            minY = point.y;
        }
        if (point.z > maxZ) {
            maxZ = point.z;
        } else if (point.z < minZ) {
            minZ = point.z;
        }
    }
    maxZ += OFFSET;

}

vec3 ShadowBox::getCenter() {
    float x = (minX + maxX) / 2.0f;
    float y = (minY + maxY) / 2.0f;
    float z = (minZ + maxZ) / 2.0f;
    vec4 cen = vec4(x, y, z, 1.0);
    mat4 invertedLight = inverse(*lightViewMatrix);
    return vec3(cen * invertedLight);
}

vector<vec4> ShadowBox::calculateFrustumVertices(mat4 rotation, vec3 forwardVector, vec3 centerNear, vec3 centerFar) {

    vec3 upVector    = vec3(UP * rotation);
    vec3 rightVector = cross(upVector, forwardVector);
    vec3 downVector  = -upVector;
    vec3 leftVector  = -rightVector;

    vec3 farTop     = centerFar + (upVector     * farHeight);
    vec3 farBottom  = centerFar + (downVector   * farHeight);
    vec3 nearTop    = centerFar + (upVector     * nearHeight);
    vec3 nearBottom = centerFar + (downVector   * nearHeight);

    vector<vec4> points(8);

    points[0] = calculateLightSpaceFrustumCorner(farTop, rightVector, farWidth);
    points[1] = calculateLightSpaceFrustumCorner(farTop, leftVector, farWidth);
    points[2] = calculateLightSpaceFrustumCorner(farBottom, rightVector, farWidth);
    points[3] = calculateLightSpaceFrustumCorner(farBottom, leftVector, farWidth);
    points[4] = calculateLightSpaceFrustumCorner(nearTop, rightVector, nearWidth);
    points[5] = calculateLightSpaceFrustumCorner(nearTop, leftVector, nearWidth);
    points[6] = calculateLightSpaceFrustumCorner(nearBottom, rightVector, nearWidth);
    points[7] = calculateLightSpaceFrustumCorner(nearBottom, leftVector, nearWidth);

    return points;
}

vec4 ShadowBox::calculateLightSpaceFrustumCorner(vec3 startPoint, vec3 direction, float width) {
    vec3 point3D = startPoint + (direction * width);
    //vec4 point4D = vec4(point3D.x, point3D.y, point3D.z, 1.0f);
    vec4 point4D = vec4(point3D, 1.0f);

    return point4D * (*lightViewMatrix);

}

mat4 ShadowBox::calculateCameraRotationMatrix() {
    mat4 rotation = mat4(1.0f);
    rotation = rotate(rotation, cam->GetYaw(), vec3(0.0f, 1.0f, 0.0f));
    rotation = rotate(rotation, -cam->GetPitch(), vec3(1.0f, 0.0f, 0.0f));
    return rotation;
}

void ShadowBox::calculateWidthsAndHeights() {
    farWidth   = SHADOW_DISTANCE * tan(radians(FOV));
    nearWidth  = NEAR_PLANE      * tan(radians(FOV));
    farHeight  = farWidth  / getAspectRatio();
    nearHeight = nearWidth / getAspectRatio();
}

float ShadowBox::getAspectRatio() {
    return display->getWidth() / display->getHeight();
}
