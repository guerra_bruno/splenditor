//
// Created by jack on 20/11/15.
//

#ifndef SPLENDITORMASTER_LOADER_H
#define SPLENDITORMASTER_LOADER_H


#include <fstream>
#include <iostream>
#include <vector>
#include <list>
#include "GL/glew.h"
#include "../Models/RawModel.h"


using namespace std;

class Loader {

public:
    Loader();
    ~Loader();
    vector<GLuint> loadToVAO(vector<float> positions, vector<float> textureCoords);
    RawModel* loadToVAO(vector<float> positions, vector<float> textureCoords, vector<float> normals, vector<unsigned int> indices);
    RawModel* loadToVAO(vector<float> positions, vector<float> textureCoords, vector<float> normals, vector<float> tangents, vector<unsigned int> indices);
    RawModel* loadToVAO(vector<float> positions, int dimensions);
    GLuint createVAO();
    int unbindVAO();
    unsigned int storeDataInAttributeList(int attributeNumber, int coordinateSize, vector<float> data);
    unsigned int createEmptyVbo(int floatCount);
    void updateVbo(int vbo, vector<float> &data);
    void bindIndicesBuffer(vector<unsigned int> indices);
    void deleteVBO(GLuint vbo);
    void deleteVAO(GLuint vao);
    void addInstancedAttribute(int vao, int vbo, int attribute, int dataSize, int instancedDataLength, int offset);
    GLuint loadTexture(string fileName);
    GLuint loadFontTexture(string fileName);
    GLuint loadCubeMap(vector<string> texturesFiles);
private:
    list<GLuint> vaos;
    list<GLuint> vbos;
    list<GLuint> textures;

};


#endif //SPLENDITORMASTER_LOADER_H
