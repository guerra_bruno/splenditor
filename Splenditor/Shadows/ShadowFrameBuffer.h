//
// Created by jack on 20/02/16.
//

#ifndef SPLENDITORMASTER_SHADOWFRAMEBUFFER_H
#define SPLENDITORMASTER_SHADOWFRAMEBUFFER_H


#include <Splenditor/RenderEngine/DisplayManager.h>

class ShadowFrameBuffer {
public:
    ShadowFrameBuffer(int width, int height, DisplayManager* display);
    ~ShadowFrameBuffer();
    void initialiseFrameBuffer();
    void bindFrameBuffer();
    void bindFrameBuffer(GLuint &frameBuffer, int width, int height);
    void unbindFrameBuffer();
    GLuint getShadowMap() { return shadowMap; }
    void createFrameBuffer(GLuint &frameBuffer);
    void createDepthBufferAttachment(int width, int height, GLuint &texture);



private:
    int WIDTH, HEIGHT;
    GLuint fbo, shadowMap;
    DisplayManager* display;
};


#endif //SPLENDITORMASTER_SHADOWFRAMEBUFFER_H
