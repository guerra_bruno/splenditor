//
// Created by jack on 12/01/16.
//

#ifndef SPLENDITORMASTER_WATERRENDERER_H
#define SPLENDITORMASTER_WATERRENDERER_H

#include <Splenditor/Models/RawModel.h>
#include <Splenditor/RenderEngine/Loader.h>
#include "WaterShader.h"
#include "WaterRenderer.h"
#include "WaterTile.h"
#include "WaterFrameBuffers.h"
#include <Splenditor/ToolBox/common.h>
#include <Splenditor/ToolBox/World.h>
#define WAVE_SPEED 0.015;

class WaterRenderer {
public:
    WaterRenderer(Loader *loader, mat4 projectionMatrix, WaterFrameBuffers* fbos);
    ~WaterRenderer();
    void prepareRender(World* world, Camera* camera);
    void render(World* world, Camera *camera);
    void unbind();
    WaterFrameBuffers* fbos;

private:
    void setUpVAO(Loader *loader);
    RawModel* quad;
    WaterShader* shader;
    float moveFactor;

};


#endif //SPLENDITORMASTER_WATERRENDERER_H
