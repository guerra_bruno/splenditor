//
// Created by jack on 20/11/15.
//

#ifndef SPLENDITORMASTER_RENDERER_H
#define SPLENDITORMASTER_RENDERER_H


#include "../Models/TexturedModel.h"
#include "Entity.h"
#include "ShadowedEntityShader.h"
#include <map>
#include <Splenditor/ToolBox/World.h>


class EntityRenderer {
public:
    EntityRenderer(mat4 projectionMatrix);
    ~EntityRenderer();
    void render(map<TexturedModel*, std::list<Entity*> > entities, vec4 clipPlane, vec3 colour, World* world);
    void render(map<TexturedModel*, std::list<Entity*> > entities, vec4 clipPlane, vec3 colour, World* world, float shadowResolution, mat4 toShadowMapSpace, int PCF, float shadowDistance);
    void rendering(map<TexturedModel*, std::list<Entity*> > entities, EntityShader* shader);
    void prepareTexturedModel(TexturedModel *model, EntityShader* shader);
    void unbindTexturedModel();
    void prepareInstance(Entity *entity, EntityShader* shader);
    void prepare(vec4 clipPlane, vec3 colour, World *world, EntityShader* shader);
private:
    ShadowedEntityShader *shadow_entity_shader;
    EntityShader *entity_shader;
};

#endif //SPLENDITORMASTER_RENDERER_H
