//
// Created by jack on 17/02/16.
//

#ifndef SPLENDITORMASTER_TEXT_H
#define SPLENDITORMASTER_TEXT_H

#include <Splenditor/FontMeshCreator/FontType.h>

class Text {

public:
    Text(GUIInfo *guInfo, FontType *font);
    ~Text() { delete guiInfo; }
    GUIInfo *getGuiInfo() { return guiInfo; }
    FontType *getFont() { return font; }

private:
    FontType *font;
    GUIInfo *guiInfo;

};


#endif //SPLENDITORMASTER_TEXT_H
