//
// Created by jack on 11/12/15.
//

#ifndef SPLENDITORMASTER_WORLD_H
#define SPLENDITORMASTER_WORLD_H
#include <string>
#include <picojson.h>
#include <iostream>
#include <fstream>
#include <string>
#include <list>
#include <Splenditor/RenderEngine/Loader.h>
#include <Splenditor/GUIs/GuiTexture.h>
#include <Splenditor/GUIs/GuiRenderer.h>
#include <Splenditor/Water/WaterTile.h>
#include <Splenditor/Models/TexturedModel.h>
#include <Splenditor/Entities/Entity.h>
#include <Splenditor/Entities/Player.h>
#include "common.h"
#include "Mouse.h"
#include "Camera.h"
#include "Light.h"

#define parray picojson::array

using namespace std;
using namespace picojson;



class World {
public:
    World(string worldFile, Mouse* mouse);
    ~World();
    TexturedModel* loadModel(object model, bool lowPoly);
    Entity* loadEntity(object entity, TexturedModel *model, string name);
    Player* loadPlayer(object player, TexturedModel *model, string name);
    void cleanWorld();
    void changeWorld(string world);
    void loadEntities(parray e);
    void loadGuis(parray g);
    void loadLights(parray l);
    void loadTerrain(object t);
    void loadPlayer(object p);
    void loadWaters(object w);
    object o;
    Loader *loader;
    Camera *camera;
    vector<Light*> lights;
    Terrain *terrain;
    Player *player;
    list<TexturedModel *> texturedModels;
    list<Entity *> entities;
    list<Entity *> normalMapEntities;
    list<Entity *> refractiveWaterEntities;
    list<Entity *> reflectiveWaterEntities;
    list<Entity *> lowPoly;
    list<GuiTexture*> guis;
    vector<WaterTile*> waters;
    GLuint water_dudv, water_normal;
    float delta;
    vec3 colour;

};


#endif //SPLENDITORMASTER_WORLD_H
