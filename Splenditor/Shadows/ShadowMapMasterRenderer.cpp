//
// Created by jack on 22/02/16.
//

#include "ShadowMapMasterRenderer.h"
#include "ShadowMapEntityRenderer.h"

ShadowMapMasterRenderer::ShadowMapMasterRenderer(Camera *camera, float resolution, float NEAR_PLANE, float FOV, DisplayManager *display) {
    cam = camera;
    SHADOW_MAP_SIZE = resolution;
    projectionMatrix = mat4(1.0);
    lightViewMatrix = new mat4(1.0);
    projectionViewMatrix = new mat4(1.0);
    offset = createOffset();
    shader = new ShadowShader("Shaders/shadowVertexShader.glsl", "Shaders/shadowFragmentShader.glsl");
    shadowBox = new ShadowBox(camera, NEAR_PLANE, FOV, lightViewMatrix, display);
    shadowFbo = new ShadowFrameBuffer((int) SHADOW_MAP_SIZE, (int) SHADOW_MAP_SIZE, display);
    entityRenderer = new ShadowMapEntityRenderer(shader, projectionViewMatrix);
}

ShadowMapMasterRenderer::~ShadowMapMasterRenderer() {
    delete shader;
    delete shadowBox;
    delete shadowFbo;
    delete lightViewMatrix;
    delete projectionViewMatrix;
}

void ShadowMapMasterRenderer::render(map<TexturedModel *, std::list<Entity *>> entities, Light *sun) {
    //shadowBox->update();
    prepare(-sun->GetPosition(), shadowBox);
    entityRenderer->render(entities);
    finish();
}

mat4 ShadowMapMasterRenderer::getToShadowMapSpaceMatrix() {
    return offset * (*projectionViewMatrix);
}

GLuint ShadowMapMasterRenderer::getShadowMap() {
    return shadowFbo->getShadowMap();
}

mat4 ShadowMapMasterRenderer::getLightSpaceTransform() {
    return *lightViewMatrix;
}

void ShadowMapMasterRenderer::prepare(vec3 lightDirection, ShadowBox *box) {
    updateOrthoProjectionMatrix(box->getWidth(), box->getHeight(), box->getLength());
    updateLightViewMatrix(lightDirection, box->getCenter());
    *projectionViewMatrix = projectionMatrix * (*lightViewMatrix);
    shadowFbo->bindFrameBuffer();
    glEnable(GL_DEPTH_TEST);
    glClear(GL_DEPTH_BUFFER_BIT);
    shader->start();
}

void ShadowMapMasterRenderer::finish() {
    shader->stop();
    shadowFbo->unbindFrameBuffer();
}

void ShadowMapMasterRenderer::updateLightViewMatrix(vec3 direction, vec3 center) {
    direction = normalize(direction);
    //center = -center;
    *lightViewMatrix = mat4(1.0);
    float pitch = degrees(acos(length(vec2(direction.x, direction.z))));
    *lightViewMatrix = rotate(*lightViewMatrix, pitch, vec3(1.0f, 0.0f, 0.0f));
    float yaw = degrees(atan(direction.x / direction.z));
    if (direction.z > 0) yaw -= 180;
    *lightViewMatrix = rotate(*lightViewMatrix, -yaw, vec3(0.0f, 1.0f, 0.0f));
    *lightViewMatrix = translate(*lightViewMatrix, -cam->player->GetPosition());
//    *lightViewMatrix = translate(*lightViewMatrix, center);
}

void ShadowMapMasterRenderer::updateOrthoProjectionMatrix(float width, float height, float length) {
//    projectionMatrix = mat4(0.0);
//    projectionMatrix[0][0] = 2.0f / width;
//    projectionMatrix[1][1] = 2.0f / height;
//    projectionMatrix[2][2] = -2.0f / length;
//    projectionMatrix[3][3] = 1.0;
    shadowDistance = 512 * cam->distanceFromPlayer/120;
    float ort = 512 * cam->distanceFromPlayer/40;
    projectionMatrix = ortho(-(ort)/4,(ort)/4,-ort/4,ort/4, -ort/2, ort/2);

//    projectionMatrix = mat4(1.0f);
//    projectionMatrix[0][0] =  2.0f / width;
//    projectionMatrix[1][1] =  2.0f / height;
//    projectionMatrix[2][2] = -2.0f / length;
//    projectionMatrix[3][3] =  1.0f;
}

mat4 ShadowMapMasterRenderer::createOffset() {
    mat4 offset = mat4(1.0f);
    offset = translate(offset, vec3(0.5f, 0.5f, 0.5f));
    offset = scale(offset, vec3(0.5f, 0.5f, 0.5f));
    return offset;
}
