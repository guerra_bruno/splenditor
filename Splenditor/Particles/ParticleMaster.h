//
// Created by jack on 18/02/16.
//

#ifndef SPLENDITORMASTER_PARTICLEMASTER_H
#define SPLENDITORMASTER_PARTICLEMASTER_H


#include <list>
#include <map>
#include "Particle.h"
#include "ParticleRenderer.h"


bool compareParticle( Particle * const &p1, Particle * const &p2);

class ParticleMaster {
public:
    ParticleMaster(Loader* loader, mat4 projectionMatrix);
    ~ParticleMaster();
    void update(World* world);
    void renderParticles(Camera* camera);
    void addParticle(Particle* particle);


private:
    map<ParticleTexture*, std::list<Particle*>> particles;
    ParticleRenderer* renderer;
};


#endif //SPLENDITORMASTER_PARTICLEMASTER_H
