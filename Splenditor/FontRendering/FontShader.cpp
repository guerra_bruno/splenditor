//
// Created by jack on 15/02/16.
//

#include "FontShader.h"

FontShader::FontShader(const char *vertex_file_path, const char *fragment_file_path) : ShaderProgram(vertex_file_path, fragment_file_path){
    bindAttributes();
    glLinkProgram(getProgramID());
    getAllUniformLocations();
}

void FontShader::bindAttributes() {
    bindAttribute(0, "position");
    bindAttribute(1, "textureCoords ");
}


void FontShader::getAllUniformLocations() {
    location_colour = getUniformLocation("colour");
    location_translation = getUniformLocation("translation");
};

void FontShader::loadColour(vec3 colour) {
    load3DVector(location_colour, colour);
}

void FontShader::loadTranslation(vec2 translation) {
    load2DVector(location_translation, translation);
}
