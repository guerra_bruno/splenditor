//
// Created by jack on 11/12/15.
//


#include "World.h"



#define parray picojson::array
#define for_each_obj(x, i)  for (object::iterator i = x.begin(); i != x.end(); ++i)
#define for_each_array(x, i)  for (parray::iterator i = x.begin(); i != x.end(); ++i)
#define vec3(v) glm::vec3((float) v[0].get<double>(), v[1].get<double>(), v[2].get<double>())
#define vec2(v) glm::vec2((float) v[0].get<double>(), (float) v[1].get<double>())
#define randFloat(LO, HI) (LO + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(HI-LO))))


World::World(string worldFile, Mouse* mouse) {

    this->loader = new Loader();


    ifstream file(worldFile.c_str());
    value world; file >> world;
    object o = world.get<object>();

    // loading terrain
    if(!o["terrain"].is<null>()) loadTerrain(o["terrain"].get<object>());

    // Loading entities
    if(!o["entities"].is<null>()) loadEntities(o["entities"].get<parray>());

    // Loading player
    if(!o["player"].is<null>()) loadPlayer(o["player"].get<object>());

    // Loading guis
    if(!o["guis"].is<null>()) loadGuis(o["guis"].get<parray>());

    // Loading lightsarray
    if(!o["lights"].is<null>()) loadLights(o["lights"].get<parray>());

    // Loading waters
    if(!o["waters"].is<null>()) loadWaters(o["waters"].get<object>());

    // load sky colour
    if(!o["skycolour"].is<null>()) colour = vec3(o["skycolour"].get<parray>());


    this->camera = new Camera(player, mouse, terrain);

    /*
    for(int i=0; i<this->entities.size(); i++){
        vec3 new_pos = entities[i]->GetPosition();
        new_pos.y = terrain->getHeightOfTerrain(new_pos.x, new_pos.z);
        entities[i]->SetPosition(new_pos);
    }
    */

}

TexturedModel *World::loadModel(object model, bool lowPoly) {

    string obj = lowPoly ? model["low-poly"].to_str() : model["obj"].to_str();
    string texture = model["texture"].to_str();
    ModelTexture *tex = new ModelTexture(this->loader->loadTexture(texture));

    if(!model["hasTransparency"].is<null>()) {
        tex->setHasTransparency(!model["hasTransparency"].is<null>());
        cout << texture << endl;
    }
    tex->setUseFakeLighting(!model["useFakeLighting"].is<null>());

    if (model["normal_map"].is<null>())
        return new TexturedModel(loadObjModel(obj, this->loader), tex);
    else {
        tex->setNormalMap(loader->loadTexture(model["normal_map"].get<string>()));
        if (!model["reflectivity"].is<null>()) tex->setReflectivity((float) model["reflectivity"].get<double>());
        if (!model["shineDamper"].is<null>()) tex->setShineDamper((float) model["shineDamper"].get<double>());
        return new TexturedModel(loadObjNormalModel(obj, this->loader), tex);
    }
}

Entity *World::loadEntity(object entity, TexturedModel *model, string name) {
    vec3 position = vec3(entity["position"].get<parray>());
    vec3 rotation = vec3(entity["rotation"].get<parray>());
    float scale = (float) entity["scale"].get<double>();
    return new Entity(model, position, rotation.x, rotation.y, rotation.z, scale, name);
}

Player *World::loadPlayer(object player, TexturedModel *model, string name) {
    vec3 position = vec3(player["position"].get<parray>());
    vec3 rotation = vec3(player["rotation"].get<parray>());
    float scale = (float) player["scale"].get<double>();
    return new Player(model, position, rotation.x, rotation.y, rotation.z, scale, name);
}


void World::loadEntities(parray e) {
    for_each_array(e, i){
        for_each_obj(i->get<object>(), model){
            string name = model->first.c_str();
            object texturedModel = model->second.get<object>();
            TexturedModel* texModel = loadModel(texturedModel, false);
            TexturedModel* texModelLP = loadModel(texturedModel, true);
            for_each_array(model->second.get("entities").get<parray>(), entity) {

                if (!texturedModel["refractive_water"].is<null>()) refractiveWaterEntities.push_back(loadEntity(entity->get<object>(), texModelLP, name));
                if (!texturedModel["reflective_water"].is<null>()) reflectiveWaterEntities.push_back(loadEntity(entity->get<object>(), texModelLP, name));

                if (texturedModel["normal_map"].is<null>())
                    entities.push_back(loadEntity(entity->get<object>(), texModel, name));
                else {
                    texModel->GetTexture()->setShineDamper(10.0);
                    normalMapEntities.push_back(loadEntity(entity->get<object>(), texModel, name));
                }

                lowPoly.push_back(loadEntity(entity->get<object>(), texModelLP, name));

            }
            texturedModels.push_back(texModel);
        }
    }
}

void World::loadLights(parray l) {
    std::list<Light *> l_lights;
    for_each_array(l, i){
        object light = i->get<object>();
        l_lights.push_back(new Light(vec3(light["position"].get<parray>()), vec3(light["colour"].get<parray>()), vec3(light["attenuation"].get<parray>())));
    }
    lights = vector<Light*>{ make_move_iterator(begin(l_lights)), make_move_iterator(end(l_lights)) };
}

void World::loadGuis(parray g) {
    for_each_array(g, i){
        object gui = i->get<object>();
        guis.push_back(new GuiTexture(loader->loadTexture(gui["texture"].to_str()), vec2(gui["position"].get<parray>()), vec2(gui["scale"].get<parray>())));
    }
}

void World::loadTerrain(object t) {
    TerrainTexture *backgroundTexture = new TerrainTexture(loader->loadTexture(t["backgroundTexture"].get<string>()));
    TerrainTexture *rTexture = new TerrainTexture(loader->loadTexture(t["rTexture"].get<string>()));
    TerrainTexture *gTexture = new TerrainTexture(loader->loadTexture(t["gTexture"].get<string>()));
    TerrainTexture *bTexture = new TerrainTexture(loader->loadTexture(t["bTexture"].get<string>()));
    TerrainTexture *blendMap = new TerrainTexture(loader->loadTexture(t["blendMap"].get<string>()));
    TerrainTexturePack *texturePack = new TerrainTexturePack(backgroundTexture, rTexture, gTexture, bTexture);
    this->terrain = new Terrain(vec2(t["coords"].get<parray>()), loader, texturePack, blendMap, t["heightMap"].get<string>());
}

void World::loadPlayer(object p) {
    player = loadPlayer(p, loadModel(p["model"].get<object>(), false), "player");
}

void World::changeWorld(string world) {
    this->cleanWorld();
}

World::~World() {

   this->cleanWorld();

}

void World::loadWaters(object w) {
    std::list<WaterTile *> l_waters;
    water_dudv = loader->loadTexture(w["dudv_map"].get<string>());
    water_normal = loader->loadTexture(w["normal_map"].get<string>());

    for_each_array(w["instances"].get<parray>(), i){
        object position = i->get<object>();
        l_waters.push_back(new WaterTile(vec3(position["position"].get<parray>())));
    }

    waters = vector<WaterTile*>{ make_move_iterator(begin(l_waters)), make_move_iterator(end(l_waters)) };
}

void World::cleanWorld() {
    delete loader;
    delete camera;
    delete terrain;
    delete player;

    for(auto& light : lights) delete light;
    for(auto& model : texturedModels) delete model;
    for(auto& entity : entities) delete entity;
    for(auto& entity : normalMapEntities) delete entity;
    for(auto& entity : refractiveWaterEntities) delete entity;
    for(auto& entity : reflectiveWaterEntities) delete entity;
    for(auto& entity : lowPoly) delete entity;
    for(auto& gui : guis) delete gui;
    for(auto& water : waters) delete water;

    entities.clear();
    normalMapEntities.clear();
    refractiveWaterEntities.clear();
    refractiveWaterEntities.clear();
    guis.clear();
    lights.clear();
    waters.clear();
}
