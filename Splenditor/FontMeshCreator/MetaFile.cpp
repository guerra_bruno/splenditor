//
// Created by jack on 31/01/16.
//

#include <Splenditor/FontMeshCreator/MetaFile.h>

MetaFile::MetaFile(string path, DisplayManager *display) {
    this->aspectRatio = (double) display->getWidth() / (double) display->getHeight();
    file.open(path);
    loadPaddingData();
    loadLineSizes();
    int imageWidth = getValueOfVariable("scaleW");
    loadCharacterData(imageWidth);
    file.close();
}

void MetaFile::loadPaddingData() {
    processNextLine();
    padding = getValuesOfVariable("padding");
    paddingWidth = padding[PAD_LEFT] + padding[PAD_RIGHT];
    paddingHeight = padding[PAD_TOP] + padding[PAD_BOTTOM];
}

void MetaFile::loadLineSizes() {
    processNextLine();
    int lineHeightPixels = getValueOfVariable("lineHeight") - paddingHeight;
    verticalPerPixelSize = LINE_HEIGHT / (double) lineHeightPixels;
    horizontalPerPixelSize = verticalPerPixelSize / aspectRatio;
}

int MetaFile::getValueOfVariable(string var) {
    return stoi(values[var]);
}

void MetaFile::loadCharacterData(int imageWidth) {
    processNextLine();
    processNextLine();
    while (processNextLine()) {
        Character *c = loadCharacter(imageWidth);
        if (c != NULL) {
            metaData[c->getId()] = c;
        }
    }
}

double MetaFile::getSpaceWidth() {
    return spaceWidth;
}

Character *MetaFile::getCharacter(int ascii) {
    return metaData[ascii];
}

bool MetaFile::processNextLine() {
    values.clear();
    string line;
    if(getline (file,line)){
        vector<string> part = str_split(line, SPLITTER);
        for (int i = 0; i < part.size(); ++i) {
            vector<string> valuePairs = str_split(part[i], '=');
            if(valuePairs.size() == 2)
                values[valuePairs[0]] = valuePairs[1];

        }
        return true;
    }
    else
        return false;
}

vector<int> MetaFile::getValuesOfVariable(string var) {
    vector<string> numbers = str_split(values[var], NUMBER_SEPARATOR);
    vector<int> actualValues(numbers.size());
    for (int i = 0; i < actualValues.size(); ++i) {
        actualValues[i] = stoi(numbers[i]);
    }
    return actualValues;
}

Character *MetaFile::loadCharacter(int imageSize) {
    int id = getValueOfVariable("id");
    if (id == SPACE_ASCII) {
        this->spaceWidth = (getValueOfVariable("xadvance") - paddingWidth) * horizontalPerPixelSize;
        return NULL;
    }
    double xTex = ((double) getValueOfVariable("x") + (padding[PAD_LEFT] - DESIRED_PADDING)) / imageSize;
    double yTex = ((double) getValueOfVariable("y") + (padding[PAD_TOP] - DESIRED_PADDING)) / imageSize;
    int width = getValueOfVariable("width") - (paddingWidth - (2 * DESIRED_PADDING));
    int height = getValueOfVariable("height") - ((paddingHeight) - (2 * DESIRED_PADDING));
    double quadWidth = width * horizontalPerPixelSize;
    double quadHeight = height * verticalPerPixelSize;
    double xTexSize = (double) width / imageSize;
    double yTexSize = (double) height / imageSize;
    double xOff = (getValueOfVariable("xoffset") + padding[PAD_LEFT] - DESIRED_PADDING) * horizontalPerPixelSize;
    double yOff = (getValueOfVariable("yoffset") + (padding[PAD_TOP] - DESIRED_PADDING)) * verticalPerPixelSize;
    double xAdvance = (getValueOfVariable("xadvance") - paddingWidth) * horizontalPerPixelSize;
    return (new Character(id, xTex, yTex, xTexSize, yTexSize, xOff, yOff, quadWidth, quadHeight, xAdvance));
}

MetaFile::~MetaFile() {
    for (auto& kv : metaData) {
        delete kv.second;
    }
    metaData.clear();
}

vector<string> MetaFile::str_split(string s, const char delimiter) {
    size_t start=0;
    size_t end=s.find_first_of(delimiter);

    vector<string> output;

    while (end <= string::npos)  {
        output.emplace_back(s.substr(start, end-start));

        if (end == string::npos)
            break;

        start=end+1;
        end = s.find_first_of(delimiter, start);
    }

    return output;
}
