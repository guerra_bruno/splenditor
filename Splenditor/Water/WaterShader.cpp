//
// Created by jack on 12/01/16.
//

#include "WaterShader.h"

WaterShader::WaterShader(const char *vertex_file_path, const char *fragment_file_path) : ShaderProgram(vertex_file_path, fragment_file_path){
    bindAttributes();
    glLinkProgram(getProgramID());
    getAllUniformLocations();
}

WaterShader::~WaterShader() {

}

void WaterShader::loadProjectionMatrix(mat4 matrix) {
    loadMatrix(location_projectionMatrix, matrix);
}

void WaterShader::loadViewMatrix(Camera *camera) {
    loadMatrix(location_viewMatrix, camera->getViewMatrix());
}

void WaterShader::getAllUniformLocations() {
    location_projectionMatrix = getUniformLocation("projectionMatrix");
    location_viewMatrix = getUniformLocation("viewMatrix");
    location_modelMatrix = getUniformLocation("modelMatrix");
    location_reflectionTexture = getUniformLocation("reflectionTexture");
    location_refractionTexture = getUniformLocation("refractionTexture");
    location_dudvMap = getUniformLocation("dudvMap");
    location_normalMap = getUniformLocation("normalMap");
    location_depthMap = getUniformLocation("depthMap");
    location_moveFactor = getUniformLocation("moveFactor");
    location_cameraPosition = getUniformLocation("cameraPosition");

    for(int i=0; i<max_lights; i++){
        ostringstream c,p,a; c << "lightColour[" << i << "]"; p << "lightPosition[" << i << "]"; a << "attenuation[" << i << "]";
        location_lightColour[i] = getUniformLocation(c.str().c_str());
        location_lightPosition[i] = getUniformLocation(p.str().c_str());
        location_attenuation[i] = getUniformLocation(a.str().c_str());
    }

}

void WaterShader::connectTextureUnits() {
    loadInt(location_reflectionTexture, 0);
    loadInt(location_refractionTexture, 1);
    loadInt(location_dudvMap, 2);
    loadInt(location_normalMap, 3);
    loadInt(location_depthMap, 4);
}

void WaterShader::bindAttributes() {
    bindAttribute(0, "position");
}

void WaterShader::loadModelMatrix(mat4 modelMatrix){
    loadMatrix(location_modelMatrix, modelMatrix);
}

void WaterShader::loadMoveFactor(float moveFactor) {
    loadFloat(location_moveFactor, moveFactor);
}

void WaterShader::loadCameraPosition(vec3 position) {
    load3DVector(location_cameraPosition, position);
}

void WaterShader::loadLights(vector<Light*> lights) {
    for(int i=0; i<max_lights; i++) {
        if(i < lights.size()){
            load3DVector(location_lightPosition[i], lights[i]->GetPosition());
            load3DVector(location_lightColour[i], lights[i]->GetColour());
            load3DVector(location_attenuation[i], lights[i]->GetAttenuation());
        }
        else{
            load3DVector(location_lightPosition[i], vec3(0.0, 0.0, 0.0));
            load3DVector(location_lightColour[i], vec3(0.0, 0.0, 0.0));
            load3DVector(location_attenuation[i], vec3(1.0, 0.0, 0.0));
        }
    }
}