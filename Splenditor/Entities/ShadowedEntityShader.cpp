//
// Created by jack on 11/03/16.
//

#include "ShadowedEntityShader.h"

ShadowedEntityShader::ShadowedEntityShader(const char *vertex_file_path, const char *fragment_file_path) : EntityShader(vertex_file_path, fragment_file_path){
    getAllUniformLocations();
}


ShadowedEntityShader::~ShadowedEntityShader() {
}


void ShadowedEntityShader::getAllUniformLocations() {
    location_textureSampler = getUniformLocation("textureSampler");
    location_toShadowMapSpace = getUniformLocation("toShadowMapSpace");
    location_shadowMap = getUniformLocation("shadowMap");
    location_mapSize = getUniformLocation("mapSize");
    location_pcfCount = getUniformLocation("pcfCount");
    location_shadowDistance = getUniformLocation("shadowDistance");
}


void ShadowedEntityShader::connectTextureUnits() {
    loadInt(location_textureSampler, 0);
    loadInt(location_shadowMap, 5);
}

void ShadowedEntityShader::loadToShadowMapSpace(mat4 matrix) {
    this->loadMatrix(location_toShadowMapSpace, matrix);
}

void ShadowedEntityShader::loadMapSize(float mapSize) {
    loadFloat(location_mapSize, mapSize);
}

void ShadowedEntityShader::loadPCF(int pcf) {
    this->loadInt(location_pcfCount, pcf);

}

void ShadowedEntityShader::loadShadowDistance(float shadowDistance) {
    this->loadFloat(location_shadowDistance, shadowDistance);
}
