//
// Created by jack on 18/02/16.
//

#include "ParticleSystem.h"
#include <ctime>
#include <cstdlib>
#include <glm/gtx/string_cast.hpp>
#define toDegrees(radians) ((radians) * (180.0 / M_PI))


float ParticleSystem::random() {
    return static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
}

float ParticleSystem::randomRange(float LO, float HI) {
    return LO + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(HI-LO)));
}

ParticleSystem::ParticleSystem(ParticleTexture* texture, float pps, float speed, float gravityComplient, float lifeLength, float scale, ParticleMaster *master, World* world) {

    scaleError = 0;
    randomRotation = false;
    directionDeviation = 0;
    srand (static_cast <unsigned> (time(0)));

    this->texture = texture;
    this->pps = pps;
    this->averageSpeed = speed;
    this->gravityComplient = gravityComplient;
    this->averageLifeLength = lifeLength;
    this->averageScale = scale;
    this->world = world;
    this->master = master;
}

ParticleSystem::~ParticleSystem() {
    world = nullptr ;
    master = nullptr;
}

void ParticleSystem::setDirection(vec3 direction, float deviation) {
    this->direction = direction;
    this->directionDeviation = (float) (deviation * M_PI);
}

void ParticleSystem::randomizeRotation() {
    randomRotation = true;
}

void ParticleSystem::setSpeedError(float error) {
    this->speedError = error * averageSpeed;
}

void ParticleSystem::setLifeError(float error) {
    this->lifeError = error * averageLifeLength;
}

void ParticleSystem::setScaleError(float error) {
    this->scaleError = error * averageScale;
}

void ParticleSystem::generateParticles(vec3 systemCenter) {
    float particlesToCreate = pps * world->delta;
    int count = (int) floor(particlesToCreate);
    float partialParticle = mod(particlesToCreate, 1.0f);
    for (int i = 0; i < count; i++) {
        emitParticle(systemCenter);
    }
    if (random() < partialParticle) {
        emitParticle(systemCenter);
    }
}

void ParticleSystem::emitParticle(vec3 center) {
    vec3 velocity(0.0, 0.0, 0.0);
    if(this->direction != vec3(0.0, 0.0, 0.0)){
        velocity = generateRandomUnitVectorWithinCone(direction, directionDeviation);
    }else{
        velocity = generateRandomUnitVector();
    }
    normalize(velocity);
    velocity *= generateValue(averageSpeed, speedError);
    float scale = generateValue(averageScale, scaleError);
    float lifeLength = generateValue(averageLifeLength, lifeError);
    master->addParticle(new Particle(texture, center, velocity, gravityComplient, lifeLength, generateRotation(), scale));
}



float ParticleSystem::generateValue(float average, float errorMargin) {
    float offset = (random() - 0.5f) * 2.0f * errorMargin;
    return average + offset;
}

float ParticleSystem::generateRotation() {
    return (randomRotation) ? random() * 360.0f : 0;
}

vec3 ParticleSystem::generateRandomUnitVectorWithinCone(vec3 coneDirection, float angle) {

    float cosAngle = cos(angle);
    float theta = (float) (random() * 2.0f * M_PI);
    float z = cosAngle + (random() * (1.0f - cosAngle));
    float rootOneMinusZSquared = sqrt(1.0f - z * z);
    float x = (rootOneMinusZSquared * cos(theta));
    float y = (rootOneMinusZSquared * sin(theta));

    vec4 direction = vec4(x, y, z, 1.0f);
    if (coneDirection.x != 0 || coneDirection.y != 0 || (coneDirection.z != 1 && coneDirection.z != -1)) {
        vec3 rotateAxis = cross(coneDirection, vec3(0, 0, 1));
        float rotateAngle = acos(dot(coneDirection, vec3(0, 0, 1)));
        mat4 rotationMatrix = rotate(mat4(1.0f), degrees(rotateAngle), normalize(rotateAxis));
        direction = direction * rotationMatrix;
    } else if (coneDirection.z == -1) {
        direction.z *= -1;
    }
    return vec3(direction.x, direction.y, direction.z);

    //return coneDirection + vec3(randomRange(-angle, +angle), randomRange(-angle, +angle), randomRange(-angle, +angle));
}

vec3 ParticleSystem::generateRandomUnitVector() {
    float theta = (float) (random() * 2.0f * M_PI);
    float z = (random() * 2) - 1;
    float rootOneMinusZSquared = sqrt(1 - z * z);
    float x = rootOneMinusZSquared * cos(theta);
    float y = rootOneMinusZSquared * sin(theta);
    return vec3(x, y, z);
}
