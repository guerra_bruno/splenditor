//
// Created by jack on 22/02/16.
//

#include "ShadowMapEntityRenderer.h"

ShadowMapEntityRenderer::ShadowMapEntityRenderer(ShadowShader *shader, mat4 *projectionViewMatrix) {
    this->shader = shader;
    this->projectionViewMatrix = projectionViewMatrix;
}

void ShadowMapEntityRenderer::render(map<TexturedModel *, std::list<Entity *>> entities) {
    for (auto model : entities) {
        bindModel(model.first->GetRawModel());
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, model.first->GetTexture()->getTextureID());
        if(model.first->GetTexture()->isHasTransparency()){
            glEnable(GL_CULL_FACE);
            glCullFace(GL_BACK);
        }
        for (auto entity : entities[model.first]) {
            prepareInstance(entity);
            glDrawElements(GL_TRIANGLES, model.first->GetRawModel()->getVertexCount(), GL_UNSIGNED_INT, (void*) 0);
        }
        if(model.first->GetTexture()->isHasTransparency())
            glDisable(GL_CULL_FACE);
    }
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);
    glBindVertexArray(0);
}

void ShadowMapEntityRenderer::bindModel(RawModel *rawModel) {
    glBindVertexArray(rawModel->getVaoID());
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
}

void ShadowMapEntityRenderer::prepareInstance(Entity *entity) {
    mat4 modelMatrix = createTransformation(entity->GetPosition(), entity->GetRx(), entity->GetRy(), entity->GetRz(), entity->GetScale());
    mat4 mvpMatrix = *projectionViewMatrix * modelMatrix;
    shader->loadMvpMatrix(mvpMatrix);
}
