//
// Created by jack on 30/01/16.
//

#ifndef SPLENDITORMASTER_NORMALMAPPINGSHADER_H
#define SPLENDITORMASTER_NORMALMAPPINGSHADER_H


#include <Splenditor/RenderEngine/ShaderProgram.h>
#include <Splenditor/ToolBox/Camera.h>
#include <Splenditor/ToolBox/Light.h>

class NormalMappingShader : public ShaderProgram {
public:
    NormalMappingShader(const char *vertex_file_path, const char *fragment_file_path);
    ~NormalMappingShader();
    void loadTransformationMatrix(mat4 matrix);
    void loadProjectionMatrix(mat4 matrix);
    void loadViewMatrix(Camera *camera);
    void loadLights(vector<Light*> lights, mat4 viewMatrix);
    void loadShineVariables(float damper, float reflectivity);
    void loadSkyColour(float r, float g, float b);
    void loadClipPlane(vec4 plane);
    void loadRowNumber(int rowNumber);
    void loadOffset(float x, float y);
    void connectTextureUnits();
    vec3 getEyeSpacePosition(Light *light, mat4 viewMatrix);

protected:
    void getAllUniformLocations();
    void bindAttributes();

private:
    GLint location_transformationMatrix;
    GLint location_projectionMatrix;
    GLint location_viewMatrix;
    GLint location_lightColour[max_lights];
    GLint location_lightPositionEyeSpace[max_lights];
    GLint location_attenuation[max_lights];
    GLint location_shineDamper;
    GLint location_reflectivity;
    GLint location_skyColour;
    GLint location_rowNumber;
    GLint location_offset;
    GLint location_plane;
    GLint location_modelTexture;
    GLint location_normalMap;
};

#endif //SPLENDITORMASTER_NORMALMAPPINGSHADER_H
