//
// Created by jack on 20/11/15.
//


#include "Loader.h"
#define STB_IMAGE_IMPLEMENTATION
#include <Splenditor/ToolBox/stb_image.h>

#define BUFFER_OFFSET(i) ((char *)NULL + (i))



Loader::Loader() {
    vaos.resize(0);
    vbos.resize(0);
    textures.resize(0);
}

GLuint Loader::createVAO() {
    GLuint vaoID;
    glGenVertexArrays(1, &vaoID);
    vaos.push_back(vaoID);
    glBindVertexArray(vaoID);
    return vaoID;
}

unsigned int Loader::storeDataInAttributeList(int attributeNumber, int coordinateSize, vector<float> data) {
    unsigned int vboID;
    glGenBuffers(1, &vboID);
    vbos.push_back(vboID);
    glBindBuffer(GL_ARRAY_BUFFER, vboID);
    glBufferData(GL_ARRAY_BUFFER, sizeof (float)* data.size(), &data[0], GL_STATIC_DRAW);
    glVertexAttribPointer((GLuint) attributeNumber, coordinateSize, GL_FLOAT, GL_FALSE, 0, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    return vboID;
}

int Loader::unbindVAO() {
    return 0;
}

void Loader::bindIndicesBuffer(vector<unsigned int> indices) {
    GLuint vboID;
    glGenBuffers(1, &vboID);
    vbos.push_back(vboID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboID);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof (unsigned int)* indices.size(), &indices[0], GL_STATIC_DRAW);
}

vector<GLuint> Loader::loadToVAO(vector<float> positions, vector<float> textureCoords) {
    GLuint vaoID = createVAO();
    unsigned int vbop = storeDataInAttributeList(0, 2, positions);
    unsigned int vbot = storeDataInAttributeList(1, 2, textureCoords);
    unbindVAO();

    vector<GLuint> v = {vaoID, vbop, vbot};
    return v;
}

RawModel* Loader::loadToVAO(vector<float> positions, vector<float> textureCoords, vector<float> normals, vector<unsigned int> indices) {
    GLuint vaoID = createVAO();
    bindIndicesBuffer(indices);
    storeDataInAttributeList(0, 3, positions);
    storeDataInAttributeList(1, 2, textureCoords);
    storeDataInAttributeList(2, 3, normals);
    unbindVAO();
    return (new RawModel(vaoID, (int) indices.size()));
}

RawModel* Loader::loadToVAO(vector<float> positions, vector<float> textureCoords, vector<float> normals, vector<float> tangents, vector<unsigned int> indices) {
    GLuint vaoID = createVAO();
    bindIndicesBuffer(indices);
    storeDataInAttributeList(0, 3, positions);
    storeDataInAttributeList(1, 2, textureCoords);
    storeDataInAttributeList(2, 3, normals);
    storeDataInAttributeList(3, 3, tangents);
    unbindVAO();
    return (new RawModel(vaoID, (int) indices.size()));
}

RawModel *Loader::loadToVAO(vector<float> positions, int dimensions) {
    GLuint vaoID = createVAO();
    storeDataInAttributeList(0, dimensions, positions);
    unbindVAO();
    return (new RawModel(vaoID, (int) positions.size() / 2));
}



GLuint Loader::loadTexture(string fileName) {


    GLuint textureID;
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);

    int x, y, n;
    unsigned char* data;
    data = stbi_load(fileName.c_str(), &x, &y, &n, 0);
    (n == 3) ? glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, x, y, 0, GL_RGB, GL_UNSIGNED_BYTE, data) : glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, x, y, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    stbi_image_free(data);



    glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, (GLint) -0.4f);

    if(glfwExtensionSupported("GL_EXT_texture_filter_anisotropic")){
        float minimum = 0;
        glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &minimum);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, min(4.0f, minimum));
    }
    else{
        printf("Anisotropic filtering not supported.\n");
    }

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    textures.push_back(textureID);
    return textureID;
}

GLuint Loader::loadFontTexture(string fileName) {


    GLuint textureID;
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);

    int x, y, n;
    unsigned char* data;
    data = stbi_load(fileName.c_str(), &x, &y, &n, 0);
    (n == 3) ? glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, x, y, 0, GL_RGB, GL_UNSIGNED_BYTE, data) : glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, x, y, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    stbi_image_free(data);

    glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, (GLint) 0.0f);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    textures.push_back(textureID);
    return textureID;
}


Loader::~Loader() {

    for(GLuint& vbo : vbos) {cout << "deleting vbo " << vbo << endl; glDeleteVertexArrays(1, &vbo);} vbos.clear();
    for(GLuint& vao : vaos) {cout << "deleting vao " << vao << endl; glDeleteVertexArrays(1, &vao);} vaos.clear();
    for(GLuint& tex : textures) {cout << "deleting tex " << tex << endl; glDeleteTextures(1, &tex);} textures.clear();

}


void Loader::deleteVBO(GLuint vbo) {
    cout << "deleting vbo " << vbo << endl;
    glDeleteVertexArrays(1, &vbo);
    vbos.remove(vbo);

}

void Loader::deleteVAO(GLuint vao) {
    cout << "deleting vao " << vao << endl;
    glDeleteVertexArrays(1, &vao);
    vaos.remove(vao);
}

GLuint Loader::loadCubeMap(vector<string> texturesFiles) {

    GLuint textureID;
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

    for (GLuint i = 0; i < texturesFiles.size(); i++) {
        int x, y, n;
        unsigned char* data = stbi_load(texturesFiles[i].c_str(), &x, &y, &n, 0);
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, x, y, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        stbi_image_free(data);
    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    textures.push_back(textureID);
    return textureID;
}

GLuint Loader::createEmptyVbo(int floatCount) {
    unsigned int vboID;
    glGenBuffers(1, &vboID);
    vbos.push_back(vboID);
    glBindBuffer(GL_ARRAY_BUFFER, vboID);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * floatCount , NULL, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    return vboID;
}

void Loader::addInstancedAttribute(int vao, int vbo, int attribute, int dataSize, int instancedDataLength, int offset) {
    glBindBuffer(GL_ARRAY_BUFFER, (GLuint) vbo);
    glBindVertexArray((GLuint) vao);
    glVertexAttribPointer((GLuint) attribute, dataSize, GL_FLOAT, GL_FALSE,
                          (GLsizei) (sizeof(float) * instancedDataLength), BUFFER_OFFSET(sizeof(float) * offset));
    glVertexAttribDivisor((GLuint) attribute, 1);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void Loader::updateVbo(int vbo, vector<float> &data) {
    glBindBuffer(GL_ARRAY_BUFFER, (GLuint) vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * data.size(), NULL, GL_STREAM_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * data.size(), &data[0]);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}
