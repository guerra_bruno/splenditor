//
// Created by jack on 18/02/16.
//

#include <glm/gtx/string_cast.hpp>
#include "ParticleRenderer.h"

ParticleRenderer::ParticleRenderer(Loader *loader, mat4 projectionMatrix) {
    this->pointer = 0;
    this->loader = loader;
    this->vbo = loader->createEmptyVbo(max_instances * instance_data_length);
    buffer.resize(max_instances * instance_data_length);
    vertices = {-0.5f, 0.5f, -0.5f, -0.5f, 0.5f, 0.5f, 0.5f, -0.5f};
    quad = loader->loadToVAO(vertices, 2);
    loader->addInstancedAttribute(quad->getVaoID(), vbo, 1, 4, instance_data_length, 0);
    loader->addInstancedAttribute(quad->getVaoID(), vbo, 2, 4, instance_data_length, 4);
    loader->addInstancedAttribute(quad->getVaoID(), vbo, 3, 4, instance_data_length, 8);
    loader->addInstancedAttribute(quad->getVaoID(), vbo, 4, 4, instance_data_length, 12);
    loader->addInstancedAttribute(quad->getVaoID(), vbo, 5, 4, instance_data_length, 16);
    loader->addInstancedAttribute(quad->getVaoID(), vbo, 6, 1, instance_data_length, 20);
    this->shader = new ParticleShader("Shaders/particleVertexShader.glsl", "Shaders/particleFragmentShader.glsl");
    shader->start();
    shader->loadProjectionMatrix(projectionMatrix);
    shader->stop();
}

ParticleRenderer::~ParticleRenderer() {
    delete shader;
    delete quad;
}

void ParticleRenderer::render(map<ParticleTexture*, std::list<Particle*>> particles, Camera* camera){
    prepare();
    for(auto& texture : particles) {
        bindTexture(texture.first);
        pointer = 0;
        vector<float> vbo_data(particles[texture.first].size() * instance_data_length);
        for (auto &particle : particles[texture.first]) {
            updateModelViewMatrix(particle->getPosition(), particle->getRotation(), particle->getScale(), camera->getViewMatrix(), vbo_data);
            updateTexCoordInfo(particle, vbo_data);
        }
        loader->updateVbo(vbo, vbo_data);
        glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, quad->getVertexCount(), (GLsizei) particles[texture.first].size());
    }
    finishRendering();
}

void ParticleRenderer::prepare() {
    shader->start();
    glBindVertexArray(quad->getVaoID());
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glEnableVertexAttribArray(3);
    glEnableVertexAttribArray(4);
    glEnableVertexAttribArray(5);
    glEnableVertexAttribArray(6);
    glEnable(GL_BLEND);
    glDepthMask((GLboolean) false);
}

void ParticleRenderer::finishRendering() {
    glDepthMask((GLboolean) true);
    glDisable(GL_BLEND);
    glDisableVertexAttribArray(6);
    glDisableVertexAttribArray(5);
    glDisableVertexAttribArray(4);
    glDisableVertexAttribArray(3);
    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);
    glBindVertexArray(0);
    shader->stop();
}

void ParticleRenderer::updateModelViewMatrix(vec3 position, float rotation, float s, mat4 viewMatrix, vector<float> &vboData) {
    mat4 modelMatrix;
    modelMatrix = translate(mat4(1.0f), position);

    modelMatrix[0][0] = viewMatrix[0][0];
    modelMatrix[0][1] = viewMatrix[1][0];
    modelMatrix[0][2] = viewMatrix[2][0];
    modelMatrix[1][0] = viewMatrix[0][1];
    modelMatrix[1][1] = viewMatrix[1][1];
    modelMatrix[1][2] = viewMatrix[2][1];
    modelMatrix[2][0] = viewMatrix[0][2];
    modelMatrix[2][1] = viewMatrix[1][2];
    modelMatrix[2][2] = viewMatrix[2][2];

    modelMatrix = rotate(modelMatrix, rotation, vec3(0.0, 0.0, 1.0));
    modelMatrix = scale(modelMatrix, vec3(s, s, s));
    mat4 modelViewMatrix = viewMatrix * modelMatrix;
    storeMatrixData(modelViewMatrix, vboData);
}

void ParticleRenderer::bindTexture(ParticleTexture *texture) {
    (texture->getAdditive())? glBlendFunc(GL_SRC_ALPHA, GL_ONE) : glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture->getTextureID());
    shader->loadNumberOfRows(texture->getNumberOfRows());
}

void ParticleRenderer::storeMatrixData(mat4 matrix, vector<float> &vboData) {
    vboData[pointer++] = matrix[0][0];
    vboData[pointer++] = matrix[0][1];
    vboData[pointer++] = matrix[0][2];
    vboData[pointer++] = matrix[0][3];
    vboData[pointer++] = matrix[1][0];
    vboData[pointer++] = matrix[1][1];
    vboData[pointer++] = matrix[1][2];
    vboData[pointer++] = matrix[1][3];
    vboData[pointer++] = matrix[2][0];
    vboData[pointer++] = matrix[2][1];
    vboData[pointer++] = matrix[2][2];
    vboData[pointer++] = matrix[2][3];
    vboData[pointer++] = matrix[3][0];
    vboData[pointer++] = matrix[3][1];
    vboData[pointer++] = matrix[3][2];
    vboData[pointer++] = matrix[3][3];
}

void ParticleRenderer::updateTexCoordInfo(Particle *particle, vector<float> &vboData) {
    vboData[pointer++] = particle->getTexOffset1().x;
    vboData[pointer++] = particle->getTexOffset1().y;
    vboData[pointer++] = particle->getTexOffset2().x;
    vboData[pointer++] = particle->getTexOffset2().y;
    vboData[pointer++] = particle->getBlendFactor();
}
