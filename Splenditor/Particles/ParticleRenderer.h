//
// Created by jack on 18/02/16.
//

#ifndef SPLENDITORMASTER_PARTICLERENDERER_H
#define SPLENDITORMASTER_PARTICLERENDERER_H

#include <Splenditor/Models/RawModel.h>
#include <vector>
#include <map>
#include <Splenditor/RenderEngine/Loader.h>
#include <Splenditor/ToolBox/common.h>
#include "ParticleShader.h"
#include "Particle.h"

using namespace std;

class ParticleRenderer {
public:
    ParticleRenderer(Loader* loader, mat4 projectionMatrix);
    ~ParticleRenderer();
    void bindTexture(ParticleTexture* texture);
    void render(map<ParticleTexture*, std::list<Particle*>> particles, Camera* camera);
    void prepare();
    void storeMatrixData(mat4 matrix, vector<float> &vboData);
    void updateTexCoordInfo(Particle* particle, vector<float> &vboData);
    void updateModelViewMatrix(vec3 position, float rotation, float s, mat4 viewMatrix, vector<float> &vboData);
    void finishRendering();


private:
    vector<float> vertices;
    vector<float> buffer;
    int pointer;
    static const int max_instances = 10000;
    static const int instance_data_length = 21;
    RawModel *quad;
    ParticleShader *shader;
    Loader *loader;
    unsigned int vbo;
};


#endif //SPLENDITORMASTER_PARTICLERENDERER_H
