//
// Created by jack on 1/12/15.
//

#ifndef SPLENDITORMASTER_TERRAINTEXTUREPACK_H
#define SPLENDITORMASTER_TERRAINTEXTUREPACK_H


#include "TerrainTexture.h"

class TerrainTexturePack {
public:
    TerrainTexturePack(TerrainTexture *bi, TerrainTexture *r, TerrainTexture *g, TerrainTexture *b);
    ~TerrainTexturePack();
    TerrainTexture* getBTexture() const;
    TerrainTexture* getGTexture() const;
    TerrainTexture* getRTexture() const;
    TerrainTexture* getBackgroundTexture() const;

private:
    TerrainTexture *backgroundTexture;
    TerrainTexture *rTexture;
    TerrainTexture *gTexture;
    TerrainTexture *bTexture;

};

#endif //SPLENDITORMASTER_TERRAINTEXTUREPACK_H
