//
// Created by jack on 27/11/15.
//

#include "TerrainRenderer.h"


TerrainRenderer::TerrainRenderer(mat4 projectionMatrix) {
    this->shadow_terrain_shader = new ShadowedTerrainShader("Shaders/shadowedTerrainVertexShader.glsl", "Shaders/shadowedTerrainFragmentShader.glsl");
    this->shadow_terrain_shader->start();
    this->shadow_terrain_shader->loadProjectionMatrix(projectionMatrix);
    this->shadow_terrain_shader->connectTextureUnits();
    this->shadow_terrain_shader->stop();

    this->terrain_shader = new TerrainShader("Shaders/terrainVertexShader.glsl", "Shaders/terrainFragmentShader.glsl");
    this->terrain_shader->start();
    this->terrain_shader->loadProjectionMatrix(projectionMatrix);
    this->terrain_shader->connectTextureUnits();
    this->terrain_shader->stop();
}

void TerrainRenderer::render(vector<Terrain *> terrains, vec4 clipPlane, vec3 colour, World *world, float shadowResolution, mat4 toShadowMapSpace, int PCF, float shadowDistance) {
    prepare(clipPlane, colour, world, shadow_terrain_shader);
    shadow_terrain_shader->loadPCF(PCF);
    shadow_terrain_shader->loadMapSize(shadowResolution);
    shadow_terrain_shader->loadToShadowMapSpace(toShadowMapSpace);
    shadow_terrain_shader->loadShadowDistance(shadowDistance);
    rendering(terrains, shadow_terrain_shader);
    shadow_terrain_shader->stop();
}

void TerrainRenderer::render(vector<Terrain *> terrains, vec4 clipPlane, vec3 colour, World *world) {
    prepare(clipPlane, colour, world, terrain_shader);
    rendering(terrains, terrain_shader);
    terrain_shader->stop();
}

void TerrainRenderer::rendering(vector<Terrain *> terrains, TerrainShader* shader){
    for(auto& terrain : terrains){
        this->prepareTexturedModel(terrain, shader);
        loadModelMatrix(terrain, shader);
        glDrawElements(GL_TRIANGLES, terrain->getModel()->getVertexCount(), GL_UNSIGNED_INT, (void*) 0);
        unbindTexturedModel();
    }
}

void TerrainRenderer::prepare(vec4 clipPlane, vec3 colour, World *world, TerrainShader* shader) {
    shader->start();
    shader->loadClipPlane(clipPlane);
    shader->loadSkyColour(colour.r, colour.g, colour.b);
    shader->loadLights(world->lights);
    shader->loadViewMatrix(world->camera);

}

void TerrainRenderer::prepareTexturedModel(Terrain *terrain, TerrainShader* shader){
    glBindVertexArray((GLuint) terrain->getModel()->getVaoID());
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    bindTextures(terrain);
    shader->loadShineVariables(1.0, 0.0);
}

void TerrainRenderer::bindTextures(Terrain *terrain) {

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, terrain->geTexturePack()->getBackgroundTexture()->GetTextureID());

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, terrain->geTexturePack()->getRTexture()->GetTextureID());

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, terrain->geTexturePack()->getGTexture()->GetTextureID());

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, terrain->geTexturePack()->getBTexture()->GetTextureID());

    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_2D, terrain->getBlendMap()->GetTextureID());

}


void TerrainRenderer::unbindTexturedModel(){
    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);
    glBindVertexArray(0);
}

void TerrainRenderer::loadModelMatrix(Terrain *terrain, TerrainShader* shader){
    mat4 transformationMatrix = createTransformation(vec3(terrain->getX(), 0.0, terrain->getZ()), 0.0, 0.0, 0.0, 1.0);
    shader->loadTransformationMatrix(transformationMatrix);
}

TerrainRenderer::~TerrainRenderer() {
    delete shadow_terrain_shader;
    delete terrain_shader;
}
