//
// Created by jack on 18/02/16.
//

#ifndef SPLENDITORMASTER_PARTICLE_H
#define SPLENDITORMASTER_PARTICLE_H

#include <Splenditor/ToolBox/common.h>
#include <Splenditor/ToolBox/World.h>
#include "ParticleTexture.h"

class Particle {

public:
    Particle(ParticleTexture* texture, vec3 position, vec3 velocity, float gravityEffect, float lifeLength, float rotation, float scale);
    ~Particle();
    vec3 getPosition() { return position; }
    float getRotation()  { return rotation; }
    float getScale() { return scale; }
    ParticleTexture* getTexture() { return texture; }
    bool update(World *world);
    vec2 getTexOffset2() { return texOffset2; }
    vec2 getTexOffset1() { return texOffset1; }
    float getBlendFactor() { return blendFactor; }
    float getDistance() { return distance; }
    void updateTextureCoordInfo();
    vec2 setTextureOffset(int index);



private:
    ParticleTexture* texture;
    vec3 reusable_change;
    vec3 position;
    vec3 velocity;
    vec2 texOffset1;
    vec2 texOffset2;
    float blendFactor;
    float gravityEffect;
    float lifeLength;
    float rotation;
    float scale;
    float elapsedTime;
    float distance;
    static constexpr float GRAVITY = -50;
};


#endif //SPLENDITORMASTER_PARTICLE_H
