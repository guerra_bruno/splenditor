//
// Created by jack on 8/12/15.
//

#ifndef SPLENDITORMASTER_SKYBOXSHADER_H
#define SPLENDITORMASTER_SKYBOXSHADER_H


#include <Splenditor/RenderEngine/ShaderProgram.h>
#include <Splenditor/ToolBox/Camera.h>

class SkyboxShader : public ShaderProgram {
public:
    SkyboxShader(const char *vertex_file_path, const char *fragment_file_path);
    ~SkyboxShader();
    void loadProjectionMatrix(mat4 matrix);
    void loadViewMatrix(Camera *camera, double delta);
    void loadFogColour(float r, float g, float b);
    void loadBlendFactor(float blend);
    void connectTextureUnits();

protected:
    void getAllUniformLocations();
    void bindAttributes();

private:
    int location_projectionMatrix;
    int location_viewMatrix;
    int location_fogColour;
    int location_cubeMap1;
    int location_cubeMap2;
    int location_blendFactor;
    float rotation;
    static constexpr float ROTATE_SPEED = 50.0;
};


#endif //SPLENDITORMASTER_SKYBOXSHADER_H
