//
// Created by jack on 27/11/15.
//

#include <sstream>
#include "TerrainShader.h"

TerrainShader::TerrainShader(const char *vertex_file_path, const char *fragment_file_path) : ShaderProgram(vertex_file_path, fragment_file_path){
    bindAttributes();
    glLinkProgram(getProgramID());
    getAllUniformLocations();
}

void TerrainShader::loadTransformationMatrix(mat4 matrix) {
    loadMatrix(location_transformationMatrix, matrix);
}

void TerrainShader::loadProjectionMatrix(mat4 matrix) {
    loadMatrix(location_projectionMatrix, matrix);
}

void TerrainShader::loadViewMatrix(Camera *camera) {
    loadMatrix(location_viewMatrix, camera->getViewMatrix());
}

void TerrainShader::loadShineVariables(float damper, float reflectivity) {
    loadFloat(location_shineDamper, damper);
    loadFloat(location_reflectivity, reflectivity);
}

void TerrainShader::getAllUniformLocations() {
    location_transformationMatrix = getUniformLocation("transformationMatrix");
    location_projectionMatrix = getUniformLocation("projectionMatrix");
    location_viewMatrix = getUniformLocation("viewMatrix");
    location_shineDamper = getUniformLocation("shineDamper");
    location_reflectivity = getUniformLocation("reflectivity");
    location_skyColour = getUniformLocation("skyColour");
    location_backgroundTexture = getUniformLocation("backgroundTexture");
    location_rTexture = getUniformLocation("rTexture");
    location_gTexture = getUniformLocation("gTexture");
    location_bTexture = getUniformLocation("bTexture");
    location_blendMap = getUniformLocation("blendMap");
    location_plane = getUniformLocation("plane");


    for(int i=0; i<max_lights; i++){
        ostringstream c,p,a; c << "lightColour[" << i << "]"; p << "lightPosition[" << i << "]"; a << "attenuation[" << i << "]";
        location_lightColour[i] = getUniformLocation(c.str().c_str());
        location_lightPosition[i] = getUniformLocation(p.str().c_str());
        location_attenuation[i] = getUniformLocation(a.str().c_str());
    }

}

void TerrainShader::loadLights(vector<Light*> lights) {
    for(int i=0; i<max_lights; i++) {
        if(i < lights.size()){
            load3DVector(location_lightPosition[i], lights[i]->GetPosition());
            load3DVector(location_lightColour[i], lights[i]->GetColour());
            load3DVector(location_attenuation[i], lights[i]->GetAttenuation());
        }
        else{
            load3DVector(location_lightPosition[i], vec3(0.0, 0.0, 0.0));
            load3DVector(location_lightColour[i], vec3(0.0, 0.0, 0.0));
            load3DVector(location_attenuation[i], vec3(1.0, 0.0, 0.0));
        }
    }
}

void TerrainShader::bindAttributes() {
    this->bindAttribute(0, "position");
    this->bindAttribute(1, "textureCoords");
    this->bindAttribute(2, "normal");
}

void TerrainShader::loadSkyColour(float r, float g, float b) {
    load3DVector(location_skyColour, vec3(r, g, b));
}

void TerrainShader::connectTextureUnits() {
    loadInt(location_backgroundTexture, 0);
    loadInt(location_rTexture, 1);
    loadInt(location_gTexture, 2);
    loadInt(location_bTexture, 3);
    loadInt(location_blendMap, 4);
}

TerrainShader::~TerrainShader() {
}


void TerrainShader::loadClipPlane(vec4 plane) {
    load4DVector(location_plane, plane);
}
