//
// Created by jack on 22/02/16.
//

#ifndef SPLENDITORMASTER_SHADOWMAPMASTERRENDERER_H
#define SPLENDITORMASTER_SHADOWMAPMASTERRENDERER_H


#include "ShadowFrameBuffer.h"
#include "ShadowBox.h"
#include "ShadowShader.h"
#include "ShadowMapEntityRenderer.h"
#include <map>
#include <Splenditor/ToolBox/Light.h>

class ShadowMapMasterRenderer {
public:
    ShadowMapMasterRenderer(Camera* camera, float resolution, float NEAR_PLANE, float FOV, DisplayManager *display);
    ~ShadowMapMasterRenderer();
    void render(map<TexturedModel*, std::list<Entity*>> entities, Light* sun);
    mat4 getToShadowMapSpaceMatrix();
    GLuint getShadowMap();
    mat4 getLightSpaceTransform();
    void prepare(vec3 lightDirection, ShadowBox* box);
    void finish();
    float getShadowDistance() { return shadowDistance; }
    void updateLightViewMatrix(vec3 direction, vec3 center);
    void updateOrthoProjectionMatrix(float width, float height, float length);
    mat4 createOffset();
private:
    Camera* cam;
    float SHADOW_MAP_SIZE;
    float FOV, NEAR_PLANE;
    float shadowDistance;
    ShadowFrameBuffer* shadowFbo;
    ShadowShader* shader;
    ShadowBox* shadowBox;
    ShadowMapEntityRenderer* entityRenderer;
    mat4 projectionMatrix;
    mat4 *lightViewMatrix;
    mat4 *projectionViewMatrix;
    mat4 offset;
};


#endif //SPLENDITORMASTER_SHADOWMAPMASTERRENDERER_H
