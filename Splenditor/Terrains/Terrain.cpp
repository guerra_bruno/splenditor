//
// Created by jack on 27/11/15.
//


#include "Terrain.h"
#include <Splenditor/ToolBox/stb_image.h>


Terrain::Terrain(vec2 grid, Loader *loader, TerrainTexturePack *texturePack, TerrainTexture* blendMap, string heightMap){
    this->texturePack = texturePack;
    this->blendMap = blendMap;
    this->x = grid.x * SIZE;
    this->z = grid.y * SIZE;
    this->model = generateTerrain(loader, heightMap);
}


RawModel* Terrain::generateTerrain(Loader *loader, string fileName) {

    int x, y, dim, n;


    unsigned char* data = stbi_load(fileName.c_str(), &x, &y, &dim, 0);
    vertex_count = y;

    float **image = new float*[x];
    heightMap = new float*[x];
    for (int i = 0; i < x; i++) image[i] = new float[x];
    for (int i = 0; i < x; i++) heightMap[i] = new float[x];

    n = 0;
    for (int i = 0; i < x; i++){
        for (int j = 0; j < x; j++) {
            if (dim == 4)       image[i][j] = (((data[n++]) + (data[n++]) + (data[n++]) + (data[n++])) / (float) dim);
            else if (dim == 3)  image[i][j] = (((data[n++]) + (data[n++]) + (data[n++])) / (float) dim);
            else                image[i][j] = (data[n++] / (float) dim);
        }
    }

    this->size = x;
    stbi_image_free(data);


    int count = vertex_count * vertex_count;
    vector<float> vertices((unsigned long) (count * 3));
    vector<float> normals((unsigned long) (count * 3));
    vector<float> textureCoords((unsigned long) (count * 2));
    vector<unsigned int> indices((unsigned long) (6 * (vertex_count - 1) * (vertex_count - 1)));
    int vertexPointer = 0;
    for (int i = 0; i < vertex_count; i++) {
        for (int j = 0; j < vertex_count; j++) {
            heightMap[j][i] = getHeight(j, i, this->size, image);
            vertices[vertexPointer * 3] = (float) j / ((float) vertex_count - 1) * SIZE;
            vertices[vertexPointer * 3 + 1] = heightMap[j][i];
            vertices[vertexPointer * 3 + 2] = (float) i / ((float) vertex_count - 1) * SIZE;
            vec3 normal = calculateNormal(j, i, this->size, image);
            normals[vertexPointer * 3] = normal.x;
            normals[vertexPointer * 3 + 1] = normal.y;
            normals[vertexPointer * 3 + 2] = normal.z;
            textureCoords[vertexPointer * 2] = (float) j / ((float) vertex_count - 1);
            textureCoords[vertexPointer * 2 + 1] = (float) i / ((float) vertex_count - 1);
            vertexPointer++;
        }
    }
    int pointer = 0;
    for (int gz = 0; gz < vertex_count - 1; gz++) {
        for (int gx = 0; gx < vertex_count - 1; gx++) {
            unsigned int topLeft = (unsigned int) ((gz * vertex_count) + gx);
            unsigned int topRight = topLeft + 1;
            unsigned int bottomLeft = (unsigned int) (((gz + 1) * vertex_count) + gx);
            unsigned int bottomRight = bottomLeft + 1;
            indices[pointer++] = topLeft;
            indices[pointer++] = bottomLeft;
            indices[pointer++] = topRight;
            indices[pointer++] = topRight;
            indices[pointer++] = bottomLeft;
            indices[pointer++] = bottomRight;
        }
    }

    for (int i = 0; i < this->size; i++)
        delete [] image[i];
    delete [] image;

    return loader->loadToVAO(vertices, textureCoords, normals, indices);
}

float Terrain::getHeight(int x, int z, int size, float **image) {
    if (x < 0 || x >= size || z < 0 || z >= size)
        return 0.0;

    float h;
    h = image[x][z];
    h -= (MAX_PIXEL_COLOR / 2.0f);
    h /= (MAX_PIXEL_COLOR / 2.0f);
    h *= MAX_HEIGHT;

    return h;
}
vec3 Terrain::calculateNormal(int x, int z, int size, float** image) {
    float hL = getHeight(x - 1, z, size, image);
    float hR = getHeight(x + 1, z, size, image);
    float hD = getHeight(x, z - 1, size, image);
    float hU = getHeight(x, z + 1, size, image);
    vec3 normal(hL - hR, 2.0f, hD - hU);

    return normalize(normal);
}

float Terrain::getHeightOfTerrain(float worldX, float worldZ) {
    float terrainX = worldX - this->x;
    float terrainZ = worldZ - this->z;
    float gridSquareSize = SIZE / ((float) this->size - 1);
    int gridX = (int) floor(terrainX / gridSquareSize);
    int gridZ = (int) floor(terrainZ / gridSquareSize);

    if (gridX >= this->size - 1 || gridZ >= this->size - 1 || gridX < 0 || gridZ < 0) {
        return 0.0;
    }

    float xCoord = mod(terrainX, gridSquareSize) / gridSquareSize;
    float zCoord = mod(terrainZ, gridSquareSize)  / gridSquareSize;
    float answer;

    if (xCoord <= (1 - zCoord))     {
        answer = barryCentric(vec3(0, heightMap[gridX][gridZ], 0), vec3(1, heightMap[gridX + 1][gridZ], 0),
                                      vec3(0, heightMap[gridX][gridZ + 1], 1), vec2(xCoord, zCoord));
    } else {
        answer = barryCentric(vec3(1, heightMap[gridX + 1][gridZ], 0), vec3(1, heightMap[gridX + 1][gridZ + 1], 1),
                                      vec3(0, heightMap[gridX][gridZ + 1], 1), vec2(xCoord, zCoord));
    }

    return answer;
}




float Terrain::getZ() const {
    return z;
}

float Terrain::getX() const {
    return x;
}

RawModel* Terrain::getModel() const {
    return model;
}

TerrainTexture *Terrain::getBlendMap() const {
    return this->blendMap;
}

TerrainTexturePack *Terrain::geTexturePack() const {
    return this->texturePack;
}

Terrain::~Terrain() {
    delete model;
    delete texturePack;
    delete blendMap;
    for (int i = 0; i < this->size; i++)
        delete [] heightMap[i];
    delete [] heightMap;
}
