//
// Created by jack on 15/02/16.
//

#ifndef SPLENDITORMASTER_FONTRENDERER_H
#define SPLENDITORMASTER_FONTRENDERER_H


#include "FontShader.h"
#include <map>
#include <vector>
#include <Splenditor/FontMeshCreator/Text.h>

class FontRenderer {
public:
    FontRenderer();
    ~FontRenderer();
    void render(map<FontType*, vector<Text*>> texts);
private:
    void renderText(Text *text);
    void prepare();
    void endRendering();
    FontShader* shader;
};


#endif //SPLENDITORMASTER_FONTRENDERER_H
