//
// Created by jack on 12/01/16.
//

#ifndef SPLENDITORMASTER_WATERSHADER_H
#define SPLENDITORMASTER_WATERSHADER_H


#include <Splenditor/RenderEngine/ShaderProgram.h>
#include <Splenditor/ToolBox/Camera.h>
#include <Splenditor/ToolBox/Light.h>

class WaterShader: public ShaderProgram {
public:
    WaterShader(const char *vertex_file_path, const char *fragment_file_path);
    ~WaterShader();
    void loadProjectionMatrix(mat4 matrix);
    void loadViewMatrix(Camera *camera);
    void loadModelMatrix(mat4 modelMatrix);
    void loadLights(vector<Light*> lights);
    void loadMoveFactor(float moveFactor);
    void loadCameraPosition(vec3 position);
    void connectTextureUnits();

protected:
    virtual void getAllUniformLocations();
    virtual void bindAttributes();
private:
    GLint location_projectionMatrix;
    GLint location_viewMatrix;
    GLint location_modelMatrix;
    GLint location_reflectionTexture;
    GLint location_refractionTexture;
    GLint location_dudvMap;
    GLint location_normalMap;
    GLint location_moveFactor;
    GLint location_cameraPosition;
    GLint location_depthMap;
    GLint location_lightColour[max_lights];
    GLint location_lightPosition[max_lights];
    GLint location_attenuation[max_lights];

};


#endif //SPLENDITORMASTER_WATERSHADER_H
