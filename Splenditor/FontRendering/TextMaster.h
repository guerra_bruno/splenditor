//
// Created by jack on 17/02/16.
//

#ifndef SPLENDITORMASTER_TEXTMASTER_H
#define SPLENDITORMASTER_TEXTMASTER_H


#include <Splenditor/RenderEngine/Loader.h>
#include <Splenditor/FontMeshCreator/FontType.h>
#include <list>
#include <Splenditor/FontMeshCreator/Text.h>
#include "FontRenderer.h"

using namespace std;

class TextMaster {
public:
    TextMaster();
    ~TextMaster();
    void render();
    void init(Loader *loader);
    void loadText(Text *text);
    void removeText(Text *text);

private:
    Loader* loader;
    map<FontType*, vector<Text *>> texts;
    FontRenderer* renderer;

};


#endif //SPLENDITORMASTER_TEXTMASTER_H
