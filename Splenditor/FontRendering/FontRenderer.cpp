//
// Created by jack on 15/02/16.
//

#include "FontRenderer.h"

FontRenderer::FontRenderer() {
    this->shader = new FontShader("Shaders/fontVertexShader.glsl", "Shaders/fontFragmentShader.glsl");
}

FontRenderer::~FontRenderer() {
    delete shader;
}

void FontRenderer::prepare() {
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_DEPTH_TEST);
    shader->start();
}

void FontRenderer::endRendering() {
    shader->stop();
    glDisable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
}

void FontRenderer::render(map<FontType *, vector<Text*>> texts) {
    prepare();
    for(auto& font : texts){
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, font.first->getTextureAtlas());
        for(auto& text : texts[font.first]){
            renderText(text);
        }
    }
    endRendering();
}

void FontRenderer::renderText(Text *text) {
    glBindVertexArray((GLuint) text->getGuiInfo()->getTextMeshVao());
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    shader->loadColour(text->getGuiInfo()->getColour());
    shader->loadTranslation(text->getGuiInfo()->getPosition());
    glDrawArrays(GL_TRIANGLES, 0, text->getGuiInfo()->getVertexCount());
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);
    glBindVertexArray(0);
}
