//
// Created by jack on 27/11/15.
//

#ifndef SPLENDITORMASTER_TERRAINSHADER_H
#define SPLENDITORMASTER_TERRAINSHADER_H


#include <Splenditor/RenderEngine/ShaderProgram.h>
#include <Splenditor/ToolBox/Light.h>
#include <Splenditor/ToolBox/Camera.h>

class TerrainShader : public ShaderProgram {
public:
    TerrainShader(const char *vertex_file_path, const char *fragment_file_path);
    ~TerrainShader();
    void connectTextureUnits();
    void loadTransformationMatrix(mat4 matrix);
    void loadProjectionMatrix(mat4 matrix);
    void loadViewMatrix(Camera *camera);
    void loadLights(vector<Light*> lights);
    void loadShineVariables(float damper, float reflectivity);
    void loadSkyColour(float r, float g, float b);
    void loadClipPlane(vec4 plane);

protected:
    void getAllUniformLocations();
    void bindAttributes();

private:
    GLint location_transformationMatrix;
    GLint location_projectionMatrix;
    GLint location_viewMatrix;
    GLint location_lightColour[max_lights];
    GLint location_lightPosition[max_lights];
    GLint location_attenuation[max_lights];
    GLint location_shineDamper;
    GLint location_reflectivity;
    GLint location_skyColour;
    GLint location_backgroundTexture;
    GLint location_rTexture;
    GLint location_gTexture;
    GLint location_bTexture;
    GLint location_blendMap;
    GLint location_plane;
};


#endif //SPLENDITORMASTER_TERRAINSHADER_H
