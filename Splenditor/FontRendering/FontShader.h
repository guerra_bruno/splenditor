//
// Created by jack on 15/02/16.
//

#ifndef SPLENDITORMASTER_FONTSHADER_H
#define SPLENDITORMASTER_FONTSHADER_H


#include <Splenditor/RenderEngine/ShaderProgram.h>

class FontShader : public ShaderProgram {
public:
    FontShader(const char *vertex_file_path, const char *fragment_file_path);
    ~FontShader() {}
    void loadColour(vec3 colour);
    void loadTranslation(vec2 translation);
protected:
    void bindAttributes();
    void getAllUniformLocations();

private:
    GLint location_colour;
    GLint location_translation;
};


#endif //SPLENDITORMASTER_FONTSHADER_H
