//
// Created by jack on 30/01/16.
//

#include <sstream>
#include "NormalMappingShader.h"


NormalMappingShader::NormalMappingShader(const char *vertex_file_path, const char *fragment_file_path) : ShaderProgram(vertex_file_path, fragment_file_path){
    bindAttributes();
    glLinkProgram(getProgramID());
    getAllUniformLocations();
}

void NormalMappingShader::bindAttributes() {
    this->bindAttribute(0, "position");
    this->bindAttribute(1, "textureCoords");
    this->bindAttribute(2, "normal");
    this->bindAttribute(3, "tangent");
}

void NormalMappingShader::getAllUniformLocations() {
    location_transformationMatrix = getUniformLocation("transformationMatrix");
    location_projectionMatrix = getUniformLocation("projectionMatrix");
    location_viewMatrix = getUniformLocation("viewMatrix");
    location_shineDamper = getUniformLocation("shineDamper");
    location_reflectivity = getUniformLocation("reflectivity");
    location_skyColour = getUniformLocation("skyColour");
    location_rowNumber = getUniformLocation("rowNumber");
    location_offset = getUniformLocation("offset");
    location_plane = getUniformLocation("plane");
    location_modelTexture = getUniformLocation("modelTexture");
    location_normalMap = getUniformLocation("normalMap");

    for(int i=0; i<max_lights; i++){
        ostringstream c,p,a; c << "lightColour[" << i << "]"; p << "lightPositionEyeSpace[" << i << "]"; a << "attenuation[" << i << "]";
        location_lightColour[i] = getUniformLocation(c.str().c_str());
        location_lightPositionEyeSpace[i] = getUniformLocation(p.str().c_str());
        location_attenuation[i] = getUniformLocation(a.str().c_str());
    }


};

void NormalMappingShader::loadTransformationMatrix(mat4 matrix) {
    loadMatrix(location_transformationMatrix, matrix);
}

void NormalMappingShader::loadProjectionMatrix(mat4 matrix) {
    loadMatrix(location_projectionMatrix, matrix);
}

void NormalMappingShader::loadViewMatrix(Camera *camera) {
    loadMatrix(location_viewMatrix, camera->getViewMatrix());
}


void NormalMappingShader::loadLights(std::vector<Light*> lights, mat4 viewMatrix) {
    for(int i=0; i<max_lights; i++) {
        if(i < lights.size()){
            load3DVector(location_lightPositionEyeSpace[i], getEyeSpacePosition(lights[i], viewMatrix));
            load3DVector(location_lightColour[i], lights[i]->GetColour());
            load3DVector(location_attenuation[i], lights[i]->GetAttenuation());
        }
        else{
            load3DVector(location_lightPositionEyeSpace[i], vec3(0.0, 0.0, 0.0));
            load3DVector(location_lightColour[i], vec3(0.0, 0.0, 0.0));
            load3DVector(location_attenuation[i], vec3(1.0, 0.0, 0.0));
        }
    }
}

void NormalMappingShader::loadShineVariables(float damper, float reflectivity) {
    loadFloat(location_shineDamper, damper);
    loadFloat(location_reflectivity, reflectivity);
}


void NormalMappingShader::loadSkyColour(float r, float g, float b) {
    load3DVector(location_skyColour, vec3(r, g, b));
}

NormalMappingShader::~NormalMappingShader() {

}

void NormalMappingShader::loadRowNumber(int rowNumber) {
    loadFloat(location_rowNumber, (float) rowNumber);
}

void NormalMappingShader::loadOffset(float x, float y) {
    load2DVector(location_offset, vec2(x, y));
}

void NormalMappingShader::loadClipPlane(vec4 plane) {
    load4DVector(location_plane, plane);
}

void NormalMappingShader::connectTextureUnits() {
    loadInt(location_modelTexture, 0);
    loadInt(location_normalMap, 1);
}

vec3 NormalMappingShader::getEyeSpacePosition(Light *light, mat4 viewMatrix){
    vec3 position = light->GetPosition();
    vec4 eyeSpacePos = vec4(position.x, position.y, position.z, 1.0f);
    eyeSpacePos = viewMatrix * eyeSpacePos;
    return vec3(eyeSpacePos.x, eyeSpacePos.y, eyeSpacePos.z);
}
