//
// Created by jack on 26/11/15.
//

#ifndef SPLENDITORMASTER_LIGHT_H
#define SPLENDITORMASTER_LIGHT_H


#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include <glm/core/type.hpp>

using namespace std;
using namespace glm;


class Light {
public:
    Light(vec3 position,vec3 colour);
    Light(vec3 position,vec3 colour, vec3 attenuation);
    ~Light();
    void SetColour(vec3 colour);
    void SetPosition(vec3 position);
    void SetAttenuation(vec3 attenuation);
    vec3 GetAttenuation() const;
    vec3 GetPosition() const;
    vec3 GetColour() const;
private:
    vec3 position;
    vec3 colour;
    vec3 attenuation;
};


#endif //SPLENDITORMASTER_LIGHT_H
