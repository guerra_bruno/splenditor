//
// Created by jack on 11/03/16.
//

#ifndef SPLENDITORMASTER_SHADOWEDENTITYSHADER_H
#define SPLENDITORMASTER_SHADOWEDENTITYSHADER_H


#include "EntityShader.h"

class ShadowedEntityShader : public EntityShader {
public:
    ShadowedEntityShader(const char *vertex_file_path, const char *fragment_file_path);
    ~ShadowedEntityShader();
    void connectTextureUnits();
    void loadMapSize(float mapSize);
    void loadToShadowMapSpace(mat4 matrix);
    void loadShadowDistance(float shadowDistance);
    void loadPCF(int pcf);

protected:
    void getAllUniformLocations();

private:
    GLint location_toShadowMapSpace;
    GLint location_textureSampler;
    GLint location_shadowMap;
    GLint location_mapSize;
    GLint location_pcfCount;
    GLint location_shadowDistance;

};


#endif //SPLENDITORMASTER_SHADOWEDENTITYSHADER_H
