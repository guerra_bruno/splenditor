//
// Created by jack on 18/02/16.
//

#ifndef SPLENDITORMASTER_PARTICLESHADER_H
#define SPLENDITORMASTER_PARTICLESHADER_H


#include <Splenditor/RenderEngine/ShaderProgram.h>

class ParticleShader : public ShaderProgram {
public:
    ParticleShader(const char *vertex_file_path, const char *fragment_file_path);
    ~ParticleShader() {}
    void loadNumberOfRows(float numRows);
    void loadProjectionMatrix(mat4 projectionMatrix);

protected:
    void bindAttributes();
    void getAllUniformLocations();

private:
    GLint location_projectionMatrix;
    GLint location_numberOfRows;
};


#endif //SPLENDITORMASTER_PARTICLESHADER_H
