//
// Created by jack on 16/02/16.
//

#include <Splenditor/FontMeshCreator/TextMeshCreator.h>


TextMeshCreator::TextMeshCreator() {

}

TextMeshCreator::TextMeshCreator(string metaFile, DisplayManager* display) {
    metaData = new MetaFile(metaFile, display);
}

TextMeshCreator::~TextMeshCreator() {
    delete metaData;

}

TextMeshData *TextMeshCreator::createTextMesh(GUIInfo *text) {
    list<Line> lines = createStructure(text);
    TextMeshData* data = createQuadVertices(text, lines);
    return data;
}

list<Line> TextMeshCreator::createStructure(GUIInfo *text) {
    list<Line> lines;
    Line currentLine = Line(metaData->getSpaceWidth(), text->getFontSize(), text->getLineMaxSize());
    Word currentWord = Word(text->getFontSize());
    for (int i=0; i<text->getTextString().size(); i++) {
        int ascii = (int) text->getTextString()[i];
        if (ascii == metaData->SPACE_ASCII) {
            bool added = currentLine.attemptToAddWord(currentWord);
            if (!added) {
                lines.push_back(currentLine);
                currentLine = Line(metaData->getSpaceWidth(), text->getFontSize(), text->getLineMaxSize());
                currentLine.attemptToAddWord(currentWord);
            }
            currentWord = Word(text->getFontSize());
            continue;
        }
        currentWord.addCharacter(metaData->getCharacter(ascii));
    }
    completeStructure(lines, currentLine, currentWord, text);
    return lines;
}

void TextMeshCreator::completeStructure(list<Line>& lines, Line& currentLine, Word& currentWord, GUIInfo * text){
    bool added = currentLine.attemptToAddWord(currentWord);
    if (!added) {
        lines.push_back(currentLine);
        currentLine = Line(metaData->getSpaceWidth(), text->getFontSize(), text->getLineMaxSize());
        currentLine.attemptToAddWord(currentWord);
    }
    lines.push_back(currentLine);
}

TextMeshData *TextMeshCreator::createQuadVertices(GUIInfo *text, list<Line>& lines) {
    text->setNumberOfLines((int) lines.size());
    double curserX = 0.0;
    double curserY = 0.0;
    list<float> vertices, textureCoords;
    for (Line& line: lines) {
        if (text->isCenterText()) {
            curserX = (line.getMaxLength() - line.getCurrentLineLength()) / 2;
        }
        for (Word& word : line.getWords()) {
            for (Character*& letter : word.getCharacters()) {
                addVerticesForCharacter(curserX, curserY, *letter, text->getFontSize(), vertices);
                addTexCoords(textureCoords, letter->getXTextureCoord(), letter->getYTextureCoord(), letter->getXMaxTextureCoord(), letter->getYMaxTextureCoord());
                curserX += letter->getXAdvance()* text->getFontSize();
            }
            curserX += metaData->getSpaceWidth() * text->getFontSize();
        }
        curserX = 0;
        curserY += metaData->LINE_HEIGHT * text->getFontSize();
    }

    return new TextMeshData(vector<float>{begin(vertices), end(vertices)}, vector<float>{begin(textureCoords), end(textureCoords)});
}

void TextMeshCreator::addVerticesForCharacter(double curserX, double curserY, Character& character, double fontSize, list<float>& vertices) {
    double x = curserX + (character.getXOffset() * fontSize);
    double y = curserY + (character.getYOffset() * fontSize);
    double maxX = x + (character.getSizeX() * fontSize);
    double maxY = y + (character.getSizeY() * fontSize);
    double properX = (2 * x) - 1;
    double properY = (-2 * y) + 1;
    double properMaxX = (2 * maxX) - 1;
    double properMaxY = (-2 * maxY) + 1;
    addVertices(vertices, properX, properY, properMaxX, properMaxY);
}

void TextMeshCreator::addVertices(list<float>& vertices, double x, double y, double maxX, double maxY) {
    vertices.push_back((float) x);
    vertices.push_back((float) y);
    vertices.push_back((float) x);
    vertices.push_back((float) maxY);
    vertices.push_back((float) maxX);
    vertices.push_back((float) maxY);
    vertices.push_back((float) maxX);
    vertices.push_back((float) maxY);
    vertices.push_back((float) maxX);
    vertices.push_back((float) y);
    vertices.push_back((float) x);
    vertices.push_back((float) y);
}

void TextMeshCreator::addTexCoords(list<float>& texCoords, double x, double y, double maxX, double maxY) {
    texCoords.push_back((float) x);
    texCoords.push_back((float) y);
    texCoords.push_back((float) x);
    texCoords.push_back((float) maxY);
    texCoords.push_back((float) maxX);
    texCoords.push_back((float) maxY);
    texCoords.push_back((float) maxX);
    texCoords.push_back((float) maxY);
    texCoords.push_back((float) maxX);
    texCoords.push_back((float) y);
    texCoords.push_back((float) x);
    texCoords.push_back((float) y);
}
