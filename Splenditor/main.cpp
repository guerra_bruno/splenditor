#include <Splenditor/ToolBox/World.h>
#include <Splenditor/RenderEngine/MasterRenderer.h>
#include <Splenditor/Particles/ParticleSystem.h>
#include <Splenditor/FontMeshCreator/GUIText.h>
#include <Splenditor/Water/WaterRenderer.h>
#include <Splenditor/ToolBox/MousePicker.h>
#include <iomanip>



#define FPS 25

using namespace std;

//Global pointers
World *world;
DisplayManager *displayManager;
Mouse *mouse;
MousePicker *picker;
MasterRenderer *renderer;
WaterRenderer *waterRenderer;
GuiRenderer *guiRenderer;
TextMaster *textMaster;
ParticleMaster *particleMaster;
ParticleSystem *particleSystem;
GUIText *guitext;

// global variables
double lastTime = 0.0;
bool keys[GLFW_KEY_LAST] = { false };
bool day = true;

// methods
void renderFPS(float frameRate);

void cleanUp();

void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods);

void mouse_callback(GLFWwindow *window, int buttom, int action, int mods);

void scroll_callback(GLFWwindow *window, double xoffset, double yoffset);

ostream &operator<< (ostream &out, const vec3 &vec) {
    out << "[" << vec.x << ", " << vec.y << ", "<< vec.z  << "]";
    return out;
}

// main function
int main() {


    // glfw setup
    displayManager = new DisplayManager(1280, 720);
    glfwSetKeyCallback(displayManager->getWindow(), key_callback);
    glfwSetMouseButtonCallback(displayManager->getWindow(), mouse_callback);
    glfwSetScrollCallback(displayManager->getWindow(), scroll_callback);
    mouse = new Mouse(displayManager);

    // loading world
    world = new World("res/lake_house/world.json", mouse);

    // renderers
    renderer = new MasterRenderer(displayManager, world);
    guiRenderer = new GuiRenderer(world->loader);
    waterRenderer = new WaterRenderer(world->loader, renderer->getProjectionMatrix(), new WaterFrameBuffers(displayManager));
    picker = new MousePicker(world->camera, mouse, world->terrain, renderer->getProjectionMatrix());
    particleMaster = new ParticleMaster(world->loader, renderer->getProjectionMatrix());

    // text rendering
    textMaster = new TextMaster();
    textMaster->init(world->loader);

//    guitext = new GUIText(
//            "Amo muito a moia genti!!",
//            4,
//            vec2(0.0, 0.5),
//            1.0,
//            new FontType(world->loader->loadFontTexture("res/lake_house/files/segoe.png"), "res/lake_house/files/segoe.fnt", displayManager),
//            textMaster,
//            true
//    );
//
//    guitext->getText()->getGuiInfo()->setColour(vec3(1.0, 0.0, 0.0));

    GuiTexture* shadowMap = new GuiTexture(renderer->getShadowMapTexture(), vec2(0.75f, 0.75f), vec2(0.25f, 0.25f));

    world->guis.push_back(shadowMap);

    // particle system
    ParticleTexture *parTex = new ParticleTexture(world->loader->loadTexture("res/lake_house/files/fire.png"), 4, true);
    particleSystem = new ParticleSystem(parTex, 300, 5, 0.05, 10.3, 1.6, particleMaster, world);
    //particleSystem = new ParticleSystem(parTex, 100, 5, 0.05, 3.0, 1.6, particleMaster, world);
    particleSystem->randomizeRotation();
    particleSystem->setDirection(vec3(0.0,1.0,0.0), 3.04f);
    particleSystem->setLifeError(0.02f);
    particleSystem->setSpeedError(0.4f);
    particleSystem->setScaleError(0.2f);

    // main game loop
    while (!glfwWindowShouldClose(displayManager->getWindow())) {
        renderFPS(FPS);
    }

    //End the application

    cleanUp();
    return 0;
}


// main game loop
void renderFPS(float frameRate) {
    double currentTime = glfwGetTime();
    world->delta = (float) (currentTime - lastTime);

    if (world->delta >= 1.0 / frameRate) {
        displayManager->updateDisplay();
        cout << 1.0 / (currentTime - lastTime) << endl;
        lastTime = currentTime;

        // ******** Game Loop **************


        std::list<Entity*> all_entities = world->entities;
        all_entities.insert(all_entities.end(), world->normalMapEntities.begin(), world->normalMapEntities.end());
        all_entities.push_back(world->player);

        world->player->move(keys, world->delta, world->terrain);
        world->camera->move(world->terrain, world->delta);
        renderer->renderShadowMap(all_entities, world->lights[0]);
        picker->update();

//        particleSystem->generateParticles(vec3(304.000, 12.969, -432.000));
//        particleSystem->generateParticles(world->player->GetPosition());
        vec3 emitter = world->camera->position;
        emitter.y -= 3.0;
        particleSystem->generateParticles(emitter);
        particleMaster->update(world);

        //  if (keys[GLFW_KEY_Y]) particleMaster->addParticle(new Particle(world->player->GetPosition(), vec3(0.0, 30.0, 0.0), 1.0, 4.0, 0.0, 1.0));





        // if water exist
        if(world->waters.size() > 0) {

            // reflect
            glEnable(GL_CLIP_DISTANCE0);
            waterRenderer->fbos->bindReflectionFrameBuffer();
            float distance = 2 * (world->camera->GetPosition().y - world->waters[0]->getHeight());
            world->camera->position.y -= distance;
            world->camera->invertPitch();
            world->camera->calculateViewMatrix();
            renderer->renderScene(world, vec4(0, 1, 0, -world->waters[0]->getHeight()+1.0f), world->delta, day, false, true);
            world->camera->position.y += distance;
            world->camera->invertPitch();

            // refract
            world->camera->calculateViewMatrix();
            waterRenderer->fbos->bindRefractionFrameBuffer();
            renderer->renderScene(world, vec4(0, -1, 0, world->waters[0]->getHeight()+1.0f), world->delta, day, true, false);
            glDisable(GL_CLIP_DISTANCE0);
            waterRenderer->fbos->unbindCurrentFrameBuffer(displayManager);

        }

        renderer->renderScene(world, vec4(0, -1, 0, 20), world->delta, day, false, false);
        if(world->waters.size() > 0) waterRenderer->render(world, world->camera);
        particleMaster->renderParticles(world->camera);
        textMaster->render();
        guiRenderer->render(world->guis);

        glfwSwapBuffers(displayManager->getWindow());
        glfwPollEvents();
    }
}

void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods) {
    keys[key] = (action != GLFW_RELEASE);
    if (keys[GLFW_KEY_ESCAPE]) glfwSetWindowShouldClose(displayManager->getWindow(), GL_TRUE);
    if (keys[GLFW_KEY_N]) day = !day;
    (keys[GLFW_KEY_P]) ? glPolygonMode(GL_FRONT_AND_BACK, GL_LINE) : glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void scroll_callback(GLFWwindow *window, double xoffset, double yoffset) {
    mouse->setWheelOffset((float) yoffset);
    world->camera->calculateZoom();

}

void mouse_callback(GLFWwindow *window, int buttom, int action, int mods) {
    mouse->updateAllPosition();
    mouse->setButtom(buttom, (action != GLFW_RELEASE)); // true if we have GLFW_PRESS or GLFW_REPEAT
}

void cleanUp() {
    delete renderer;
    delete guiRenderer;
    delete mouse;
    delete picker;
    delete waterRenderer;
    delete textMaster;
    delete particleMaster;
    delete displayManager;
    delete particleSystem;


    cout << world->player->GetPosition() << endl;

    delete world;
    delete guitext;

    /*
    std::cout << std::fixed;
    for(unsigned int i=0; i < world->entities.size(); i++){
        std::cout << std::setprecision(3);

        if(!world->entities[i]->GetName().compare("bonfire")) {
            world->entities[i]->PrintWorldObject();
        }
    }
    */

}


