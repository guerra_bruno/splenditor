//
// Created by jack on 16/02/16.
//

#ifndef SPLENDITORMASTER_GUIINFO_H
#define SPLENDITORMASTER_GUIINFO_H

#include <Splenditor/ToolBox/common.h>
#include <Splenditor/RenderEngine/DisplayManager.h>
#include <string>
#include <vector>

using namespace std;

class GUIInfo {

public:
    GUIInfo(string text, float fontSize, vec2 position, float maxLineLength, bool centered = false);
    ~GUIInfo();
    void remove();
    void setMeshInfo(GLuint vao, vector<GLuint> vbo, int verticesCount);
    string getTextString() { return textString; }
    void setTextString( string textString) { this->textString = textString; }
    float getFontSize()  { return fontSize; }
    void setFontSize(float fontSize) { this->fontSize = fontSize; }
    float getLineMaxSize() { return lineMaxSize; }
    void setLineMaxSize(float lineMaxSize) { this->lineMaxSize = lineMaxSize; }
    int getVertexCount() { return vertexCount; }
    void setVertexCount(int vertexCount) { this->vertexCount = vertexCount; }
    GLuint getTextMeshVao() { return textMeshVao; }
    vector<GLuint> getTextMeshVbo() { return textMeshVbos; }
    void setTextMeshVao(GLuint textMeshVao) { this->textMeshVao = textMeshVao; }
    void setTextMeshVbo(vector<GLuint> textMeshVbo) { this->textMeshVbos = textMeshVbo; }
    int getNumberOfLines() const { return numberOfLines; }
    void setNumberOfLines(int numberOfLines) { this->numberOfLines = numberOfLines; }
    bool isCenterText() { return centerText; }
    void setCenterText(bool centerText) { this->centerText = centerText; }
    vec2 getPosition() const { return position; }
    void setPosition(vec2 position) { this->position = position; }
    vec3 getColour() { return colour; }
    void setColour(vec3 colour) { this->colour = colour; }

private:
    string textString;
    float fontSize;
    float lineMaxSize;
    int vertexCount;
    GLuint textMeshVao;
    vector<GLuint> textMeshVbos;
    int numberOfLines;
    bool centerText;
    vec2 position;
    vec3 colour = vec3(0.0, 0.0, 0.0);
};


#endif //SPLENDITORMASTER_GUIINFO
