//
// Created by jack on 20/02/16.
//

#include "ShadowFrameBuffer.h"

ShadowFrameBuffer::ShadowFrameBuffer(int width, int height, DisplayManager* display) {
    this->display = display;
    this->WIDTH = width;
    this->HEIGHT = height;
    initialiseFrameBuffer();
}

ShadowFrameBuffer::~ShadowFrameBuffer() {
    glDeleteFramebuffers(1, &fbo);
    glDeleteTextures(1, &shadowMap);

}

void ShadowFrameBuffer::bindFrameBuffer() {
    bindFrameBuffer(fbo, WIDTH, HEIGHT);
}

void ShadowFrameBuffer::bindFrameBuffer(GLuint &frameBuffer, int width, int height) {
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, frameBuffer);
    glViewport(0, 0, width, height);
}

void ShadowFrameBuffer::unbindFrameBuffer() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, display->getWidth(), display->getHeight());
}

void ShadowFrameBuffer::createFrameBuffer(GLuint &frameBuffer) {
    glGenFramebuffers(1, &frameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    glDrawBuffer(GL_NONE);
}

void ShadowFrameBuffer::initialiseFrameBuffer() {
    createFrameBuffer(fbo);
    createDepthBufferAttachment(WIDTH, HEIGHT, shadowMap);;
    unbindFrameBuffer();
}


void ShadowFrameBuffer::createDepthBufferAttachment(int width, int height, GLuint &texture) {
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, (const char*) NULL);
    //glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, texture, 0);
}
