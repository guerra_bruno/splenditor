//
// Created by jack on 22/02/16.
//

#ifndef SPLENDITORMASTER_SHADOWMAPENTITYRENDERER_H
#define SPLENDITORMASTER_SHADOWMAPENTITYRENDERER_H


#include <Splenditor/Models/TexturedModel.h>
#include <Splenditor/Entities/Entity.h>
#include "ShadowShader.h"
#include <list>
#include <map>

class ShadowMapEntityRenderer {

public:
    ShadowMapEntityRenderer(ShadowShader* shader, mat4* projectionViewMatrix);
    void render(map<TexturedModel*, std::list<Entity*>> entities);
    void bindModel(RawModel* rawModel);
    void prepareInstance(Entity* entity);

private:
    mat4* projectionViewMatrix;
    ShadowShader* shader;
};


#endif //SPLENDITORMASTER_SHADOWMAPENTITYRENDERER_H
