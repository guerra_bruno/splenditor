//
// Created by jack on 23/01/16.
//

#ifndef SPLENDITORMASTER_WATERFRAMEBUFFERS_H
#define SPLENDITORMASTER_WATERFRAMEBUFFERS_H


#include <Splenditor/RenderEngine/Loader.h>
#include <Splenditor/RenderEngine/DisplayManager.h>

class WaterFrameBuffers {

public:
    WaterFrameBuffers(DisplayManager* display);
    ~WaterFrameBuffers();
    void bindReflectionFrameBuffer();
    void bindRefractionFrameBuffer();
    void unbindCurrentFrameBuffer(DisplayManager* display);
    GLuint getRefractionDepthTexture() const;
    GLuint getRefractionTexture() const;
    GLuint getReflectionTexture() const;
    void initialiseReflectionFrameBuffer(DisplayManager* display);
    void initialiseRefractionFrameBuffer(DisplayManager* display);
    void bindFrameBuffer(GLuint &frameBuffer, int width, int height);
    void createFrameBuffer(GLuint &frameBuffer);
    void createTextureAttachment(int width, int height, GLuint &texture);
    void createDepthTextureAttachment(int width, int height, GLuint &texture);
    void createDepthBufferAttachment(int width, int height, GLuint &depthBuffer);

private:
    GLuint reflectionFrameBuffer;
    GLuint reflectionTexture;
    GLuint reflectionDepthBuffer;

    GLuint refractionFrameBuffer;
    GLuint refractionTexture;
    GLuint refractionDepthTexture;


    const static int REFLECTION_WIDTH =  1.5 * 320;
    const static int REFLECTION_HEIGHT =  1.5 * 180;

    const static int REFRACTION_WIDTH =   1.5 * 320;
    const static int REFRACTION_HEIGHT =  1.5 * 180;
};


#endif //SPLENDITORMASTER_WATERFRAMEBUFFERS_H
