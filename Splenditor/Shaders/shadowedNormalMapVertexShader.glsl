
in vec3 position;
in vec2 textureCoords;
in vec3 normal;
in vec3 tangent;

out vec2 pass_textureCoords;
out vec3 toLightVector[max_lights];
out vec3 toCameraVector;
out float visibility;
out vec4 shadowCoords;

uniform mat4 transformationMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform vec4 plane;
uniform vec3 lightPositionEyeSpace[max_lights];
uniform mat4 toShadowMapSpace;
uniform float shadowDistance;
uniform float rowNumber;
uniform vec2 offset;


const float density = 0.0000000003;
const float gradient = 1.000000005;
const float transitionDistance = 15.0;


void main(void){

	vec4 worldPosition = transformationMatrix * vec4(position,1.0);
    shadowCoords = toShadowMapSpace * worldPosition;

	gl_ClipDistance[0] = dot(worldPosition, plane);
	mat4 modelViewMatrix = viewMatrix * transformationMatrix;
    vec4 positionRelativeToCam = viewMatrix * worldPosition;
	gl_Position = projectionMatrix * positionRelativeToCam;

	pass_textureCoords = ((textureCoords) / rowNumber) + offset;


	vec3 surfaceNormal = (modelViewMatrix * vec4(normal,0.0)).xyz;

	vec3 norm = normalize(surfaceNormal);
	vec3 tang = normalize((modelViewMatrix * vec4(tangent, 0.0)).xyz);
	vec3 bitang = normalize(cross(norm,tang));

	mat3 toTangentSpace = mat3(
	    tang.x, bitang.x, norm.x,
	    tang.y, bitang.y, norm.y,
	    tang.z, bitang.z, norm.z
	);

	for(int i=0;i<max_lights;i++){
		toLightVector[i] = toTangentSpace * (lightPositionEyeSpace[i] - positionRelativeToCam.xyz);
	}
	toCameraVector = toTangentSpace * (-positionRelativeToCam.xyz);

	float distance = length(positionRelativeToCam.xyz);
	visibility = exp(-pow((distance*density),gradient));
	visibility = clamp(visibility,0.0,1.0);

    distance = distance - (shadowDistance - transitionDistance);
    distance = distance / transitionDistance;
    shadowCoords.w = clamp(1.0-distance, 0.0, 1.0);

}