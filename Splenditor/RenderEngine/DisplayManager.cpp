//
// Created by jack on 20/11/15.
//



#include "DisplayManager.h"

DisplayManager::DisplayManager(int width, int height) {

    this->width = width;
    this->height = height;

    // initializes glfw
    glfwInit();
    glfwWindowHint(GLFW_SAMPLES, 0);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    // open a window and create its OpenGL context
    window = glfwCreateWindow( this->width, this->height, "Splenditor", NULL, NULL);
    glfwSetWindowPos(window, 350, 150);
    glfwMakeContextCurrent(window);

    // Initialize GLEW
    glewExperimental = GL_TRUE;
    glewInit();



}

void DisplayManager::updateDisplay() {
    //float ratio;
    glfwGetFramebufferSize(window, &width, &height);
    //ratio = width / (float) height;
    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT);
}

DisplayManager::~DisplayManager() {
    glfwDestroyWindow(window);
    glfwTerminate();
}
