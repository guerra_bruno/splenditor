//
// Created by jack on 10/12/15.
//

#ifndef SPLENDITORMASTER_MOUSE_H
#define SPLENDITORMASTER_MOUSE_H

#include "common.h"
#include <Splenditor/RenderEngine/DisplayManager.h>

class Mouse {
public:
    Mouse(DisplayManager *display);
    ~Mouse();
    bool isButtomActive(int buttomCode);
    void setButtom(int code, bool value);
    void setPosition(vec2 position);
    void setWheelOffset(float wheelOffset);
    float getWheelOffset();
    void updatePosition();
    DisplayManager* getDisplay();
    void updateAllPosition();
    vec2 getPosition();
    vec2 getOffset();
private:
    float wheelOffset;
    vec2 position;
    vec2 old_position;
    DisplayManager *display;
    bool mouse_buttons[GLFW_MOUSE_BUTTON_LAST];
};


#endif //SPLENDITORMASTER_MOUSE_H
