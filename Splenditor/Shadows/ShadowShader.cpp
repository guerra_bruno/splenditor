//
// Created by jack on 20/02/16.
//

#include "ShadowShader.h"

ShadowShader::ShadowShader(const char *vertex_file_path, const char *fragment_file_path) : ShaderProgram(vertex_file_path, fragment_file_path){
    bindAttributes();
    glLinkProgram(getProgramID());
    getAllUniformLocations();
}

void ShadowShader::loadMvpMatrix(mat4 mvpMatrix) {
    loadMatrix(location_mvpMatrix, mvpMatrix);
}

void ShadowShader::bindAttributes() {
    bindAttribute(0, "in_position");
    bindAttribute(1, "in_textureCoords");
}

void ShadowShader::getAllUniformLocations() {
    location_mvpMatrix = getUniformLocation("mvpMatrix");
}
