//
// Created by jack on 1/12/15.
//

#include "TerrainTexturePack.h"

TerrainTexturePack::TerrainTexturePack(TerrainTexture *bi, TerrainTexture *r, TerrainTexture *g, TerrainTexture *b) {
    this->backgroundTexture = bi;
    this->rTexture = r;
    this->gTexture = g;
    this->bTexture = b;
}

TerrainTexture* TerrainTexturePack::getBTexture() const {
    return bTexture;
}

TerrainTexture* TerrainTexturePack::getGTexture() const {
    return gTexture;
}

TerrainTexture* TerrainTexturePack::getRTexture() const {
    return rTexture;
}

TerrainTexture* TerrainTexturePack::getBackgroundTexture() const {
    return backgroundTexture;
}

TerrainTexturePack::~TerrainTexturePack() {
    delete backgroundTexture;
    delete rTexture;
    delete gTexture;
    delete bTexture;
}
