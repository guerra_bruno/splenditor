//
// Created by jack on 23/01/16.
//

#include "WaterFrameBuffers.h"

WaterFrameBuffers::WaterFrameBuffers(DisplayManager* display) {
    initialiseReflectionFrameBuffer(display);
    initialiseRefractionFrameBuffer(display);
}

WaterFrameBuffers::~WaterFrameBuffers() {
    glDeleteFramebuffers(1, &reflectionFrameBuffer);
    glDeleteFramebuffers(1, &refractionFrameBuffer);
    glDeleteRenderbuffers(1, &reflectionDepthBuffer);
    glDeleteTextures(1, &reflectionTexture);
    glDeleteTextures(1, &refractionTexture);
    glDeleteTextures(1, &refractionDepthTexture);
}

void WaterFrameBuffers::bindReflectionFrameBuffer() {
    bindFrameBuffer(reflectionFrameBuffer,REFLECTION_WIDTH,REFLECTION_HEIGHT);
}

void WaterFrameBuffers::bindRefractionFrameBuffer() {
    bindFrameBuffer(refractionFrameBuffer, REFRACTION_WIDTH, REFRACTION_HEIGHT);
}

void WaterFrameBuffers::unbindCurrentFrameBuffer(DisplayManager* display) {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, display->getWidth(), display->getHeight());
}

GLuint WaterFrameBuffers::getRefractionDepthTexture() const {
    return refractionDepthTexture;
}

GLuint WaterFrameBuffers::getRefractionTexture() const {
    return refractionTexture;
}

GLuint WaterFrameBuffers::getReflectionTexture() const {
    return reflectionTexture;
}

void WaterFrameBuffers::initialiseReflectionFrameBuffer(DisplayManager* display) {
    glGenTextures(1, &reflectionTexture);
    createFrameBuffer(reflectionFrameBuffer);
    createTextureAttachment(REFLECTION_WIDTH, REFLECTION_HEIGHT, reflectionTexture);
    createDepthBufferAttachment(REFLECTION_WIDTH, REFLECTION_HEIGHT, reflectionDepthBuffer);
    unbindCurrentFrameBuffer(display);
}

void WaterFrameBuffers::initialiseRefractionFrameBuffer(DisplayManager* display) {
    createFrameBuffer(refractionFrameBuffer);
    createTextureAttachment(REFRACTION_WIDTH, REFRACTION_HEIGHT, refractionTexture);
    createDepthTextureAttachment(REFRACTION_WIDTH, REFRACTION_HEIGHT, refractionDepthTexture);
    unbindCurrentFrameBuffer(display);
}

void WaterFrameBuffers::bindFrameBuffer(GLuint &frameBuffer, int width, int height) {
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    glViewport(0, 0, width, height);
}

void WaterFrameBuffers::createFrameBuffer(GLuint &frameBuffer) {
    glGenFramebuffers(1, &frameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    glDrawBuffer(GL_COLOR_ATTACHMENT0);
}


void WaterFrameBuffers::createTextureAttachment(int width, int height, GLuint &texture) {
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, (const char*) NULL);
    glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture, 0);


}

void WaterFrameBuffers::createDepthTextureAttachment(int width, int height, GLuint &texture) {
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, (const char*) NULL);
    glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, texture, 0);
}

void WaterFrameBuffers::createDepthBufferAttachment(int width, int height, GLuint &depthBuffer) {
    glGenRenderbuffers(1, &depthBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
}
