//
// Created by jack on 27/11/15.
//

#ifndef SPLENDITORMASTER_TERRAINRENDERER_H
#define SPLENDITORMASTER_TERRAINRENDERER_H


#include <Splenditor/Terrains/Terrain.h>
#include <Splenditor/ToolBox/World.h>
#include "ShadowedTerrainShader.h"

class TerrainRenderer {
public:
    TerrainRenderer(mat4 projectionMatrix);
    ~TerrainRenderer();
    void rendering(vector<Terrain *> terrains, TerrainShader* shader);
    void render(vector<Terrain *> terrains, vec4 clipPlane, vec3 colour, World *world);
    void render(vector<Terrain *> terrains, vec4 clipPlane, vec3 colour, World *world, float shadowResolution, mat4 toShadowMapSpace, int PCF, float shadowDistance);
    void prepare(vec4 clipPlane, vec3 colour, World *world, TerrainShader* shader);
    void unbindTexturedModel();
    void prepareTexturedModel(Terrain *terrain, TerrainShader* shader);
    void bindTextures(Terrain *terrain);
    void loadModelMatrix(Terrain *terrain, TerrainShader* shader);
private:
    ShadowedTerrainShader *shadow_terrain_shader;
    TerrainShader *terrain_shader;
};


#endif //SPLENDITORMASTER_TERRAINRENDERER_H
