//
// Created by jack on 22/02/16.
//

#ifndef SPLENDITORMASTER_SHADOWBOX_H
#define SPLENDITORMASTER_SHADOWBOX_H


#include <Splenditor/ToolBox/Camera.h>

class ShadowBox {

public:
    ShadowBox(Camera* camera, float NEAR_PLANE, float FOV, mat4* lightViewMatrix, DisplayManager *display);
    void update();
    vec3 getCenter();
    float getWidth() { return maxX - minX; }
    float getHeight() { return maxY - minY; }
    float getLength() { return maxZ - minZ; }
    vec3 croiss(vec3 left, vec3 right) {return vec3(left.y * right.z - left.z * right.y, right.x * left.z - right.z * left.x, left.x * right.y - left.y * right.x ); }
    float doti(vec3 left, vec3 right) { return left.x * right.x + left.y * right.y + left.z * right.z; }

private:
    float OFFSET, SHADOW_DISTANCE, NEAR_PLANE, FOV;
    vec4 UP, FORWARD;
    float minX, maxX;
    float minY, maxY;
    float minZ, maxZ;
    float farHeight, farWidth, nearHeight, nearWidth;
    mat4* lightViewMatrix;
    Camera* cam;
    DisplayManager *display;
    vector<vec4> calculateFrustumVertices(mat4 rotation, vec3 forwardVector, vec3 centerNear, vec3 centerFar);
    vec4 calculateLightSpaceFrustumCorner(vec3 startPoint, vec3 direction, float width);
    mat4 calculateCameraRotationMatrix();
    void calculateWidthsAndHeights();
    float getAspectRatio();
};


#endif //SPLENDITORMASTER_SHADOWBOX_H
