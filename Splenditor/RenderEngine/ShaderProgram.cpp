//
// Created by jack on 20/11/15.
//

#include <GL/glew.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>
#include "ShaderProgram.h"

using namespace std;

ShaderProgram::ShaderProgram(const char *vertex_file_path, const char *fragment_file_path) {
    // Create the Shaders
    vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

    if (!CompileShader(vertex_file_path, vertexShaderID) || !CompileShader(fragment_file_path, fragmentShaderID)) {
        exit(1);
    }

    // Link the program
    programID = glCreateProgram();
    glAttachShader(programID, vertexShaderID);
    glAttachShader(programID, fragmentShaderID);
}


int ShaderProgram::CompileShader(const char* ShaderPath, const GLuint ShaderID) {

    // Read shadow_terrain_shader code from file
    std::string ShaderCode;
    std::ifstream ShaderStream(ShaderPath, std::ios::in);

    ShaderCode += "#version 330 core\n#define max_lights ";
    ShaderCode += to_string(max_lights);

    if (ShaderStream.is_open()) {
        std::string Line = "";
        while (getline(ShaderStream, Line)) {
            ShaderCode += string("\n") + Line;
        }
        ShaderStream.close();
    } else {
        std::cerr << "Cannot open " << ShaderPath << ". Are you in the right directory?" << std::endl;
        return 0;
    }


    // Compile Shader
    char const *SourcePointer = ShaderCode.c_str();
    glShaderSource(ShaderID, 1, &SourcePointer, NULL);
    glCompileShader(ShaderID);

    return 1;
}


void ShaderProgram::start() {
    glUseProgram(programID);
}

void ShaderProgram::stop() {
    glUseProgram(0);
}

void ShaderProgram::cleanUp() {
    cout << "shader " << programID << " cleaned." << endl;
    stop();
    glDetachShader(programID, vertexShaderID);
    glDetachShader(programID, fragmentShaderID);
    glDeleteShader(vertexShaderID);
    glDeleteShader(fragmentShaderID);
    glDeleteProgram(programID);
}

void ShaderProgram::bindAttribute(int attribute, const char* variableName) {
    glBindAttribLocation(programID, (GLuint) attribute, variableName);
}

GLint ShaderProgram::getUniformLocation(string uniformName) {
    return glGetUniformLocation(programID, uniformName.c_str());
}

void ShaderProgram::loadFloat(int location, float value) {
    glUniform1f(location, value);
}

void ShaderProgram::load3DVector(int location, vec3 vector) {
    glUniform3f(location, vector.x, vector.y, vector.z);
}

void ShaderProgram::load4DVector(int location, vec4 vector) {
    glUniform4f(location, vector.x, vector.y, vector.z, vector.w);
}
void ShaderProgram::load2DVector(int location, vec2 vector) {
    glUniform2f(location, vector.x, vector.y);
}

void ShaderProgram::loadBoolean(int location, bool value) {
    value ? glUniform1f(location, 1.0) : glUniform1f(location, 0.0);
}

void ShaderProgram::loadMatrix(int location, mat4 matrix) {
    glUniformMatrix4fv(location, 1, (GLboolean) false, value_ptr(matrix));
}

void ShaderProgram::loadInt(int location, int value) {
    glUniform1i(location, value);
}

