//
// Created by jack on 16/02/16.
//

#include <Splenditor/FontMeshCreator/Word.h>
#include <cstdio>

Word::Word(double fontSize) {
    this->width = 0;
    this->fontSize = fontSize;
}

void Word::addCharacter(Character* character) {
    characters.push_back(character);
    width += character->getXAdvance() * fontSize;
}

Word::~Word() {
    for(auto& c : characters){
        c->~Character();
    }
    characters.clear();
}
