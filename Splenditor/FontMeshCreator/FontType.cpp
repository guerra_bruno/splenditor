//
// Created by jack on 16/02/16.
//

#include <Splenditor/FontMeshCreator/FontType.h>


FontType::FontType(GLuint textureAtlas, string path, DisplayManager* display) {
    this->textureAtlas = textureAtlas;
    this->loader = new TextMeshCreator(path, display);
}

FontType::~FontType() {
    delete loader;
}

GLuint FontType::getTextureAtlas() {
    return this->textureAtlas;
}

TextMeshData *FontType::loadText(GUIInfo *text) {
    return loader->createTextMesh(text);
}
