//
// Created by jack on 5/12/15.
//

#include "GuiShader.h"


GuiShader::GuiShader(const char *vertex_file_path, const char *fragment_file_path) : ShaderProgram(vertex_file_path, fragment_file_path){
    bindAttributes();
    glLinkProgram(getProgramID());
    getAllUniformLocations();
}

GuiShader::~GuiShader() {
}

void GuiShader::loadTransformationMatrix(mat4 matrix) {
    loadMatrix(location_transformationMatrix, matrix);
}

void GuiShader::getAllUniformLocations() {
    location_transformationMatrix = getUniformLocation("transformationMatrix");
}

void GuiShader::bindAttributes() {
    this->bindAttribute(0, "position");
}
