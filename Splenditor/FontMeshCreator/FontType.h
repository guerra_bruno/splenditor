//
// Created by jack on 16/02/16.
//

#ifndef SPLENDITORMASTER_FONTTYPE_H
#define SPLENDITORMASTER_FONTTYPE_H


#include <Splenditor/FontMeshCreator/TextMeshCreator.h>
#include "GUIInfo.h"


class FontType {
public:
    FontType(GLuint textureAtlas, string path, DisplayManager* display);
    ~FontType();
    GLuint getTextureAtlas();
    TextMeshData* loadText(GUIInfo *text);
private:
    TextMeshCreator* loader;
    GLuint textureAtlas;
};


#endif //SPLENDITORMASTER_FONTTYPE_H
