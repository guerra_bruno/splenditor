//
// Created by jack on 30/01/16.
//

#ifndef SPLENDITORMASTER_NORMALMAPPINGRENDERER_H
#define SPLENDITORMASTER_NORMALMAPPINGRENDERER_H

#include "../Models/TexturedModel.h"
#include "../Entities/Entity.h"
#include "ShadowedNormalMappingShader.h"
#include <map>
#include <Splenditor/ToolBox/World.h>

class NormalMappingRenderer {
public:
    NormalMappingRenderer(mat4 projectionMatrix);
    ~NormalMappingRenderer();
    void render(map<TexturedModel*, std::list<Entity*> > entities, vec4 clipPlane, World* world);
    void render(map<TexturedModel*, std::list<Entity*> > entities, vec4 clipPlane, World* world, float shadowResolution, mat4 toShadowMapSpace, int PCF, float shadowDistance);
    void rendering(map<TexturedModel *, std::list<Entity *> > entities, NormalMappingShader* shader);
    void prepareTexturedModel(TexturedModel *model, NormalMappingShader* shader);
    void unbindTexturedModel();
    void prepareInstance(Entity *entity, NormalMappingShader* shader);
    void prepare(vec4 clipPlane, World* world, NormalMappingShader* shader);
    vec3 rbg_OpenGl(vec3 rbg);
private:
    NormalMappingShader *normal_shader;
    ShadowedNormalMappingShader *shadowed_normal_shader;
};


#endif //SPLENDITORMASTER_NORMALMAPPINGRENDERER_H
