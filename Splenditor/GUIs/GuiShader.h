//
// Created by jack on 5/12/15.
//

#ifndef SPLENDITORMASTER_GUISHADER_H
#define SPLENDITORMASTER_GUISHADER_H


#include <Splenditor/RenderEngine/ShaderProgram.h>

class GuiShader : public ShaderProgram {
public:
    GuiShader(const char *vertex_file_path, const char *fragment_file_path);
    ~GuiShader();
    void loadTransformationMatrix(mat4 matrix);

protected:
    void getAllUniformLocations();
    void bindAttributes();

private:
    GLint location_transformationMatrix;

};



#endif //SPLENDITORMASTER_GUISHADER_H
