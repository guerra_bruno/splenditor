//
// Created by jack on 1/12/15.
//

#ifndef SPLENDITORMASTER_PLAYER_H
#define SPLENDITORMASTER_PLAYER_H


#include <GL/glew.h>
#include <glfw3.h>
#include <Splenditor/Terrains/Terrain.h>
#include "Entity.h"

class Player : public Entity {

public:
    Player(TexturedModel *model, vec3 position, float rx, float ry, float rz, float scale, const string &name) : Entity(model, position, rx, ry, rz, scale, name) { }
    ~Player();
    void move(bool *keys, double delta, Terrain *terrain);
    void jump();
    void checkInputs(bool *keys);
    float getSpeed();
    float getCurrentTurnSpeed();
private:
    static constexpr float RUN_SPEED = 30, TURN_SPEED = 160, GRAVITY = -50, JUMP_POWER = 30;
    float currentSpeed = 0.0, currentTurnSpeed = 0, upwardsSpeed = 0.0;
    bool isInAir = false;

};


#endif //SPLENDITORMASTER_PLAYER_H
