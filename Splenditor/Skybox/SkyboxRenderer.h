//
// Created by jack on 8/12/15.
//

#ifndef SPLENDITORMASTER_SKYBOXRENDERER_H
#define SPLENDITORMASTER_SKYBOXRENDERER_H


#include <Splenditor/RenderEngine/Loader.h>
#include <Splenditor/ToolBox/common.h>
#include "SkyboxShader.h"

class SkyboxRenderer {
public:
    SkyboxRenderer(Loader *loader, mat4 projectionMatrix);
    ~SkyboxRenderer();
    void render(Camera *camera, double delta, bool day, float r, float g, float b);
    void bindTextures(float blendFactor);
private:
    RawModel *cube;
    SkyboxShader *shader;
    GLuint dayTextureID, nightTextureID;
    vector<float> vertices;
    vector<string> textures_day;
    vector<string> textures_night;
    static constexpr float SIZE = 1000.0, SUNSET_TRANSITION = 1.0f;
    float blendFactor;
};


#endif //SPLENDITORMASTER_SKYBOXRENDERER_H
