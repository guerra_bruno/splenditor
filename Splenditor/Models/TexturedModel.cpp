//
// Created by jack on 23/11/15.
//

#include "TexturedModel.h"

// textureModel is a raw model with a textureID
TexturedModel::TexturedModel(RawModel *model, ModelTexture *texture) {
    this->rawModel = model;
    this->texture = texture;
}

RawModel* TexturedModel::GetRawModel() const {
    return rawModel;
}

ModelTexture* TexturedModel::GetTexture() const {
    return texture;
}

TexturedModel::~TexturedModel() {
    delete rawModel;
    delete texture;
}
