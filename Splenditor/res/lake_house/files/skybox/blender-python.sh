#!/bin/bash

PYTHON_VERSION_PREFIX='3.4'
PYTHON_VERSION_SUFFIX='2'
PYTHON_VERSION="$PYTHON_VERSION_PREFIX.$PYTHON_VERSION_SUFFIX"
BLENDER_VERSION_PREFIX='2.76'
BLENDER_VERSION_SUFFIX='b'
BLENDER_VERSION="$BLENDER_VERSION_PREFIX$BLENDER_VERSION_SUFFIX"

clean_sources=false
pip_packages=('numpy' 'scipy' 'scikit-learn' 'scikit-image')

function help() {
	printf "An installation script for Blender & Python for the MeshGen software.\n"
	printf "Blender currently uses its own version of Python 3, which does not provide all necessary functionality required for the MeshGen software. This installation script replaces Blender's version of Python with a full package, including pip.\n"
	printf "This script will also install local versions of the following packages:\n"
	printf "\t%s\n" "${pip_packages[@]}"
	printf "These are required in order for MeshGen's Python scripts to function properly.\n\n"
	printf "Valid arguments:\n"
	printf "\t-c\tClean downloaded source files & packages. These are not deleted by default.\n"
	printf "\t-h\tPrint this help text.\n"
}

while getopts ":ch" opt; do
	case $opt in
		c)
			clean_sources=true
			printf "Sources will be cleaned.\n"
			;;
		h)
			help
			exit 0
			;;
		\?)
			printf "Invalid option: -%s\n" "$OPTARG" 1>&2
			help
			exit 1
			;;
		:)
			printf "Option -%s requires an argument.\n" "$OPTARG" 1>&2
			exit 1
			;;
	esac
done

# Download & extract Python source
if [ ! -d Python-"$PYTHON_VERSION" ]; then
	# If there's a .tgz file already there, don't download another.
	if [ ! -f Python-"$PYTHON_VERSION".tgz ]; then
		printf "Downloading Python.\n"
		wget https://www.python.org/ftp/python/"$PYTHON_VERSION"/Python-"$PYTHON_VERSION".tgz
	fi

	printf "Extracting Python.\n"
	tar -xzf Python-"$PYTHON_VERSION".tgz
fi

# Download & extract Blender
# TODO: get downloading compatible with other operating systems (with uname -s)
if [ ! -d blender-"$BLENDER_VERSION" ]; then
	if [ ! -f blender-"$BLENDER_VERSION".tar.bz2 ]; then
		wget -O blender-"$BLENDER_VERSION".tar.bz2 https://download.blender.org/release/Blender"$BLENDER_VERSION_PREFIX"/blender-"$BLENDER_VERSION"-linux-glibc211-$(uname -m).tar.bz2
	fi

	# Extract Blender.
	# We simplify the name of the extracted directory - otherwise it's pretty confusing and verbose.
	mkdir -p blender-"$BLENDER_VERSION"
	printf "Extracting Blender.\n"
	tar -xjf blender-"$BLENDER_VERSION".tar.bz2 -C blender-"$BLENDER_VERSION" --strip-components=1
fi

# Keep a backup of Blender's version of Python if one does not already exist
cd blender-"$BLENDER_VERSION"/"$BLENDER_VERSION_PREFIX"
if [ -d python ] && [ ! -d python-backup ]; then
	printf "Backing up old Blender version of Python to python-backup.\n"
	mv python python-backup
fi

# Compile a portable version of Python from source
printf "Building Python.\n"
mkdir python
cd ../../Python-"$PYTHON_VERSION"
./configure --prefix=$(pwd)/../blender-"$BLENDER_VERSION"/"$BLENDER_VERSION_PREFIX"/python
make
make install

# Build and install pip packages
cd ../blender-"$BLENDER_VERSION"/"$BLENDER_VERSION_PREFIX"/python
printf "Building pip packages.\n"
for p in ${pip_packages[@]}; do
	printf "Installing pip package %s.\n" "$p"
	bin/pip3.4 install "$p"
done

# Perform cleanup of source files.
# Known issue: we need to keep the extracted Python package around - otherwise pip will no longer function. Unless I'm doing something wrong, Python's 'portable' package isn't actually completely portable with regards to pip.
if [ $clean_sources = true ]; then
	printf "Cleaning source packages.\n"
	rm Python-"$PYTHON_VERSION".tgz
	rm blender-"$BLENDER_VERSION".tar.bz2
fi
printf "Done!\n"
