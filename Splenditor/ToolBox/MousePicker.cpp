//
// Created by jack on 10/12/15.
//

#include "MousePicker.h"


MousePicker::MousePicker(Camera *cam, Mouse *mouse, Terrain *terrain, mat4 projection) {
    this->camera = cam;
    this->mouse = mouse;
    this->terrain = terrain;
    this->projectionMatrix = projection;
    this->viewMatrix = createViewMatrix(camera->GetPitch(), camera->GetYaw(), camera->GetPosition());
    this->RAY_RANGE = 600;
    this->RECURSION_COUNT = 200;

}

MousePicker::~MousePicker() {
}

vec3 MousePicker::getCurrentRay() {
    return currentRay;
}

vec3 MousePicker::calculateMouseRay() {
    vec2 normalizeCoords = getNormalizedDeviceCoords(mouse->getPosition().x, mouse->getPosition().y);
    vec4 clipCoords = vec4(normalizeCoords.x, normalizeCoords.y, -1.0, 1.0);
    vec4 eyeCoords = toEyeCoords(clipCoords);
    vec3 worldRay = toWorldCoords(eyeCoords);
    return worldRay;
}

void MousePicker::update() {
    viewMatrix = createViewMatrix(camera->GetPitch(), camera->GetYaw(), camera->GetPosition());
    currentRay = calculateMouseRay();

    if (intersectionInRange(0, RAY_RANGE, currentRay)) {
        currentTerrainPoint = binarySearch(0, 0, RAY_RANGE, currentRay);
    } else {
        currentTerrainPoint = vec3(0.0,0.0,0.0);
    }
}

vec2 MousePicker::getNormalizedDeviceCoords(float mouse_x, float mouse_y) {
    float x = (float) ((2.0 * mouse_x) / mouse->getDisplay()->getWidth() - 1.0);
    float y = (float) ((2.0 * mouse_y) / mouse->getDisplay()->getHeight() - 1.0);
    return vec2(x,-y);
}

vec4 MousePicker::toEyeCoords(vec4 clipCoords) {
    mat4 invertProjection = inverse(projectionMatrix);
    vec4 eyeCoords = invertProjection * clipCoords;
    return vec4(eyeCoords.x, eyeCoords.y, -1.0, 0.0);

}

vec3 MousePicker::toWorldCoords(vec4 eyeCoords) {
    mat4 invertedView = inverse(viewMatrix);
    vec4 rayWorld = invertedView * eyeCoords;
    vec3 mouseRay = vec3(rayWorld.x, rayWorld.y, rayWorld.z);
    return normalize(mouseRay);
}

vec3 MousePicker::getPointOnRay(vec3 ray, float distance) {
    vec3 camPos = camera->GetPosition();
    vec3 start = vec3(camPos.x, camPos.y, camPos.z);
    vec3 scaledRay = vec3(ray.x * distance, ray.y * distance, ray.z * distance);
    return start + scaledRay;
}

vec3 MousePicker::binarySearch(int count, float start, float finish, vec3 ray) {
    float half = start + ((finish - start) / 2.0f);
    if (count >= RECURSION_COUNT) {
        vec3 endPoint = getPointOnRay(ray, half);
        return endPoint;
    }
    if (intersectionInRange(start, half, ray)) {
        return binarySearch(count + 1, start, half, ray);
    } else {
        return binarySearch(count + 1, half, finish, ray);
    }
}

bool MousePicker::intersectionInRange(float start, float finish, vec3 ray) {
    vec3 startPoint = getPointOnRay(ray, start);
    vec3 endPoint = getPointOnRay(ray, finish);
    return !isUnderGround(startPoint) && isUnderGround(endPoint);
}

bool MousePicker::isUnderGround(vec3 testPoint) {
    float height = 0;
    height = terrain->getHeightOfTerrain(testPoint.x, testPoint.z);
    return testPoint.y < height;
}

vec3 MousePicker::getCurrentTerrainPoint() {
    return currentTerrainPoint;
}
