//
// Created by jack on 11/03/16.
//

#ifndef SPLENDITORMASTER_SHADOWEDTERRAIN_H
#define SPLENDITORMASTER_SHADOWEDTERRAIN_H


#include "TerrainShader.h"

class ShadowedTerrainShader : public TerrainShader {
public:
    ShadowedTerrainShader(const char *vertex_file_path, const char *fragment_file_path);
    void connectTextureUnits();
    void loadMapSize(float mapSize);
    void loadToShadowMapSpace(mat4 matrix);
    void loadPCF(int pcf);
    void loadShadowDistance(float shadowDistance);

protected:
    void getAllUniformLocations();

private:
    GLint location_toShadowMapSpace;
    GLint location_shadowMap;
    GLint location_mapSize;
    GLint location_pcfCount;
    GLint location_shadowDistance;

};

#endif //SPLENDITORMASTER_SHADOWEDTERRAIN_H
