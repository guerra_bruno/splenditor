//
// Created by jack on 31/01/16.
//

#ifndef SPLENDITORMASTER_COMMON_H
#define SPLENDITORMASTER_COMMON_H

#include <vector>
#include <stdio.h>
#include <string>
#include <sstream>
#include <cstring>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <Splenditor/RenderEngine/Loader.h>


using namespace std;
using namespace glm;

vector<string> &split(const string &s, char delim, vector<string> &elems);

vector<string> split(const string &s, char delim);

mat4 createTransformation(vec2 translation, vec2 scale);

mat4 createViewMatrix(float pitch, float yaw, vec3 position);

mat4 createTransformation(vec3 translation, float rx, float ry, float rz, float s);

float barryCentric(vec3 p1, vec3 p2, vec3 p3, vec2 pos);

RawModel* loadObjModel(string fileName, Loader *loader);

RawModel* loadObjNormalModel(string fileName, Loader *loader);

void enableCulling();

void disableCulling();


bool loadOBJ(
        const char * path,
        vector<vec3> & v,
        vector<vec2> & uv,
        vector<vec3> & n
);



void indexVBO_TBN(
        vector<vec3> & in_vertices,
        vector<vec2> & in_uvs,
        vector<vec3> & in_normals,
        vector<vec3> & in_tangents,

        vector<unsigned int> & out_indices,
        vector<vec3> & out_vertices,
        vector<vec2> & out_uvs,
        vector<vec3> & out_normals,
        vector<vec3> & out_tangents
);

void indexVBO_TBN(
        vector<vec3> & in_vertices,
        vector<vec2> & in_uvs,
        vector<vec3> & in_normals,

        vector<unsigned int> & out_indices,
        vector<vec3> & out_vertices,
        vector<vec2> & out_uvs,
        vector<vec3> & out_normals
);

bool getSimilarVertexIndex(
        vec3 & in_vertex,
        vec2 & in_uv,
        vec3 & in_normal,
        vector<vec3> & out_vertices,
        vector<vec2> & out_uvs,
        vector<vec3> & out_normals,
        unsigned int & result,
        int size
);


void computeTangentBasis(
        // inputs
        vector<vec3> & vertices,
        vector<vec2> & uvs,
        vector<vec3> & normals,
        // outputs
        vector<vec3> & tangents
);


bool is_near(
        float v1,
        float v2
);



#endif //SPLENDITORMASTER_COMMON_H
