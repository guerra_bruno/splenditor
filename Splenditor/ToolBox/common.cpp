//
// Created by jack on 31/01/16.
//

#include "common.h"
#include <list>
#include <iostream>


vector<string> &split(const string &s, char delim, vector<string> &elems) {
    list<string> l_elems;
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        l_elems.push_back(item);
    }
    elems = vector<string>{ make_move_iterator(begin(l_elems)), make_move_iterator(end(l_elems)) };
    return elems;
}


vector<string> split(const string &s, char delim) {
    vector<string> elems;
    split(s, delim, elems);
    return elems;
}

bool loadOBJ(
        const char * path,
        vector<vec3> & v,
        vector<vec2> & uv,
        vector<vec3> & n
){
    printf("Loading OBJ file %s...\n", path);

    list<vec3> out_vertices;
    list<vec2> out_uvs;
    list<vec3> out_normals;

    list<unsigned int> vertexIndices, uvIndices, normalIndices;
    list<vec3> temp_vertices;
    list<vec2> temp_uvs;
    list<vec3> temp_normals;


    FILE * file = fopen(path, "r");
    if( file == NULL ){
        printf("Impossible to open the file ! Are you in the right path ? \n");
        getchar();
        return false;
    }

    while( 1 ){

        char lineHeader[128];
        // read the first word of the line
        int res = fscanf(file, "%s", lineHeader);
        if (res == EOF)
            break; // EOF = End Of File. Quit the loop.

        // else : parse lineHeader

        if ( strcmp( lineHeader, "v" ) == 0 ){
            vec3 vertex;
            fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z );
            temp_vertices.push_back(vertex);
        }else if ( strcmp( lineHeader, "vt" ) == 0 ){
            vec2 uv;
            fscanf(file, "%f %f\n", &uv.x, &uv.y );
            uv.y = -uv.y; // Invert V coordinate since we will only use DDS texture, which are inverted. Remove if you want to use TGA or BMP loaders.
            temp_uvs.push_back(uv);
        }else if ( strcmp( lineHeader, "vn" ) == 0 ){
            vec3 normal;
            fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z );
            temp_normals.push_back(normal);
        }else if ( strcmp( lineHeader, "f" ) == 0 ){
            string vertex1, vertex2, vertex3;
            unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
            int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2] );
            if (matches != 9){
                printf("File can't be read by our simple parser :-( Try exporting with other options\n");
                return false;
            }
            vertexIndices.push_back(vertexIndex[0]);
            vertexIndices.push_back(vertexIndex[1]);
            vertexIndices.push_back(vertexIndex[2]);
            uvIndices    .push_back(uvIndex[0]);
            uvIndices    .push_back(uvIndex[1]);
            uvIndices    .push_back(uvIndex[2]);
            normalIndices.push_back(normalIndex[0]);
            normalIndices.push_back(normalIndex[1]);
            normalIndices.push_back(normalIndex[2]);
        }else{
            // Probably a comment, eat up the rest of the line
            char stupidBuffer[1000];
            fgets(stupidBuffer, 1000, file);
        }

    }

    vector<vec3> l_v = vector<vec3>{ make_move_iterator(begin(temp_vertices)), make_move_iterator(end(temp_vertices)) };
    vector<vec2> l_uv = vector<vec2>{ make_move_iterator(begin(temp_uvs)), make_move_iterator(end(temp_uvs)) };
    vector<vec3> l_n = vector<vec3>{ make_move_iterator(begin(temp_normals)), make_move_iterator(end(temp_normals)) };
    vector<unsigned int> l_vi = vector<unsigned int>{ make_move_iterator(begin(vertexIndices)), make_move_iterator(end(vertexIndices)) };
    vector<unsigned int> l_uvi = vector<unsigned int>{ make_move_iterator(begin(uvIndices)), make_move_iterator(end(uvIndices)) };
    vector<unsigned int> l_ni = vector<unsigned int>{ make_move_iterator(begin(normalIndices)), make_move_iterator(end(normalIndices)) };

    // For each vertex of each triangle
    for( unsigned int i=0; i<vertexIndices.size(); i++ ){

        // Get the indices of its attributes
        unsigned int vertexIndex = l_vi[i];
        unsigned int uvIndex = l_uvi[i];
        unsigned int normalIndex = l_ni[i];

        // Get the attributes thanks to the index
        vec3 vertex = l_v[ vertexIndex-1 ];
        vec2 uv = l_uv[ uvIndex-1 ];
        vec3 normal = l_n[ normalIndex-1 ];

        // Put the attributes in buffers
        out_vertices.push_back(vertex);
        out_uvs     .push_back(uv);
        out_normals .push_back(normal);

    }


    v = vector<vec3>{ make_move_iterator(begin(out_vertices)), make_move_iterator(end(out_vertices)) };
    uv = vector<vec2>{ make_move_iterator(begin(out_uvs)), make_move_iterator(end(out_uvs)) };
    n = vector<vec3>{ make_move_iterator(begin(out_normals)), make_move_iterator(end(out_normals)) };


    return true;
}



void indexVBO_TBN(
        vector<vec3> & in_vertices,
        vector<vec2> & in_uvs,
        vector<vec3> & in_normals,
        vector<vec3> & in_tangents,

        vector<unsigned int> & out_indices,
        vector<vec3> & out_vertices,
        vector<vec2> & out_uvs,
        vector<vec3> & out_normals,
        vector<vec3> & out_tangents
){
    out_vertices.resize(in_vertices.size());
    out_uvs.resize(in_uvs.size());
    out_normals.resize(in_normals.size());
    out_tangents.resize(in_tangents.size());
    list<unsigned int> l_out_indices;
    // For each input vertex
    int j = 0;

    // For each input vertex
    for ( unsigned int i=0; i<in_vertices.size(); i++ ){

        // Try to find a similar vertex in out_XXXX
        unsigned int index;
        bool found = getSimilarVertexIndex(in_vertices[i], in_uvs[i], in_normals[i], out_vertices, out_uvs, out_normals, index, j);

        if ( found ){ // A similar vertex is already in the VBO, use it instead !
            l_out_indices.push_back( index );

            // Average the tangents and the bitangents
            out_tangents  [index] += in_tangents[i];
        }else{ // If not, it needs to be added in the output data.
            out_vertices  [j] = ( in_vertices[i]);
            out_uvs       [j] = ( in_uvs[i]);
            out_normals   [j] = ( in_normals[i]);
            out_tangents  [j] = ( in_tangents[i]);
            l_out_indices .push_back((unsigned int) j);
            j++;
        }
    }
    out_vertices.resize(j);
    out_uvs.resize(j);
    out_normals.resize(j);
    out_tangents.resize(j);
    out_indices = vector<unsigned int>{ make_move_iterator(begin(l_out_indices)), make_move_iterator(end(l_out_indices)) };
}


void indexVBO_TBN(
        vector<vec3> & in_vertices,
        vector<vec2> & in_uvs,
        vector<vec3> & in_normals,

        vector<unsigned int> & out_indices,
        vector<vec3> & out_vertices,
        vector<vec2> & out_uvs,
        vector<vec3> & out_normals
){
    out_vertices.resize(in_vertices.size());
    out_uvs.resize(in_uvs.size());
    out_normals.resize(in_normals.size());
    list<unsigned int> l_out_indices;
    // For each input vertex
    int j = 0;
    for ( unsigned int i=0; i<in_vertices.size(); i++ ){

        // Try to find a similar vertex in out_XXXX
        unsigned int index;
        bool found = getSimilarVertexIndex(in_vertices[i], in_uvs[i], in_normals[i], out_vertices, out_uvs, out_normals, index, j);

        if ( found ){ // A similar vertex is already in the VBO, use it instead !
            l_out_indices.push_back( index );

        }else{ // If not, it needs to be added in the output data.
            out_vertices  [j] = ( in_vertices[i]);
            out_uvs       [j] = ( in_uvs[i]);
            out_normals   [j] = ( in_normals[i]);
            l_out_indices.push_back((unsigned int) j);
            j++;
        }
    }
    out_vertices.resize(j);
    out_uvs.resize(j);
    out_normals.resize(j);
    out_indices = vector<unsigned int>{ make_move_iterator(begin(l_out_indices)), make_move_iterator(end(l_out_indices)) };
}


bool getSimilarVertexIndex(
        vec3 & in_vertex,
        vec2 & in_uv,
        vec3 & in_normal,
        vector<vec3> & out_vertices,
        vector<vec2> & out_uvs,
        vector<vec3> & out_normals,
        unsigned int & result,
        int size
){
    // Lame linear search
    for ( unsigned int i=0; i < size; i++ ){
        if (
                is_near( in_vertex.x , out_vertices[i].x ) &&
                is_near( in_vertex.y , out_vertices[i].y ) &&
                is_near( in_vertex.z , out_vertices[i].z ) &&
                is_near( in_uv.x     , out_uvs     [i].x ) &&
                is_near( in_uv.y     , out_uvs     [i].y ) &&
                is_near( in_normal.x , out_normals [i].x ) &&
                is_near( in_normal.y , out_normals [i].y ) &&
                is_near( in_normal.z , out_normals [i].z )
                ){
            result = (unsigned int) i;
            return true;
        }
    }
    // No other vertex could be used instead.
    // Looks like we'll have to add it to the VBO.
    return false;
}


bool is_near(float v1, float v2){
    return fabs( v1-v2 ) < 0.01f;
}


void computeTangentBasis(
        // inputs
        vector<vec3> & vertices,
        vector<vec2> & uvs,
        vector<vec3> & normals,
        // outputs
        vector<vec3> & tangents
){

    list<vec3> l_tangents;

    for (unsigned int i=0; i<vertices.size(); i+=3 ){

        // Shortcuts for vertices
        vec3 & v0 = vertices[i+0];
        vec3 & v1 = vertices[i+1];
        vec3 & v2 = vertices[i+2];

        // Shortcuts for UVs
        vec2 & uv0 = uvs[i+0];
        vec2 & uv1 = uvs[i+1];
        vec2 & uv2 = uvs[i+2];

        // Edges of the triangle : postion delta
        vec3 deltaPos1 = v1-v0;
        vec3 deltaPos2 = v2-v0;

        // UV delta
        vec2 deltaUV1 = uv1-uv0;
        vec2 deltaUV2 = uv2-uv0;

        float r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
        vec3 tangent = (deltaPos1 * deltaUV2.y   - deltaPos2 * deltaUV1.y)*r;
        vec3 bitangent = (deltaPos2 * deltaUV1.x   - deltaPos1 * deltaUV2.x)*r;

        // Set the same tangent for all three vertices of the triangle.
        // They will be merged later, in vboindexer.cpp
        l_tangents.push_back(tangent);
        l_tangents.push_back(tangent);
        l_tangents.push_back(tangent);

    }

    tangents = vector<vec3>{ make_move_iterator(begin(l_tangents)), make_move_iterator(end(l_tangents)) };

}


float barryCentric(vec3 p1, vec3 p2, vec3 p3, vec2 pos) {
    float det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
    float l1 = ((p2.z - p3.z) * (pos.x - p3.x) + (p3.x - p2.x) * (pos.y - p3.z)) / det;
    float l2 = ((p3.z - p1.z) * (pos.x - p3.x) + (p1.x - p3.x) * (pos.y - p3.z)) / det;
    float l3 = 1.0f - l1 - l2;
    return l1 * p1.y + l2 * p2.y + l3 * p3.y;
}

mat4 createTransformation(vec3 translation, float rx, float ry, float rz, float s) {

    mat4 translationMatrix, rotationMatrix, scaleMatrix;

    translationMatrix = translate(mat4(1.0f), translation);
    rotationMatrix =  rotate(mat4(1.0), rx, vec3(1.0, 0.0, 0.0));
    rotationMatrix *= rotate(mat4(1.0), ry, vec3(0.0, 1.0, 0.0));
    rotationMatrix *= rotate(mat4(1.0), rz, vec3(0.0, 0.0, 1.0));
    scaleMatrix = scale(mat4(1.0f), vec3(s, s, s));

    return translationMatrix * rotationMatrix * scaleMatrix;
}

mat4 createViewMatrix(float pitch, float yaw, vec3 position) {
    mat4 viewMatrix = mat4(1.0);

    viewMatrix = rotate(viewMatrix, pitch, vec3(1.0, 0.0, 0.0));
    viewMatrix = rotate(viewMatrix, yaw, vec3(0.0, 1.0, 0.0));
    vec3 negativeCameraPos(-position.x, -position.y, -position.z);
    viewMatrix = translate(viewMatrix, negativeCameraPos);


    return viewMatrix;
}


mat4 createTransformation(vec2 translation, vec2 scale) {
    mat4 scaleMatrix, matrix = mat4(1.0);
    matrix = translate(mat4(1.0f), vec3(translation.x, translation.y, 0.0));
    scaleMatrix = glm::scale(mat4(1.0f), vec3(scale.x, scale.y, 1.0f));
    return matrix * scaleMatrix;
}

RawModel *loadObjModel(string fileName, Loader *loader) {
    vector<vec3> vert, normals, indexed_vert, indexed_normals;
    vector<vec2> uvs, indexed_uvs;
    vector<unsigned int> indices;

    if (!loadOBJ(fileName.c_str(), vert, uvs, normals)) cout << "obj file is corrupted";
    indexVBO_TBN(vert, uvs, normals, indices, indexed_vert, indexed_uvs, indexed_normals);


    vector<float> i_vert {&indexed_vert[0].x, &indexed_vert[0].x + indexed_vert.size() * 3 };
    vector<float> i_uvs {&indexed_uvs[0].x, &indexed_uvs[0].x + indexed_uvs.size() * 2 };
    vector<float> i_normals {&indexed_normals[0].x, &indexed_normals[0].x + indexed_normals.size() * 3 };


    return loader->loadToVAO(i_vert, i_uvs, i_normals, indices);
}

RawModel *loadObjNormalModel(string fileName, Loader *loader) {
    vector<vec3> vert, tangents, normals, indexed_vert, indexed_normals, indexed_tangents;
    vector<vec2> uvs, indexed_uvs;
    vector<unsigned int> indices;

    if (!loadOBJ(fileName.c_str(), vert, uvs, normals)) cout << "obj file is corrupted";
    computeTangentBasis( vert, uvs, normals,  tangents );
    indexVBO_TBN(vert, uvs, normals, tangents, indices, indexed_vert, indexed_uvs, indexed_normals, indexed_tangents );


    vector<float> i_vert {&indexed_vert[0].x, &indexed_vert[0].x + indexed_vert.size() * 3 };
    vector<float> i_uvs {&indexed_uvs[0].x, &indexed_uvs[0].x + indexed_uvs.size() * 2 };
    vector<float> i_normals {&indexed_normals[0].x, &indexed_normals[0].x + indexed_normals.size() * 3 };
    vector<float> i_tangents {&indexed_tangents[0].x, &indexed_tangents[0].x + indexed_tangents.size() * 3 };


    return loader->loadToVAO(i_vert, i_uvs, i_normals, i_tangents, indices);
}

void enableCulling() {
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
}

void disableCulling() {
    glDisable(GL_CULL_FACE);
}
