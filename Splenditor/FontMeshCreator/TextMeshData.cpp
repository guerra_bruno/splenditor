//
// Created by jack on 16/02/16.
//

#include <Splenditor/FontMeshCreator/TextMeshData.h>
#include <cstdio>

TextMeshData::TextMeshData(vector<float> vertexPositions, vector<float> textureCoords) {
    this->vertexPositions = vertexPositions;
    this->textureCoords = textureCoords;
}

int TextMeshData::getVertexCount() {
    return (int) (vertexPositions.size() / 2);
}

TextMeshData::~TextMeshData() {
}
