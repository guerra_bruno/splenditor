//
// Created by jack on 18/02/16.
//

#ifndef SPLENDITORMASTER_PARTICLETEXTURE_H
#define SPLENDITORMASTER_PARTICLETEXTURE_H

#include "GL/glew.h"

class ParticleTexture {
public:
    ParticleTexture(GLuint textureID, GLuint numberOfRows, bool additive) : textureID(textureID), numberOfRows(numberOfRows), additive(additive) { }
    ~ParticleTexture();
    GLuint getTextureID() const { return textureID; }
    GLuint getNumberOfRows() const { return numberOfRows; }
    bool getAdditive() const { return additive; }

private:
    GLuint textureID;
    GLuint numberOfRows;
    bool additive;


};


#endif //SPLENDITORMASTER_PARTICLETEXTURE_H
