//
// Created by jack on 30/01/16.
//

#include <Splenditor/RenderEngine/MasterRenderer.h>
#include "NormalMappingRenderer.h"

NormalMappingRenderer::NormalMappingRenderer(mat4 projectionMatrix) {
    shadowed_normal_shader = new ShadowedNormalMappingShader("Shaders/shadowedNormalMapVertexShader.glsl", "Shaders/shadowedNormalMapFragmentShader.glsl");
    shadowed_normal_shader->start();
    shadowed_normal_shader->loadProjectionMatrix(projectionMatrix);
    shadowed_normal_shader->connectTextureUnits();
    shadowed_normal_shader->stop();

    normal_shader = new NormalMappingShader("Shaders/normalMapVertexShader.glsl", "Shaders/normalMapFragmentShader.glsl");
    normal_shader->start();
    normal_shader->loadProjectionMatrix(projectionMatrix);
    normal_shader->connectTextureUnits();
    normal_shader->stop();
}

NormalMappingRenderer::~NormalMappingRenderer() {
    delete shadowed_normal_shader;
    delete normal_shader;
}

void NormalMappingRenderer::render(map<TexturedModel *, std::list<Entity *> > entities, vec4 clipPlane, World* world) {
    prepare(clipPlane, world, normal_shader);
    rendering(entities, normal_shader);

}

void NormalMappingRenderer::render(map<TexturedModel *, std::list<Entity *> > entities, vec4 clipPlane, World* world, float shadowResolution, mat4 toShadowMapSpace, int PCF, float shadowDistance) {
    prepare(clipPlane, world, shadowed_normal_shader);
    shadowed_normal_shader->loadPCF(PCF);
    shadowed_normal_shader->loadMapSize(shadowResolution);
    shadowed_normal_shader->loadToShadowMapSpace(toShadowMapSpace);
    shadowed_normal_shader->loadShadowDistance(shadowDistance);
    rendering(entities, shadowed_normal_shader);
}

void NormalMappingRenderer::rendering(map<TexturedModel *, std::list<Entity *> > entities, NormalMappingShader* shader) {

    for (auto& entity : entities){
        prepareTexturedModel(entity.first, shader);
        for(auto& model : entity.second){
            prepareInstance(model, shader);
            glDrawElements(GL_TRIANGLES, entity.first->GetRawModel()->getVertexCount(), GL_UNSIGNED_INT, (void*) 0);
        }
        unbindTexturedModel();
    }
    shader->stop();
    entities.clear();
}

void NormalMappingRenderer::prepareTexturedModel(TexturedModel *model, NormalMappingShader* shader) {
    glBindVertexArray((GLuint) model->GetRawModel()->getVaoID());
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glEnableVertexAttribArray(3);
    if(model->GetTexture()->isHasTransparency()) disableCulling();
    shader->loadRowNumber(model->GetTexture()->getRowNumber());
    shader->loadShineVariables(model->GetTexture()->getShineDamper(), model->GetTexture()->getReflectivity());

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, model->GetTexture()->getTextureID());

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, model->GetTexture()->getNormalMap());
}

void NormalMappingRenderer::unbindTexturedModel() {
    enableCulling();
    glDisableVertexAttribArray(3);
    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);
    glBindVertexArray(0);
}

void NormalMappingRenderer::prepareInstance(Entity *entity, NormalMappingShader* shader) {
    mat4 transformationMatrix = createTransformation(entity->GetPosition(), entity->GetRx(), entity->GetRy(), entity->GetRz(), entity->GetScale());
    shader->loadTransformationMatrix(transformationMatrix);
    shader->loadOffset(entity->getTextureXOffset(), entity->getTextureYOffset());
}


void NormalMappingRenderer::prepare(vec4 clipPlane, World* world, NormalMappingShader* shader) {
    shader->start();
    vec3 colour = rbg_OpenGl(world->colour);
    shader->loadClipPlane(clipPlane);
    shader->loadSkyColour(colour.r, colour.g, colour.b);
    shader->loadLights(world->lights, createViewMatrix(world->camera->GetPitch(), world->camera->GetYaw(), world->camera->GetPosition()));
    shader->loadViewMatrix(world->camera);

}

vec3 NormalMappingRenderer::rbg_OpenGl(vec3 rbg) {
    return vec3(rbg.r * (1.0/255.0), rbg.g * (1.0/255.0), rbg.b * (1.0/255.0));
}


