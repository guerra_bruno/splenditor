//
// Created by jack on 26/11/15.
//


#include <iomanip>
#include "Entity.h"


Entity::Entity(TexturedModel *model, vec3 position, float rx, float ry, float rz, float scale, string name) {
    this->model = model;
    this->textureIndex = 0;
    this->position = position;
    this->rx = rx;
    this->ry = ry;
    this->rz = rz;
    this->name = name;
    this->scale = scale;
}


Entity::Entity(TexturedModel *model, int index, vec3 position, float rx, float ry, float rz, float scale, string name) {
    this->model = model;
    this->textureIndex = index;
    this->position = position;
    this->rx = rx;
    this->ry = ry;
    this->rz = rz;
    this->name = name;
    this->scale = scale;
}

Entity::Entity(Entity *entity) {
    model = entity->GetModel();
    position = entity->GetPosition();
    rx = entity->GetRx();
    ry = entity->GetRy();
    rz = entity->GetRz();
    scale = entity->GetScale();
    textureIndex = entity->textureIndex;
    name = entity->GetName();
}

Entity::Entity(Entity *entity, vec3 position) {
    model = entity->GetModel();
    this->position = position;
    rx = entity->GetRx();
    ry = entity->GetRy();
    rz = entity->GetRz();
    scale = entity->GetScale();
    textureIndex = entity->textureIndex;
    name = entity->GetName();
}


// incrise position of obj
void Entity::increasePosition(float dx, float dy, float dz) {
    this->position.x += dx;
    this->position.y += dy;
    this->position.z += dz;
}

// increase rotation of object
void Entity::increaseRotation(float dx, float dy, float dz) {
    this->rx += dx;
    this->ry += dy;
    this->rz += dz;
}

string Entity::GetName() const {
    return name;
}

void Entity::SetScale(float scale) {
    this->scale = scale;
}

float Entity::GetScale() const {
    return scale;
}

void Entity::SetRz(float rz) {
    this->rz = rz;
}

float Entity::GetRz() const {
    return rz;
}

void Entity::SetRy(float ry) {
    this->ry = ry;
}

float Entity::GetRy() const {
    return ry;
}

void Entity::SetRx(float rx) {
    this->rx = rx;
}

float Entity::GetRx() const {
    return rx;
}

void Entity::SetPosition(vec3 position) {
    this->position = position;
}

void Entity::SetPositionY(float y) {
    this->position.y = y;
}


vec3 Entity::GetPosition() const {
    return position;
}

void Entity::SetModel(TexturedModel* model) {
    this->model = model;
}

TexturedModel* Entity::GetModel() const {
    return model;
}

Entity::~Entity() {

}

float Entity::getTextureYOffset() {
    int row = textureIndex / model->GetTexture()->getRowNumber();
    return (float) row / (float) model->GetTexture()->getRowNumber();
}

float Entity::getTextureXOffset() {
    int column = textureIndex % model->GetTexture()->getRowNumber();
    return (float) column / (float) model->GetTexture()->getRowNumber();
}

void Entity::PrintWorldObject() const {
    cout << "{ \"position\": " << "[" << this->GetPosition().x << ", " << this->GetPosition().y << ", "<< this->GetPosition().z  << "]" << ", ";
    cout << "\"rotation\": [" << this->GetRx() << ", " << this->GetRy() << ", " << this->GetRz() << "], ";
    cout << setprecision(1);
    cout << "\"scale\": " << this->GetScale() << " }," << endl;
}
