//
// Created by jack on 26/11/15.
//

#ifndef SPLENDITORMASTER_CAMERA_H
#define SPLENDITORMASTER_CAMERA_H

#include "../ToolBox/common.h"
#include <Splenditor/RenderEngine/DisplayManager.h>
#include <Splenditor/ToolBox/Mouse.h>
#include <Splenditor/Terrains/Terrain.h>
#include <Splenditor/Entities/Player.h>

class Camera {
public:
    Camera(Player *player, Mouse *mouse, Terrain* terrain);
    ~Camera();
    float GetYaw();
    float GetPitch();
    void calculateZoom();
    void move(Terrain* terrain, double delta);
    void calculateAnglePitch(double delta);
    void invertPitch();
    void calculateViewMatrix();
    void setViewMatrix(mat4 view);
    vec3 GetPosition();
    mat4 getViewMatrix();
    vec3 position;
    Player *player;
    float distanceFromPlayer = 50.0, angleAroundPlayer = 0;
private:
    float calculateHorizontalDistance(int prediction);
    float calculateVerticalDistance(int prediction);
    void camera_collision(int prediction, vec3 pos, Terrain* terrain);
    vec3  calculateCameraPosition(float horizDistance, float verticDistance);
    vec3 prediction_front;
    vec3 prediction_back;
    mat4 viewMatrix;
    float yaw = 0, pitch;

    Mouse *mouse;

};


#endif //SPLENDITORMASTER_CAMERA_H
