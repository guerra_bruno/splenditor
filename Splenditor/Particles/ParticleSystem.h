//
// Created by jack on 18/02/16.
//

#ifndef SPLENDITORMASTER_PARTICLESYSTEM_H
#define SPLENDITORMASTER_PARTICLESYSTEM_H

#include <Splenditor/RenderEngine/DisplayManager.h>
#include <Splenditor/ToolBox/common.h>
#include <Splenditor/ToolBox/World.h>
#include "ParticleMaster.h"

class ParticleSystem {
public:
    ParticleSystem(ParticleTexture* texture, float pps, float speed, float gravityComplient, float lifeLength, float scale, ParticleMaster *master, World* world);
    ~ParticleSystem();
    float random();
    float randomRange(float LO, float HI);
    void setDirection(vec3 direction, float deviation);
    void randomizeRotation();
    void setSpeedError(float error);
    void setLifeError(float error);
    void setScaleError(float error);
    void generateParticles(vec3 systemCenter);
    vec3 croiss(vec3 left, vec3 right) {return vec3(left.y * right.z - left.z * right.y, right.x * left.z - right.z * left.x, left.x * right.y - left.y * right.x ); }
    float doti(vec3 left, vec3 right) { return left.x * right.x + left.y * right.y + left.z * right.z; }



private:
    ParticleTexture* texture;
    World* world;
    ParticleMaster *master;
    float pps, averageSpeed, gravityComplient, averageLifeLength, averageScale;
    float speedError, lifeError, scaleError;
    bool randomRotation;
    vec3 direction;
    float directionDeviation;
    void emitParticle(vec3 center);
    float generateValue(float average, float errorMargin);
    float generateRotation();
    vec3 generateRandomUnitVectorWithinCone(vec3 coneDirection, float angle);
    vec3 generateRandomUnitVector();

};


#endif //SPLENDITORMASTER_PARTICLESYSTEM_H
