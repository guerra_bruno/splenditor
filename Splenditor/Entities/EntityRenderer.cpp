//
// Created by jack on 20/11/15.
//


#include <GL/glew.h>
#include "EntityRenderer.h"


EntityRenderer::EntityRenderer(mat4 projectionMatrix) {

    this->shadow_entity_shader = new ShadowedEntityShader("Shaders/shadowedEntityVertexShader.glsl", "Shaders/shadowedEntityFragmentShader.glsl");
    this->shadow_entity_shader->start();
    this->shadow_entity_shader->loadProjectionMatrix(projectionMatrix);
    this->shadow_entity_shader->connectTextureUnits();
    this->shadow_entity_shader->stop();

    this->entity_shader = new EntityShader("Shaders/entityVertexShader.glsl", "Shaders/entityFragmentShader.glsl");
    this->entity_shader->start();
    this->entity_shader->loadProjectionMatrix(projectionMatrix);
    this->entity_shader->stop();
}

void EntityRenderer::rendering(map<TexturedModel*, std::list<Entity*> > entities, EntityShader *shader) {
    for (auto& entity : entities){
        prepareTexturedModel(entity.first, shader);
        for(auto& model : entity.second){
            prepareInstance(model, shader);
            glDrawElements(GL_TRIANGLES, entity.first->GetRawModel()->getVertexCount(), GL_UNSIGNED_INT, (void*) 0);
        }
        unbindTexturedModel();
    }
    entities.clear();
}


void EntityRenderer::prepareTexturedModel(TexturedModel *model, EntityShader* shader){
    glBindVertexArray((GLuint) model->GetRawModel()->getVaoID());
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    if(model->GetTexture()->isHasTransparency()) disableCulling();
    shader->loadRowNumber(model->GetTexture()->getRowNumber());
    shader->loadFakeLighting(model->GetTexture()->isUseFakeLighting());
    shader->loadShineVariables(model->GetTexture()->getShineDamper(), model->GetTexture()->getReflectivity());
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, model->GetTexture()->getTextureID());

}


void EntityRenderer::unbindTexturedModel(){
    enableCulling();
    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);
    glBindVertexArray(0);
}

void EntityRenderer::prepareInstance(Entity *entity, EntityShader* shader){
    mat4 transformationMatrix = createTransformation(entity->GetPosition(), entity->GetRx(), entity->GetRy(), entity->GetRz(), entity->GetScale());
    shader->loadTransformationMatrix(transformationMatrix);
    shader->loadOffset(entity->getTextureXOffset(), entity->getTextureYOffset());
}


EntityRenderer::~EntityRenderer() {
    delete shadow_entity_shader;
    delete entity_shader;
}

void EntityRenderer::render(map<TexturedModel *, std::list<Entity *> > entities, vec4 clipPlane, vec3 colour, World *world) {
    prepare(clipPlane, colour, world, entity_shader);
    rendering(entities, entity_shader);
    entity_shader->stop();
}

void EntityRenderer::render(map<TexturedModel *, std::list<Entity *> > entities, vec4 clipPlane, vec3 colour, World *world, float shadowResolution, mat4 toShadowMapSpace, int PCF, float shadowDistance) {
    prepare(clipPlane, colour, world, shadow_entity_shader);
    shadow_entity_shader->loadPCF(PCF);
    shadow_entity_shader->loadMapSize(shadowResolution);
    shadow_entity_shader->loadToShadowMapSpace(toShadowMapSpace);
    shadow_entity_shader->loadShadowDistance(shadowDistance);
    rendering(entities, shadow_entity_shader);
    shadow_entity_shader->stop();
}

void EntityRenderer::prepare(vec4 clipPlane, vec3 colour, World *world, EntityShader* shader) {
    shader->start();
    shader->loadClipPlane(clipPlane);
    shader->loadSkyColour(colour.r, colour.g, colour.b);
    shader->loadLights(world->lights);
    shader->loadViewMatrix(world->camera);
}
