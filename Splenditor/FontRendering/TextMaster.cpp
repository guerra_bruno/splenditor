//
// Created by jack on 17/02/16.
//

#include "TextMaster.h"

TextMaster::TextMaster() {

}

TextMaster::~TextMaster() {

    for (auto& kv : texts) {
        delete kv.first;
        kv.second.clear();
    }
    texts.clear();

    delete renderer;
}

void TextMaster::init(Loader *loader) {
    this->renderer = new FontRenderer();
    this->loader = loader;
}

void TextMaster::loadText(Text *text) {
    FontType *font = text->getFont();
    TextMeshData *data = font->loadText(text->getGuiInfo());
    vector<GLuint> vb = loader->loadToVAO(data->getVertexPositions(), data->getTextureCoords());
    text->getGuiInfo()->setMeshInfo(vb[0], vector<GLuint>(vb.begin()+1, vb.end()), data->getVertexCount());
    vector<Text *> textBatch = texts[font];
    if(textBatch.empty()){
        texts[font] = textBatch;
    }
    texts[font].push_back(text);
}

void TextMaster::removeText(Text *text) {
    vector<Text *> textBatch = texts[text->getFont()];
    //texts[text->getFont()].erase(text);
    if(texts[text->getFont()].empty()){
        texts.erase(text->getFont());
        loader->deleteVAO(text->getGuiInfo()->getTextMeshVao());
        for(auto& vbo : text->getGuiInfo()->getTextMeshVbo()) loader->deleteVBO(vbo);
    }

}

void TextMaster::render() {
    this->renderer->render(texts);
}
