//
// Created by jack on 16/02/16.
//

#ifndef SPLENDITORMASTER_LINE_H
#define SPLENDITORMASTER_LINE_H


#include <Splenditor/FontMeshCreator/Word.h>
#include <vector>

using namespace std;

class Line {
public:
    Line(double spaceWidth, double fontSize, double maxLength);
    ~Line() { }
    double getCurrentLineLength() const { return currentLineLength; }
    vector<Word> getWords() { return words;  }
    double getMaxLength() const { return maxLength; }
    bool attemptToAddWord(Word word);

private:
    double maxLength;
    double spaceSize;
    vector<Word> words;
    double currentLineLength;
};


#endif //SPLENDITORMASTER_LINE_H
